The Image Class
=================

This class is used to display an image on the screen.
It can be scale affecting the original relative dimensions.

__init__ method
-----------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Image.__init__
    :linenos:
    :language: python

List of attributes :
^^^^^^^^^^^^^^^^^^^^^^^^

* *self.surface*: As discussed previously this is generally the screen surface.

* | *self.path*: This is the relative path of the image file with respect to the working directory.
  | Use `os.path.join <https://www.geeksforgeeks.org/python-os-path-join-method/>`_ for compatibility with different operating systems.

* *self.pos*: The position in pixel of the top left corner of the image on the screen.

* | *self.image*: A pygame surface loaded from the image file. It may be worth checking
  | `the pygame.image module documentation <https://www.pygame.org/docs/ref/image.html#pygame.image.load>`_.

  .. note::
      Note that TE smart Thermostat UI assumes that your pygame configuration can load PNG files.
      Check this by ensuring that ``pygame.image.get_extended()`` returns ``True`` or ``1``.

* *self.og_size*: The original size of the image in pixel.

* | *self.size*: The desired size of the image in pixel, as a tuple. The original image will be scaled down to
  | this size. To avoid affecting the original aspect ratio of the image, one of the parameters of
  | this attribute may be set to ``"x"``, and the new size will be chosen so that the aspect ratio is not
  | affected: the other size dimension is taken as the leading one.

show method
--------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Image.show
    :linenos:
    :language: python

This method first scales down the image to *self.size* using `pygame.transform.scale function <https://www.pygame.org/docs/ref/transform.html#pygame.transform.scale>`_.
The resulting surface is then written to *self.surface* using the blit method.

Final example
------------------

.. code-block:: python
    :linenos:

    te_logo = Image(self.surface, os.path.join(sys.path[0], "files", "icons", "te_logo.png"),
                    (0, 0), (250, "x"))
    te_logo.pos = (int((self.width - te_logo.size[0]) / 2), int((self.height - te_logo.size[1]) / 2))
    te_logo.show()

This code creates an image object from the file ``te_logo.png``, without affecting its aspect ratio.
The image is then centred on the screen, giving:

.. image:: /_static/example_image.png
    :align: center
    :alt: alternate
