Database
=======================================
Support
---------------------------------------
The database is hosted by ``Sqlite3``, it is **not a server** but a **single file**.

Tables summary
---------------------------------------
In order to store any useful data, there are 6 tables in the convergence project database.

Tables list
~~~~~~~~~~~~~~~~~
- action (**id**, *id_actuator*, value, timeDate)
- actuator (**id**, name, gpio, value)
- actuator_sensor (*id_actuator*, *id_sensor*, up)
- average (**id**,id_av, id_sensor, timeDate, value)
- average_timeDate (id_av, id_sensor, timeDate, primary_key)
- measure (**id**, value, timeDate, *id_sensor*)
- sensor (**id**, name, unit, target, outer, out, value, derivative_up, derivative_down, date_change)
- setting (**id**, *id_sensor*, delta, start, end, days, months)

Tables scheme
~~~~~~~~~~~~~~~~~

.. list-table:: Database scheme
   :align: center

   * - |scheme_database|
   * - 	.. centered:: Database


Tables description
---------------------------------------

Table action
~~~~~~~~~~~~~~~~~
Introduction
    This log table is used for storing information relating to when the actuators are triggered.
Columns
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |**Column name**         |**Type**    |**Not null** |**Primary Key** |**Auto Increment** |**Unique** |**Default Value**    |
    +========================+============+=============+================+===================+===========+=====================+
    |**id**                  |``INTEGER`` ||x|          ||x|             ||x|                ||x|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |*id_actuator*           |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |value                   |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |timeDate                |``TEXT``    ||x|          ||o|             ||o|                ||o|        |``CURRENT_TIMESTAMP``|
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+

    .. note::
        Column ``value`` is a ``boolean``, it describe the state of the actuator.
        If value is ``True`` it means the actuator was **ON** at the corresponding date.

        Values are added automatically for every change in the ``actuator`` table.
SQL code
    .. code-block:: sql
        :linenos:

        CREATE TABLE "action" (
            "id"	        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
            "id_actuator"	INTEGER NOT NULL,
            "value"	        INTEGER NOT NULL,
            "timeDate"	        TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP
        );

Table actuator
~~~~~~~~~~~~~~~~~
Introduction
    This table references all actuators connected and their *GPIO pins*.
Columns
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |**Column name**         |**Type**    |**Not null** |**Primary Key** |**Auto Increment** |**Unique** |**Default Value**    |
    +========================+============+=============+================+===================+===========+=====================+
    |**id**                  |``INTEGER`` ||x|          ||x|             ||x|                ||x|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |name                    |``TEXT``    ||x|          ||o|             ||o|                ||x|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |gpio                    |``INTEGER`` ||x|          ||o|             ||o|                ||x|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |value                   |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``0``                |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+

    .. note::
        Column ``value`` is a ``boolean``, it describes the state of the actuator.
        If the value is on ``True`` it means the actuator is currently ON.
SQL code
    .. code-block:: sql
        :linenos:

        CREATE TABLE "actuator" (
            "id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
            "name"	TEXT NOT NULL UNIQUE,
            "gpio"	INTEGER NOT NULL UNIQUE,
            "value"	REAL NOT NULL DEFAULT 0
        );

Table actuator_sensor
~~~~~~~~~~~~~~~~~~~~~~~
Introduction
    This table references all the actuators and their influence on all the sensors.
Columns
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |**Column name**         |**Type**    |**Not null** |**Primary Key** |**Auto Increment** |**Unique** |**Default Value**    |
    +========================+============+=============+================+===================+===========+=====================+
    |*id_actuator*           |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |*id_sensor*             |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |up                      |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``0``                |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |backup                  |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``0``                |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+

    .. note::
        This table links the influence of an actuator to a sensor through the column ``up``.

    .. warning::
        Using a single actuator for two sensors is possible but could create conflict.
        Make sure you fully appreciate the implications that come with adding each actuator.
SQL code
    .. code-block:: sql
        :linenos:

        CREATE TABLE "actuator_sensor" (
            "id_actuator"	INTEGER NOT NULL,
            "id_sensor"	        INTEGER NOT NULL,
            "up"	        INTEGER NOT NULL DEFAULT 0,
            "backup"	        INTEGER NOT NULL DEFAULT 0
        );

Table graphs
~~~~~~~~~~~~~~~~~~~~~~~
Introduction
    This table is a cache table that stores points X and Y for quicker graph update.
Columns
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |**Column name**         |**Type**    |**Not null** |**Primary Key** |**Auto Increment** |**Unique** |**Default Value**    |
    +========================+============+=============+================+===================+===========+=====================+
    | **id_sensor**          |``INTEGER`` ||x|          ||x|             ||o|                ||o|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    | **id_graph**           |``INTEGER`` ||x|          ||x|             ||o|                ||o|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    | X                      |``TEXT``    ||x|          ||o|             ||o|                ||o|        | 0                   |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    | Y                      |``TEXT``    ||x|          ||o|             ||o|                ||o|        | 0                   |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+

    .. note::
        ``id_graph`` is an index that indicates if the graph is for the day, week or month (1,2 or 3). 

SQL code
    .. code-block:: sql
        :linenos:

        CREATE TABLE IF NOT EXISTS "graphs" (
                "id_sensor"	INTEGER NOT NULL,
                "id_graph"	INTEGER NOT NULL,
                "X"	TEXT NOT NULL DEFAULT 0,
                "Y"	TEXT NOT NULL DEFAULT 0,
                PRIMARY KEY (id_sensor, id_graph)
        );


Table measure
~~~~~~~~~~~~~~~~~
Introduction
    This table is a log table that stores the measurement history of all sensors. Used for predictions.
Columns
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |**Column name**         |**Type**    |**Not null** |**Primary Key** |**Auto Increment** |**Unique** |**Default Value**    |
    +========================+============+=============+================+===================+===========+=====================+
    |**id**                  |``INTEGER`` ||x|          ||x|             ||x|                ||x|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |value                   |``REAL``    ||x|          ||o|             ||o|                ||o|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |timeDate                |``TEXT``    ||x|          ||o|             ||o|                ||o|        |``CURRENT_TIMESTAMP``|
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |*id_sensor*             |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+

    .. note::
        Values are added automatically for every change in the ``sensor`` table.
SQL code
    .. code-block:: sql
        :linenos:

        CREATE TABLE "measure" (
            "id"	    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
            "value"	    INTEGER NOT NULL,
            "timeDate"	    TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
            "id_sensor"	    INTEGER NOT NULL
        );

Table sensor
~~~~~~~~~~~~~~~~~
Introduction
    This table stores all the sensors and any information relating specifically
    to each property measured.
Columns
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |**Column name**         |**Type**    |**Not null** |**Primary Key** |**Auto Increment** |**Unique** |**Default Value**    |
    +========================+============+=============+================+===================+===========+=====================+
    |**id**                  |``INTEGER`` ||x|          ||x|             ||x|                ||x|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |name                    |``TEXT``    ||x|          ||o|             ||o|                ||o|        |``Sensor``           |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |unit                    |``TEXT``    ||x|          ||o|             ||o|                ||o|        |``Unknow``           |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |target                  |``REAL``    ||x|          ||o|             ||o|                ||o|        |``0``                |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |outer                   |``REAL``    ||x|          ||o|             ||o|                ||o|        |``1``                |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |out                     |``REAL``    ||x|          ||o|             ||o|                ||o|        |``1``                |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |value                   |``REAL``    ||x|          ||o|             ||o|                ||o|        |``0``                |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |derivative_up           |``REAL``    ||x|          ||o|             ||o|                ||o|        |``0``                |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |derivative_down         |``REAL``    ||x|          ||o|             ||o|                ||o|        |``0``                |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |date_change             |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+

    .. note::
        Columns ``outer`` and ``out`` define the double hysteresis borders as defined HERE

SQL code
    .. code-block:: sql
        :linenos:

        CREATE TABLE "sensor" (
            "id"	        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
            "name"	        TEXT NOT NULL DEFAULT 'Sensor',
            "unit"	        TEXT NOT NULL DEFAULT 'Unknown',
            "target"	        REAL NOT NULL DEFAULT 0,
            "outer"	        REAL NOT NULL DEFAULT 1,
            "out"	        REAL NOT NULL DEFAULT 1,
            "value"	        REAL NOT NULL DEFAULT 0,
            "derivative_up"	REAL NOT NULL DEFAULT 0,
            "derivative_down"	REAL NOT NULL DEFAULT 0,
            "date_change"	INTEGER NOT NULL DEFAULT 0
        );

Table setting
~~~~~~~~~~~~~~~~~
Introduction
    This table is paramount for an advanced user, it allows to set a ``delta`` (difference) for the
    the ``target`` of any sensor, that take effect for a given **month**, **day of week**, and **hour**.
    For example, you might want your house to be slightly colder than your usual target on the weekend afternoons,
    during the summer months. Rather than having to modify your target each day, you can create a setting of:
    - 1 °C, from 12h to 18h, on Saturday and Sunday, for the months of July and August (Northern hemisphere).

Columns
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |**Column name**         |**Type**    |**Not null** |**Primary Key** |**Auto Increment** |**Unique** |**Default Value**    |
    +========================+============+=============+================+===================+===========+=====================+
    |**id**                  |``INTEGER`` ||x|          ||x|             ||x|                ||x|        |``NULL``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |*id_sensor*             |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``Sensor``           |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |delta                   |``REAL``    ||x|          ||o|             ||o|                ||o|        |``0``                |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |start                   |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``0``                |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |end                     |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``24``               |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |days                    |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``255``              |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+
    |months                  |``INTEGER`` ||x|          ||o|             ||o|                ||o|        |``4095``             |
    +------------------------+------------+-------------+----------------+-------------------+-----------+---------------------+

    .. note::
        It is possible to have many settings that modify at the same time the ``target`` value with their ``delta`` value.
        The result is:

        .. math::

            final\_target_{\mathrm{sensor}} = target_{\mathrm{sensor}}+\sum_{setting\_sensor} delta_{\mathrm{sensor}}

        The columns ``start`` and ``end`` define the beginning and the end of the period affected by the setting (in 24h day format).
        The columns ``days`` and ``months`` store information as integers varying from 1 to 255 and 1 to 4095 respectively.
        These integers are calculated from binary values with 7 and 12 bits, where Monday and January are the respective
        Least Significant Bits (LSB). For example, if a setting were to take effect on Tuesday and Wednesday only, the corresponding
        binary value would be 0000110, and the database should then store a value of :math:`2^2 + 2^1 = 6` in the days column.
        Similarly, if a setting were to take effect in March and November only, the corresponding binary value would be 010000000100,
        and the database should then store a value of :math:`2^{10} + 2^2 = 1028` in the months column.

SQL code
    .. code-block:: sql
        :linenos:

        CREATE TABLE "setting" (
            "id"	    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
            "id_sensor"	    INTEGER NOT NULL,
            "delta"	    REAL NOT NULL DEFAULT 0,
            "start"	    INTEGER NOT NULL DEFAULT 0,
            "end"	    INTEGER NOT NULL DEFAULT 24,
            "days"	    INTEGER NOT NULL DEFAULT 255,
            "months"	    INTEGER NOT NULL DEFAULT 4095
        );

Object code
---------------------------
.. literalinclude:: ../../../db.py
    :emphasize-lines: 1
    :language: python
    :linenos:
    :pyobject: DB
