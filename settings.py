''' 
   Copyright 2020 TE Connectivity
   Distributed under MIT license.
   See file LICENSE for detail or copy at https://opensource.org/licenses/MIT
'''

import os
import sys
import json
import pubsub
import variables
class Settings:
    def __init__(self):
        self.data = {}
        self.Pb = pubsub.Pubsub()
        self.dir = sys.path[0]
        try:
            self.refresh()
        except FileNotFoundError:
            if self.data == {}:
                self.data = variables.settings
                with open(os.path.join(self.dir,"..", "settings.json"), "w") as write_file:
                    write_file.write(json.dumps(self.data))
                    write_file.close()
    def refresh(self):  # Read the file and update the dictionnary
        with open(os.path.join(self.dir, "..","settings.json"), "r") as read_file:
            self.data = json.load(read_file)
            read_file.close()
    def get(self,data_name):  # Get value, if updated, refresh dictionnary
        if self.Pb.get(data_name):
            self.refresh()
        return (self.data[data_name])
    def set(self, name, value):  # Update dictionnary and write in the file
        self.data[name] = value
        with open(os.path.join(self.dir,"..", "settings.json"), "w") as write_file:
            write_file.write(json.dumps(self.data))
            write_file.close()
        self.Pb.set(name)
if __name__=="__main__":
    s=Settings()