Other non-menu methods
===============================

We now examine all the remaining methods of the class WhatToSho, that are not menu methods.

increment_target method
-----------------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.increment_target
    :linenos:
    :language: python

This method is used to change the value of the base target associated with a sensor.
This is done from the Home menu, by clicking an Arrow object pointing upwards.
Note that the sensor whose target is being changed has name *self.home_property*.



The method first seeks to define an increment (``inc``) and will then increment the target by this increment, both in
the database and in the UI.
The program starts by checking whether *self.home_property* is ``"Temperature"`` or not.
If it is, the increment has to appear as :math:`\frac{1}{2}` of the current unit of temperature.
So ``inc`` must be 0.5 °C if the temperature unit is set to "°C".
Similarly, it should be 0.5 °F if the temperature unit is set to "°F".

.. note::

    Regardless of the unit of temperature in the UI (°C or °F), the value associated with
    the temperature sensor is **always** stored in °C in the database.

    Let F be the value of a temperature expressed in °F corresponding to the value C of the same temperature expressed in °C.

    We have :math:`F = \frac{9}{5} \times C + 32`.


Since we are only storing values in °C, our increment (``inc``) should only be in °C.
So if the temperature unit is set to °C, we should simply set our increment to the value 0.5 with ``inc = 0.5``.
However, if the temperature unit is set to °F, our increment should be set to :math:`\frac{5}{18}` of a degree Celsius.
This is done with ``inc = 5 / 18``.

.. note::

    Since :math:`\frac{5}{18}` is an irrational number, a rounding error may occur if the target is changed
    too much in °F. This should not be too bad of an issue, and will be resolved by clicking on the other triangle.

    **Tip:**

    If you need to change your temperature target by a large amount, it will always be quicker to do so with the unit set to °C.

If *self.home_property* is **not** ``"Temperature"``, the increment ``inc`` is simply taken as one fifth (:math:`\frac{1}{5}`)
of the value ``outer`` associated with the sensor, in the *sensor* table of **db.db**.

Once the increment is defined, the new value of the target is then calculated and stored in
the variable ``new_value`` with the line ``new_value = self.list_sensor_dict[self.home_property]["target"] + inc``.

This new value is then assigned to the target of the home property, using the ``change_attribute`` method.
The new value is also used to reset the value of the sensor target in the database, using ``self.db.set_sensor_target``.

reduce_target method
---------------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.reduce_target
    :linenos:
    :language: python

This method is in all points similar to the previous one, except it decreses the value of the target, by ``dec``.
The two methods *could* be combined into one adding an extra argument that indicates if the user wishes to increase or
decrease the target value, but they have remained as two separate methods for clarity.

go_to method
------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.go_to
    :linenos:
    :language: python

This method is used to go to a different menu method on the next iteration of the ``while True`` loop.
It is generally associated to the "available menus tabs", at the top left of the menu screens.
It takes an argument, which is the name of the next menu method to be called: for example ``self.home``, ``self.options``, or ``self.settings``.

When called, it switches the value of *self.changed_menu* to ``True`` (see the ``display`` method to see what this will cause).
It stores its argument ``menu`` in the attribute *self.menu*, which will be called at the end of the ``display`` method.
Lastly, it reinitialises the value of *self.new_setting_dict* to ``{"confirm": False}``. This allows a user to quit the New Setting menu without
having to finish entering a setting.

temp_diff_given_unit method
-------------------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.temp_diff_given_unit
    :linenos:
    :language: python

This method takes one argument ``diff``, which is a **temperature difference** in °C, and returns
the value of the temperature difference in the appropriate temperature unit.
That is it will return ``diff`` if the UI's temperature unit is set to °C, and ``diff * (9 / 5)`` otherwise.
It is used for the conversion of the setting delta to the appropriate unit.

temp_given_unit method
---------------------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.temp_given_unit
    :linenos:
    :language: python

This method has a similar function to ``temp_diff_given_unit``, except it takes
a **temperature** - rather than a temperature difference - as its argument ``temp``,
and it returns the corresponding value of temperature in the UI's temperature unit.
Effectively, it returns ``temp`` if the UI's temperature unit is set to °C, and
``temp * (9 / 5) + 32`` otherwise.

get_time_to_target method
--------------------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.get_time_to_target
    :linenos:
    :language: python

This method takes one argument, ``date_change`` - a time formated as a string of the form dd/mm/yyyy, HH:MM:SS
and returns the time difference between the current time and this argument.
As the name suggests, this method is usually used to calculate the time before the target of
a property is reached. The argument ``date_change`` is obtained from the database as the value
stored in the column *date_change* of the *sensor* table in **db.db** for the corresponding sensor.
This corresponds to the next time an actuator associated with the sensor property will switch its value and is calculated
by the program in **analyser.py**.

The method returns a string that indicates the time difference with the argument ``date_change``.
The method will first use the `time.strptime <https://docs.python.org/2/library/time.html#time.strptime>`_
function to convert its argument to a `struct_time <https://docs.python.org/2/library/time.html#time.struct_time>`_
object. time.strptime might raise a ``ValueError`` if the string is of incorrect format. This case could for example occur
if a user changes the time format of the *date_change* column in the *sensor* table.
If that error is raised, the method will simply return the string ``"..."``.
Otherwise, the obtained struct_time object is further converted to Unix time with `time.mktime <https://docs.python.org/2/library/time.html#time.mktime>`_.

Two cases may now occur:

1. | The date entered points to a time in the past or exactly at the current time.
   | This indicates that the analyser loop has not yet recalculated when the next date change will be.
   | If that is the case, the method will simply return the string ``"..."``.

2. | The date entered points to a time in the future. If that is the case, we store the time it will
   | take to reach the time corresponding to ``date_change`` in seconds in the variable ``target_time``.
   |
   | If the variable ``target_time`` is larger than 3600 seconds (corresponding to an hour),
   | we make the variable ``target_time_str`` be of the form XXh YY, where it takes XX hours and YY minutes
   | to reach the target. For example, if it is 10:03 and the target is expected to be reached at 13:45,
   | ``target_time_str`` would be ``"3h 42"``.
   | Note that when calculating the number of hours, we first take the modulo of ``target_time`` by :math:`3600 \times 24`,
   | the number of seconds in a day. We thereby make the crude assumption that the time to target will **not** be larger than a day.
   |
   | If the variable ``target_time`` is not larger than 3600 seconds but larger than 60 seconds (1 minute),
   | the variable ``target_time_str`` will be of the form XX min.
   | For example, if it is 10:03 and the target is expected to be rached at 10:36, ``target_time_str`` would be ``"33 min"``
   |
   | Lastly, if the variable ``target_time`` is less than 60 seconds, the ``target_time_str`` will be ``"< 1 min"``.
   | The method then returns ``target_time_str``.

convert_bool method
------------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.convert_bool
    :linenos:
    :language: python

This method is used to convert the state of actuators (stored as 1 for ON and 0 for OFF in the database),
to the corresponding string "ON" or "OFF".
It takes the argument ``boolean``, which is the state of the actuator as stored in the database,
and executes a simple if statement to return either "ON or "OFF".
This is used for the text of the actuator tabs at the bottom of the home screen.

toggle_actuator method
----------------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.toggle_actuator
    :linenos:
    :language: python

This method is used to switch an actuator ON (if it is OFF) or to switch an actuator OFF (if is ON).
This is useful in manual mode, when the user can control the state of actuators directly by pressing the actuator
tabs.
The method takes one argument ``name``, which is the name of the actuator to be toggled.
It will switch the value of the actuator state in the database by making use of the ``set_actuator_value`` method.
It takes the actuator id from the attribute *self.actuator_dict*, a dictionary with keys corresponding to actuator names
and values tuples of the form (actuator database id, actuator state).
Since we are toggling the actuator state, the new state is taken as ``not(self.actuator_dict[name][1])``.
Once it is set in the database, we also change the state value in *self.actuator_dict*.

remove_setting method
---------------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.remove_setting
    :linenos:
    :language: python

This method is used to remove a setting from the database and the UI by extension.
It is called by the DELETE tab associated to each setting in the Settings menu.
Settings are stored in the *setting* table of **db.db**, with a unique id, sotred in the *id* column of the
*setting* table.
This method takes one argument, ``id``, which corresponds to the database id of the setting to be deleted.
We first create a list of all the settings id, for all the settings stored in *self.table_setting*, and store it in the variable ``list_id``.
Recall that this attribute is a list of list of tuples, with each tuple corresponding to one line of the
*setting* table.
We then iterate through ``list_id`` with index i. When we reach the id of the setting to be deleted,
we first delete it from *self.table_setting* using the del method of python list.

.. note::

    The above assumes that there is only one setting displayed per page: each sublist is assumed to contain only one tuple.

We then use the database method ``remove_setting``, to remove the setting from the database.

Finally, we recalculate the index *self.setting_page* that tells us which page of the settings to display.
In effect, if we did not have that recalculation, our list index could be out of range.
We first handle the case that the last setting was deleted that making sure that *self.setting_page* is
``len(self.table_setting) - 1`` at most.
However, if after our deletion the list *self.table_setting* is now empty, we would have made *self.setting_page* be ``-1``,
so we would still get an out of range error.
We handle this case by making sure that *self.setting_page* is at least 0.

change_setting_page method
---------------------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.change_setting_page
    :linenos:
    :language: python

This method is used to browse between pages of settings in the Settings menu.
It is called by the up and down Arrow objects in this menu.
it takes one argument, ``increment``, which is generally ``+1`` or ``-1``, and corresponds to the
change to make to *self.setting_page*.
We first increment *self.setting_page* by the value of ``increment``.
We then take its modulo by the length of the list *self.table_setting* to ensure that our index is never out of range.

Note that this method is strikingly similar to the `change_displaying method <file:///C:/Users/TE392816/Documents/smart_thermostat/docs/build/html/ui/BasicClasses/checklist.html#change-displaying-method>`_
of CheckList objects, which you may refer to for more details.

make_new_setting_dict method
--------------------------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.make_new_setting_dict
    :linenos:
    :language: python

This method allows us to change the value of *self.new_setting_dict*, by adding new key: values
pairs to it, or by changing the value associated with an existing key. Recall that *self.new_setting_dict*
is a dictionary that is used to temporarily store the parameters associated with a new setting as the user
enters it in the New Setting menu.
This method simply takes two arguments, ``parameter`` and ``value``, and modifies the dictionary
by giving it a key: value pair of the form ``parameter: value``.
This is achieved with the unique line ``self.new_setting_dict[parameter] = value``.
