''' 
   Copyright 2020 TE Connectivity
   Distributed under MIT license.
   See file LICENSE for detail or copy at https://opensource.org/licenses/MIT
'''

def loop():

    import os
    import sys

    with open(os.devnull, 'w') as f:
        oldstdout = sys.stdout
        sys.stdout = f
        import pygame
        sys.stdout = oldstdout

    import gui
    import pubsub
    import settings
    import variables

    pygame.init()

    surfaceSize = variables.screen_dimensions

    St = settings.Settings()  # Initialize variable storage object

    if St.get("is_raspberry"):
        screen = pygame.Surface(surfaceSize)
    else:
        screen = pygame.display.set_mode(surfaceSize)

    which_screen = gui.WhatToShow(screen)
    while True:
        if St.get("is_raspberry"):
            which_screen.py_touch = True
        else:
            which_screen.py_touch = False
        which_screen.coord = which_screen.get_coord()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                which_screen.py_touch = True

        which_screen.display()
        which_screen.refresh()
        if St.get("is_raspberry"):
            which_screen.py_touch = True
        else:
            which_screen.py_touch = False

if __name__=="__main__":
    loop()
