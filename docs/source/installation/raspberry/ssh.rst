SSH client configuration
=========================

SSH client
----------
What is it?
~~~~~~~~~~~
Secure Shell (SSH) is a cryptographic network protocol for operating network services securely over an unsecured network.
Typical applications include remote command-line, login, and remote command execution, but any network service can be secured with SSH.
SSH provides a secure channel over an unsecured network in a client–server architecture, 
connecting an SSH client application with an SSH server.
The protocol specification distinguishes between two major versions, referred to as SSH-1 and SSH-2. 
The standard TCP port for SSH is 22. SSH is generally used to access Unix-like operating systems,
but it can also be used on Microsoft Windows. 
Windows 10 uses OpenSSH as its default SSH client.

Find the IP
-----------

Solution 1: Boot with HDMI
~~~~~~~~~~~~~~~~~~~~~~~~~~
You can found your raspberry IP when booting your raspberry with a HDMI screen plugged.
The IP is displayed at the end of the boot.

Solution 2: Open your DHCP panel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
All IP with their MAC address of machine connected to the network are visible from this panel. 
You can open it by typing your ``Gateway IP`` in your brower.

On **Windows** in the cmd type ``ipconfig`` to get your ``Gateway IP``.

On **Linux** in the cmd type ``ip route | grep default`` to get your ``Gateway IP``.

On **Mac OS** in the cmd type ``netstat -nr | grep default`` to get your ``Gateway IP``.

Solution 3: Scan the network
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In order to get IPs from all raspberry of your network, You can run a script such as 
:download:`raspberry_finder.py <../../_downloads/raspberry_finder.py>`.

.. note:: For this script you have to be in the same mask 255.255.255.0

Solution 4: Serial connection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If you have follow the `Serial configuration <#Serial configuration (optional)>`_

Connect to your raspberry
-------------------------
Open you consol and type: ``ssh YOUR_LOGIN@YOUR_IP``

Example for a default login raspberry with the ip 172.0.217.139
    .. code-block:: console
        :linenos:

        ssh pi@172.0.217.139
