''' 
   Copyright 2020 TE Connectivity
   Distributed under MIT license.
   See file LICENSE for detail or copy at https://opensource.org/licenses/MIT
'''

import settings
import variables
import sys
import db
import warnings
import sqlite3
import tempfile
import threading
import controller
import relay
import sensor
import analyser
import random
import unittest
import db
import variables
import pubsub
import time
import os
import sys

try:
    os.remove(os.path.join(tempfile.gettempdir(), 'pubsub.db'))
except:
    pass

St = settings.Settings()
try:  # Test if the machine is a raspberry
    import RPi.GPIO
    St.set("is_raspberry",True)
except ImportError:
    St.set("is_raspberry",False)
warnings.simplefilter("ignore")
def convergence():

    threading.Thread(target = sensor.loop).start()
    threading.Thread(target = controller.loop).start()
    threading.Thread(target = relay.loop).start()
    threading.Thread(target = analyser.loop).start()
    if not(St.get("is_unittest")):
        import refresh
        threading.Thread(target =refresh.loop).start()
if __name__ == "__main__":
    Db = db.DB()
    St.set("is_unittest",False)

    if St.get("is_raspberry"):
        Db.clear_after_reboot()
        result = os.statvfs('/')
        giga = 10**9
        db_size = os.path.getsize(Db.path) / giga
        sdFree_size = result.f_bfree * result.f_frsize / giga
        if sdFree_size < variables.db_left_size or db_size > variables.db_max_size:
            Db.clear_old()

    convergence()

    if St.get("is_raspberry"):
        start_time = time.time()
        while time.time() - start_time <= variables.auto_reboot:
            time.sleep(1)
        os.system("sudo reboot")
