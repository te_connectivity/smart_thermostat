Raspbian setup
=======================================

Raspbian OS
-----------
What is it?
~~~~~~~~~~~
Raspbian is a Debian-based computer operating system for Raspberry Pi. 
There are several versions of Raspbian including Raspbian Buster and Raspbian Stretch. 
Since 2015 it has been officially provided by the Raspberry Pi Foundation as the primary 
operating system for the family of Raspberry Pi single-board computers.
Raspbian was created by Mike Thompson and Peter Green as an independent project.
The initial build was completed in June 2012.
The operating system is still under active development. 
Raspbian is highly optimized for the Raspberry Pi line's low-performance ARM CPUs.

Versions
~~~~~~~~~
Raspbian is declined in 3 different versions.

.. list-table:: Rasbian versions
  :align: center

  * - **Version**
    - **Packages**
    - **User interface**
    - **Boot time**
  * - Lite
    - |o|
    - |o|
    - 100 % (45 s)
  * - Desktop
    - |o|
    - |x|
    - 150 %
  * - Noob
    - |x|
    - |x|
    - 180 %

|pic_step_1_ras|

For optimisation reasons, we will choose the ``Lite version``.

Download
--------
First thing to do is download Raspbian from Foundation’s website. 

You can download ``Raspbian Buster Lite`` as **zip file** by clicking 
`HERE <https://downloads.raspberrypi.org/raspbian_lite_latest>`_.
