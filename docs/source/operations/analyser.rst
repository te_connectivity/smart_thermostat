Analyser loop
=======================================

Goal
----

The goal of this algorithm is to determine the average derivatives
and to predict the date (day and time) of the next actuator change for each sensor.

How it works
------------

There are many steps used to calculate the date of the next actuator change step:

.. image:: /_static/gif_scheme_graph_filter.gif
    :align: right

Steps:
    - **Step 0** : Get the sensor's measure history since last actuator action
    - **Step 1** : Transform to monotonic data
    - **Step 2** : Remove constant values at the beginning
    - **Step 3** : Fit a custom function to remaining data points
    - **Step 4** : Extend the function until it crosses the next objective value
    - **Step 5** : Get cross time
    - **Step 6** : Compare with last data's date

Get the sensor's measure history since last actuator action
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: /_static/scheme_graph_filter_step0.svg
    :align: right

An SQL request gets all the measurements associated with a sensor since
the last action of an actuator that influenced the sensor.

.. code-block:: sql
    :linenos:

    SELECT value
    FROM measure
    WHERE
        id_sensor = '{0}' AND
        timeDate > '{1}'
    ORDER BY timeDate ASC;

Transform to monotonic data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: /_static/scheme_graph_filter_step1.svg
    :align: right

The algorithm must fit its function to monotonic data only.

This implies that the algorithm should discard all data recorded before the
last change in the sign of the derivative.

This is achieved with a double python while loop.

Remove constant values at the beginning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: /_static/scheme_graph_filter_step2.svg
    :align: right

When data is ordered in ascending order (ASC), this step removes all repeating
data at the beginning. This improves the precision of the final estimation by an
average of 20%.

In effect, the algorithm tries to fit a curve to a list of two-dimensional data points,
removing constant measurements at the beginning improves the fit.

This functionality is integrated in the double while loop of **Step 2**


Fit a custom function to remaining data points
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: /_static/scheme_graph_filter_step3.svg
    :align: right

The algorithm now fits the following function to the remaining measurements
by changing parameters a, b, and c.
This is done with the function ``scipy.optimize.curve_fit`` of scipy.

.. math::

    f(x, a, b, c) = a + b \times x^{min(1,c)}

Curve
    If the system is far from the objective, the curve is concave and the estimation has to be a line.
    When the system is close to the objective, the curve is convex and the estimation is perfect.

Initialisation
    The initialisation of coefficients is crucial to find the right solution and reduce the calculation time.

    - **a** is the offset and it is initialised at the value of the last measure.
    - **b** is initialised at 1.
    - **c** is initialised between 0.1 and 1 and gradually increased by 10% each iteration.

Extend the function until it crosses the next objective value
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: /_static/scheme_graph_filter_step4.svg
    :align: right

Once the function has been generated, it is extended along the X axis to predict the future sensors measurements.

Step 3 returns the coefficients a, b, and c from which the best fit function f may be found.

We are therefore able to calculate the value of f for future time values and check when it will reach a given value.

Get cross time
~~~~~~~~~~~~~~

.. image:: /_static/scheme_graph_filter_step5.svg
    :align: right

Many conditions are checked in order to avoid an infinite calculation time.

A counter that can't go over 10000 iteration.

This implies that the maximum calculation time is 24 hours.

.. math::

    max_\mathrm{time} = \tfrac{max_\mathrm{iterations}}{FPS_\mathrm{sensor}}

Compare with last data's date
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: /_static/scheme_graph_filter_step6.svg
    :align: right

Finally the returned value is simply the difference between the date of the intersection of
the function with the objective and the last measurement's date.

The returned value is a time in second, but the value stored in the database is a date.
It allows to update the time to target on the UI without constantly making this computation.

The average derivative is also stored in order to give useful information to the user.

Examples
--------
.. list-table:: Analyser examples
   :align: center

   * - |graph_analyser_1|
     - |graph_analyser_2|
   * - 	.. centered:: AC - Far from target
     -  .. centered:: AC - Close to target
   * - |graph_analyser_3|
     - |graph_analyser_4|
   * - 	.. centered:: HE - Far from target
     - 	.. centered:: HE - close to target

Loop code
---------
.. literalinclude:: ../../../analyser.py
    :emphasize-lines: 1
    :language: python
    :linenos:
    :pyobject: loop
