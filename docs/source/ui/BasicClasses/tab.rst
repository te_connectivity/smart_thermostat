The Tab Class
===================

This class is used to display a tab: a box with text in the centre, that triggers an action when clicked.
This class is created from a Box and Text object previously initialised. It may also have a subtext.

__init__ method
---------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Tab.__init__
    :linenos:
    :language: python

List of attributes
^^^^^^^^^^^^^^^^^^^^^^

* *self.box*: This is a box object as previously defined.

* *self.text*: This is a text object as previously defined, it will be the main text displayed in the centre of the box.

* *self.action*: This is a callable function that is called whenever a touch is detected as inside the tab's box.

* | *self.sub_text*: This is a list of text strings that will serve as the text's attribute of a new text object.
  | The idea is that this piece of text is displayed as extra information, between the bottom of the tab's box
  | and the bottom of the main text. Note: this argument is optional and is ``None`` by default. If it is None,
  | no subtext is displayed. The new text object created will have the same font and font colour as the main text.

.. note::
    1. | This class is simpler than others in its initialisation: it lacks surface, pos and size arguments which are deduced
       | from the box and text object. This implies that the box object should have its surface, pos, size and colours defined
       | *before* it is used to define a text object.

    2. | This structure implies that to find out the size of a tab object, one should use tab_object.box.size,
       | **NOT** tab_object.size.

    3. | The main text's position is recalculated from the box parameters so that it is centred. Therefore, there is no
       | use in setting up the pos attribute of the *self.text* attribute and it is generally given the value (0, 0).

show method
---------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Tab.show
    :linenos:
    :language: python

This method first checks that the main text fits inside the box. If it is not the case, the
font size of the main text is reduced until it fits (using a while loop).
The new position of the top left corner of the main text is calculated so that the main text is centred
with respect to the box. The box and the text are then displayed to the screen.

If *self.sub_text* is not None, that is if the Tab object is to have a sub text, the font size of this
sub text is also calculated to fit in its place. The position of the subtext object is calculated
to centre it and it is displayed to the screen.

act method
-------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Tab.act
    :linenos:
    :language: python

This method takes the coordinate of the last touch as an argument.
It checks whether the coordinates correspond to a point that is within the rectangle spanned
by the box object with the booleans ``in_width`` and ``in_height``. If these are both ``True``
and if *self.action* is a callable function, then *self.action* is called with the line
``self.action()``.

.. note::
    Checking if *self.action* is callable allows to have a tab object that does not call any action when clicked,
    simply define *self.action* as a non-callable object, like the string ``"action"``.

    If the box of a tab object has rounded corners, the "touch zone" that triggers the Tab's action
    will still be a rectangle with right angled corners.

Final example
-----------------

.. code-block::
    :linenos:

    example_box = Box(self.surface, (100, 100), (150, 90), variables.green, variables.light_blue0,
                  rounding=0.5, line_colour= variables.black, line_width=1)
    example_text = Text(self.surface, (50, 50), ["1st line of text", "2nd line of text"],
                        self.font_name_bold_italic, 20, variables.black)
    example_tab = Tab(example_box, example_text, "action", ["subtext"])
    example_tab.show()

This code produces:

.. image:: /_static/example_arrow.png
    :align: center
    :alt: alternate
