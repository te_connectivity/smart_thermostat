.. _sd_setup:

SD card setup
=======================================

Avalaible software
------------------
In this step image writer to write the downloaded OS will be necessary. For this purpose you can use:

.. list-table:: SD writing software
  :align: center

  * - **Software**
    - **Windows**
    - **Linux**
    - **Mac OS**
    - **Portable**
    - **Size**
  * - `Etcher <https://www.balena.io/etcher/>`_
    - |x|
    - |x|
    - |x|
    - |x|
    - 139 MB
  * - `Rufus <https://rufus.ie/>`_
    - |x|
    - |o|
    - |o|
    - |x|
    - 1.1 MB
  * - `Win32 Disk Imager <https://sourceforge.net/projects/win32diskimager/>`_
    - |x|
    - |o|
    - |o|
    - |o|
    - 12 MB

We recommand to use Etcher for his compatibility with every OS.

Write on your SD card
----------------------
Bellow the steps to write ``Raspbian Lite OS`` on your SD card with your computeur.

.. note:: Make sure you computer has detected your SD card.

Steps:
    - Plug your **SD card** into your computer
    - Run your **SD writing software**
    - Select your unzipped **Raspbian image** as image
    - Select your **SD card** as drive
    - Clip on the **Flash** button

Wireless configuration
----------------------

Enabling SSH
~~~~~~~~~~~~

.. list-table::
  :align: center

  * - |gif_step_3_ras|

In order to connect to Raspberry Pi we need to enable SSH communication.
To do it, please open SD card disc (BOOT) and create **ssh** ``file`` without any extension.

.. warning::
    On windows, make sure the file extension is not ``----.txt``.
    You can display extensions by checking the box
    ``view > File name extensions`` in your folder brower.

Wifi configuration
~~~~~~~~~~~~~~~~~~
If you're using local wireless network you need to add file named **wpa_supplicant.conf** with code above inside.


.. code-block:: console
    :linenos:

    country=YOUR_COUNTRY_ABREVIATION
    update_config=1
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev

    network={
        ssid="Your network name/SSID"
        psk="Your WPA/WPA2 security key"
        key_mgmt=WPA-PSK
    }

.. note::
    * ``YOUR_COUNTRY_ABREVIATION`` is your country abreviation :download:`country_abreviation.txt <../../_downloads/country_abreviation.txt>`.
    * ``ssid`` is the name of your network.
    * ``psk`` is the password of your network.


Serial configuration (optional)
-------------------------------
This step is here if you want to communicate with your raspberry Pi through an USB wire.

Enable Serial
~~~~~~~~~~~~~
You need to edit **cmdline.txt**. Clear the file and past this code to it:

.. code-block:: console
    :linenos:

    dwc_otg.lpm_enable=0 console=serial0,115200 console=tty3 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline rootwait modules-load=dwc2, g_ether fbcon=map:10 fbcon=font:ProFont6x11 logo.nologo vt.global_cursor_default=0 quiet

Set overlay as dwc2
~~~~~~~~~~~~~~~~~~~
You need to edit **config.txt**. At the very end of the file, add a line with:

.. code-block:: console
    :linenos:

    dtoverlay=dwc2

USB connection
~~~~~~~~~~~~~~

.. list-table:: Now Eject your SD Card and take the USB cable and plug into the micro USB connector (not Power)
   :align: center

   * - |pic_step3_workshop|

SSH connection
~~~~~~~~~~~~~~

.. list-table:: After about 2 minutes, SSH into the Pi with the hostname raspberrypi.local
   :align: center

   * - |pic_step4a_workshop|
   * - |pic_step4b_workshop|

Set a unique hostname
~~~~~~~~~~~~~~~~~~~~~

.. list-table:: Enter the following command
   :align: center

   * - |pic_step5_workshop|

.. list-table:: Now navigate to the following screen:
   :align: center

   * - |pic_step6a_workshop|
   * - |pic_step6b_workshop|

.. list-table:: Affect a unique name to your raspberry
   :align: center

   * - |pic_step6c_workshop|

.. list-table:: Change the password
   :align: center

   * - |pic_step6d_workshop|

Reboot your raspberry
~~~~~~~~~~~~~~~~~~~~~

.. list-table:: Now click **Yes** to reboot
   :align: center

   * - |pic_step6e_workshop|

Now remove the USB cable and connect the power supply to the PWR connector on the PI.
You will now be able to ssh into yoru Pi via the hostname you assigned.

Plug your SD card
-----------------

.. list-table:: Plug the SD card in the Raspberry pi
  :align: center

  * - |pic_step_5_con|

Remove the SD card from your computer and plug it in the **raspberry Pi**, then connect it to the **5V power supply**.

.. warning:: SD cards are very fragile.
