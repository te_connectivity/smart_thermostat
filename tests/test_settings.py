import inspect
import os
import sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))  # Get current dir
parentdir = os.path.dirname(currentdir)  # get parent dir
dir = parentdir + "/"
sys.path.insert(0, dir)  # insert path of parent dir in import settings

from convergence import *

class Test(unittest.TestCase):

    def setUp(self):
        St=settings.Settings()
        St.set("is_unittest",True)
        self.Pub = pubsub.Pubsub()
        self.Db = db.DB()
        self.target = 10**3
        self.outer = 10**2
        self.out = 10**1
        
    def test_settings_pos(self):
        self.delta=1.1
        self.Db.clear_bdd()
        self.Db.insert_sensor("test_sensor", self.target, self.outer, self.out)
        self.id_sensor = self.Db.get_sensor_id("test_sensor")
        self.Db.add_setting(self.id_sensor, self.delta, 0, 24, 255, 4095)
        self.assertEqual(self.Db.get_sensor_target_delta(self.id_sensor)==self.target+self.delta,True)
    
    def test_settings_neg(self):
        self.delta=-1.1
        self.Db.clear_bdd()
        self.Db.insert_sensor("test_sensor", self.target, self.outer, self.out)
        self.id_sensor = self.Db.get_sensor_id("test_sensor")
        self.Db.add_setting(self.id_sensor, self.delta, 0, 24, 255, 4095)
        self.assertEqual(self.Db.get_sensor_target_delta(self.id_sensor)==self.target+self.delta,True)
    
    

if __name__ == '__main__':
    unittest.main()
