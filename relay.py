''' 
   Copyright 2020 TE Connectivity
   Distributed under MIT license.
   See file LICENSE for detail or copy at https://opensource.org/licenses/MIT
'''

def loop():
    import sys
    import time
    import db
    import pubsub
    import settings
    import variables
    name = __name__
    Pub = pubsub.Pubsub()  # Initialize communication object
    Db = db.DB()  # Initialize data storage object
    St = settings.Settings()  # Initialize variable storage object
    if not St.get("is_unittest"):
        actuators = Db.reset_actuators_state()  # Initialize relay to 0 state
    Pub.get(__name__)  # Initialize comminication channel __name__
    if St.get("is_raspberry"):
        import RPi.GPIO  # Import RPi.gpio module
        RPi.GPIO.setmode(RPi.GPIO.BCM)  # Set BCM mode
    list=Db.get_actuators_gpio_value()  # Get all (pin, state, id, name) actuators
    for element in list:
        gpio = int(element[0])
        state = int(element[1])
        id_actuator = int(element[2])
        name = element[3]
        # Db.set_actuator_value(id_actuator,0)  # Initialize all actuators to 0 in the database
        if St.get("is_raspberry"):
            RPi.GPIO.setup(gpio, RPi.GPIO.OUT)  # Initialize GPIO state
            RPi.GPIO.output(gpio, 0)
    time_loop = time.time()  # Start time loop (To regulate max FPS)
    while 1:
        time.sleep(St.get("is_raspberry") * max(0, 1 / float(variables.FPS_relay) - (time.time() - time_loop)))  # Sleep time to not trigger the script more than the FPS ratio fixed
        time_loop = time.time()  # Start time loop (To regulate max FPS)
        if Pub.get(__name__) or __name__ == "__main__" or not(St.get("is_raspberry")):  # triggered or test from computer (from convergence.py or from this file)
            list = Db.get_actuators_gpio_value()  # Get all (pin, state, id, name) actuators
            for element in list:
                gpio = int(element[0])  # Gpio pin
                state = int(element[1])  # gpio state
                id_actuator = int(element[2])  # Unique id in the database of the actuator
                name = element[3]  # Name of the actuator
                if St.get("is_raspberry"):
                    try:
                        if state != RPi.GPIO.input(gpio):
                            RPi.GPIO.output(gpio, state)  # Set gpio state (3.3V in the relay wire to trigger the hardware relay)
                    except RuntimeError:
                        RPi.GPIO.setup(gpio, RPi.GPIO.OUT)
                        RPi.GPIO.output(gpio, state)
                if St.get("is_unittest"):
                    if gpio == 1 and state == 1:
                        Pub.set("relay_test1_pass")
        if St.get("is_unittest") or __name__ == "__main__":
            break
if __name__ == "__main__":
    import settings
    St = settings.Settings()  # Initialize variable storage object
    St.set("is_unittest", False)
    import convergence
    loop()
