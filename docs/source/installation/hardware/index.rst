.. _hardware_setup:

Hardware setup
=======================================

.. toctree::
   :caption: Content:

   Connections <connections>
   Printing the case <printing>
   Mounting <mounting>
