''' 
   Copyright 2020 TE Connectivity
   Distributed under MIT license.
   See file LICENSE for detail or copy at https://opensource.org/licenses/MIT
'''

# Colours
white = (255, 255, 255)
black = (0, 0, 0)
red = (255, 0, 0)
green = (0, 255, 0)
light_green = (70, 185, 125)
blue = (0, 0, 255)
te_orange = (231, 131, 0)
opposite_te = (256 - te_orange[0], 256 - te_orange[1], 256 - te_orange[2])
te_case = (218, 146, 132)
light_blue0 = (0, 150, 255)
light_blue1 = (0, 170, 255)
light_blue2 = (0, 190, 255)
light_blue3 = (0, 210, 255)
light_blue4 = (0, 230, 255)
dark_grey = (100, 100, 100)
light_grey = (200, 200, 200)
light_red = (230, 200, 200)
light_yellow = (250, 250, 200)
grid_colour = (160, 210, 240)

# UI
av_day = 15
av_week = 100
av_month = 400
back_to_default = 2 * 60  # time before default screen shown in seconds
default_te = 20 * 60  # time after which te_logo appears on default in seconds, set to 0 to never display te_logo
time_format = "%H:%M" # Time format for clock object, set to HH:MM format by default
# replace with "%I:%M %p" for HH:MM XM format
date_format = "%d/%m" # Date format for clock object, set to dd/mm format y default
# replace with "%m/%d" for mm/dd format
# For all date formats other that dd/mm and mm/dd, add an if condition in the graphs method
# of the WhatToShow class, so that it is correctly displayed on the time axis of graphs.
# Check out the available abbreviations on https://docs.python.org/3/library/time.html#time.strftime

screen_dimensions = (320, 240)  # in pixels and in the form (width, height)
default_property = "Temperature"
grid = True

# checking if ambimate sensor is connected
ambimate_visible = True

# FPS
FPS_gui = 30
FPS_relay = 1
FPS_sensor = 1 / 60
FPS_controller = 1 / 90
FPS_analyser = 1 / 90
#Defaults
sensors={
    "Temperature": [22, 0.6, 1, "°C"],
    "Humidity": [50, 5, 10, "%"],
    # "CO2": [750, 100, 250, "ppm"],
    # "VOC": [1000, 200, 300, "ppb"],
    # "Light": [0, 0, 0, "lx"],
    # "Audio": [0, 0, 0, "dB"]
}
actuators={  # "name": [gpio, [["sensor1", up, backup], ["sensor2", up, backup]]]
    "AC": [26, [["Temperature", False, False]]],
    "HE": [13, [["Temperature", True, False]]],
    # "HE+": [6, [["Temperature", True, True]]],
    "FAN": [5, [
        ["Humidity", False, False],
        # ["CO2", False, False],
        # ["VOC", False, False]
    ]]
}
settings={
    "AUTO": True,
    "is_raspberry": True,
    "is_unittest": False,
    "temp_unit": "°C"
}
backup_steps=10

#Memory
db_left_size = 5  # How many place do you want to keep on your SD card
db_max_size = 5  # How many place do you want to keep on your SD card
auto_reboot = 60 * 60 * 24  # In second, time for reboot
remaining_data = 3600 * 24 * 31 # variable to indicate deleting data older than (in seconds)
