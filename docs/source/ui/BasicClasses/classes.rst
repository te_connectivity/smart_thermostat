Basic User Interface (UI) objects
========================================

Introduction
---------------

The UI is based on the pygame module of python.
This module contains the necessary fundamentals for a UI, such as ways to draw
elementary shapes (lines, rectangles, circles and triangles) or display text.

Our UI has two components:

1. New basic objects (classes) that have the desired properties, that will be used
extensively throughout the UI

2. One new "super class" that uses all the new objects created above and make them interact
with one another and the user

We now dive into the documentation of all our new basic objects.

List of basic classes and their overall use
---------------------------------------------

* | **Box**: Used to draw a box on the screen. The box may have an horizontal colour gradient,
  | as well as rounded corners.

* | **Text**: Used to have text displayed on the screen. One may choose the font (which can be italic or bold),
  | as well as its size, colour. Useful to display many lines of text at once.

* | **Tab**: Used to display a tab: a box with text in the centre, that triggers an action when clicked.
  | This class is created from a Box and Text object previously initialised. It may also have a subtext.

* **Image**: Used to display an image on the screen. Possibility to scale it down without affecting the relative dimensions.

* **Line**: Used to display a line with a certain colour, width, density and dash size on the screen.

* **Clock**: Used to display the current local time or date, in a given time and date format.

* | **Arrow**: Used to draw an isosceles triangle pointing in one of the 4 following directions:

             * Vertically upwards ("North")
             * Horizontally to the right hand side ("East")
             * Vertically downwards ("South")
             * Horizontally to the left hand side ("West")

  | It may also trigger an action when clicked. Note that, in essence, an Arrow object is like a Tab object,
  | except it does not have text on it and has a different shape.

* | **Radio**: Used to display a radio on the screen. Radio objects are used in situation when the user chooses
  | one option among mutually exclusive options.

* | **CheckBox**: Used to display a checkbox on the screen, which may be checked or unchecked and with a label to its right.
  | Checkboxes mainly become handy as part of a CheckList object.

* | **CheckList**: This class uses a box object and a list of Checkbox objects. It works in a similar fashion to Radio objects,
  | except that more than one item in the list may be selected at once and that the associated action does not
  | necessarily take effect immediately. If the checklist is too long for the given box, arrow objects are added at
  | its top and its bottom to allow the user to browse between the different checklist pages. The page number is
  | displayed in the top right corner of the box.

Features common to several basic classes
----------------------------------------------

The surface attribute
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Most classes have *surface* as their first __init__ argument (except those that use objects that already do).
The pygame `"newbie guide" <https://www.pygame.org/docs/tut/newbieguide.html>`_ describes the concept of pygame surfaces well:
a blank piece of paper on which we may draw.
In our case, this surface is always the screen surface, but this structure allows more flexibility for future uses,
as well as the possibility to initialise the screen surface much later.

About colours
^^^^^^^^^^^^^^^^^

If a *colour* argument is required the value given is a 3-valued tuple. This follows the `RGB colour model <https://en.wikipedia.org/wiki/RGB_color_model>`_:
in pygame, the maximum value for each item is 255. For example, (255, 0, 0) corresponds to the brightest red.
In this project, all colour tuples are stored in the variables.py file and called by their colour names.

.. note::
    In pygame, colours have `24-bit depth <https://en.wikipedia.org/wiki/Color_depth#True_color_(24-bit)>`_: The level of red for each colour is coded as one byte (8 bits)
    and the same goes for the level of green and blue. This allows to render
    :math:`2^{24} = 16 777 216` different colours!

    However, the Waveshare 2.8 inch touchscreen for Raspberry Pi that this project is intended to work with
    uses `16-bit high colour <https://en.wikipedia.org/wiki/High_color>`_,
    which can "only" render :math:`2^{16} = 65 536` colours.

    The conversion from 24-bit depth to 16-bit depth is done in the refresh() method using the convert() method of surface objects.
    However, differences will inevitably exist between the image seen on a computer screen and that seen on the Waveshare screen.
    In particular, for colour gradients, the Waveshare screen may display several thin bands of uniform colours,
    rather than a continuously changing gradient.

The pos and size attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Many classes have *pos* and *size* as init arguments. These stand for the position and the size of a given object, respectively.
They are both `tuples <https://www.w3schools.com/python/python_tuples.asp>`_ of the form (x_value, y_value)

.. note::

    .. image:: /_static/screen.svg
        :align: center
        :alt: alternate text

    From the user's perspective, position on the screen (in pixel) is defined with the
    positive X direction to the *right*, and the positive Y direction *downwards*.

The pos tuple generally defines the position of the *top-left* corner of an object.

The size tuple generally defines the difference between:

    * the rightmost and the leftmost pixels of an object for its first value

    * the downmost and the upmost pixels of an object for its second value

The show method
^^^^^^^^^^^^^^^^^^^

All our basic classes have a method called "show". As you may guess, when this method is called, it writes to
the surface attribute (self.surface), so that on the next refresh of the screen, an object is displayed to the user.

.. note::
    You might wonder why this needs to be a separate method that needs to be called each time an object needs to be displayed.
    This allows  greater flexibility: we sometimes wish to simply initialise an object and position it with respect
    to other objects before showing it (for example, when we centre text), or we might even only use the object as a parent object to another one
    (like CheckBox objects that are only shown as part of CheckList objects).

The act method
^^^^^^^^^^^^^^^^^^

All objects that are meant to trigger an action when clicked (from now on nicknamed **active objects**) have a method called act that takes coordinates
as its argument. These methods all work in a similar fashion: if the coordinates entered as arguments are within the pixel space occupied by
the active object, then the action to which it is linked (which is really a python function or method) is then called and executed.

For example, consider a Tab object that has the following attributes:

    * A blue Box object with attributes:

        * pos = (50, 40)

        * size = (150, 100)

    * An action ``go_green`` that changes the box colour to green.

When a touch is detected on the screen, the touch coordinates are stored as :math:`x_{touch}` and :math:`y_{touch}`.
The act method is triggered and takes the touch coordinates as arguments.
If

.. math::
    \begin{cases}
      50 < x_{touch} < 200\\
      40 < y_{touch} < 140
    \end{cases}

then the ``go_green`` function is called as ``go_green()`` and the colour
attribute of the Box object is changed to green.

.. note::
    The fact that all active objects share a common name and structure for the act method
    means that we are able to append all active objects within a menu to a python list named
    ``menu_active_objects``.
    At the end of the menu, we may then iterate through that list and tell each active object to "act":

    .. code-block:: python
        :linenos:

        for active_object in menu_active_objects:
            active_object.act(coord)
