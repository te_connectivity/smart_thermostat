.. _connection:

Connections
=======================================
.. centered::
  |gif_connections|

This guide will take you through the steps to assemble the basic
components of your TE Smart Thermostat.

Here is the overview of connections:

|scheme_cablage|
