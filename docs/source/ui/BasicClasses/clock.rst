The Clock Class
==================

This class is used to display the current local time or date, in a given time and date format.

__init__ method
----------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Clock.__init__
    :linenos:
    :language: python

List of attributes :
^^^^^^^^^^^^^^^^^^^^^^^^

* *self.surface*: As discussed previously this is generally the screen surface.

* | *self.time_format*: This is the time format that will be used by the strftime function
  | of the time module. Visit `time.strftime <https://docs.python.org/3/library/time.html#time.strftime>`_ for a list of acceptable abbreviations.
  | For TE smart thermostat UI, the time format is generally set to ``"%H:%M"`` by default, corresponding to HH:MM format.
  | Modify its value in **variables.py** to change the displayed time format.

* | *self.date_format*: Similar to *self.time_format* but for the date format.
  | For TE smart thermostat UI, the date format is generally set to ``"%d/%m"`` by default, corresponding to dd/mm format.
  | Modify its value in **variables.py** to change the displayed date format.

* *self.time*: A string of the current local time in the chosen time format, obtained with ``time.strftime``.

* *self.date*: A string of the current local date in the chosen time format, obtained with ``time.strftime``.

* | *self.font_name*: The relative path of the font file used when displaying time and date,
  | refer to the Text Class documentation for further information.

* *self.font_size*: The font size used for displaying the text.

* | *self.time_text*: A Text object that can be displayed on the screen, showing *self.time*.
  | Note that the pos attribute is set to 0, and the font colour to black. This does not
  | matter as these parameters are set later in the show_time and show_date methods.

* *self.date_text*: Similar to *self.time_text*, a Text object that can be displayed on the screen, showing *self.date*.

.. note::

    *self.time*, *self.date*, *self.time_text*, and *self.date_text* only appear in the
    __init__ method for clarity, but there is no other use in having them here.

show_time method
----------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Clock.show_time
    :linenos:
    :language: python


List of arguments:
^^^^^^^^^^^^^^^^^^^^

* pos: The position of the top left corner of *self.time_text*  when displayed.

* | font_colour: The colour of the font of *self.time_text* when displayed.
  | Note that it is set to ``variables.black`` by default.

Code flow
^^^^^^^^^^^^^^^^

*self.time* is first recalculated so that the time displayed corresponds to the current local time.
*self.time_text* is then recreated. Note that this time, the pos and font_colour attributes
correspond to what will actually be displayed.

Lastly, the show method of *self.time_text* is called making the text appear on the screen.

show_date method
-----------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Clock.show_date
    :linenos:
    :language: python

This method works just like the show_time method (see above) to display the date.

Final example
---------------


.. code-block:: python
    :linenos:

    example_clock = Clock(self.surface, self.time_format,
                          self.date_format, self.font_name, 15)
    example_clock.show_time((50, 50))
    example_clock.show_date((100, 150), variables.light_blue0)

Provided that all variables are correctly defined, at 03:07 PM on the 16th of September,
this code produces:

.. image:: /_static/example_clock.png
    :align: center
    :alt: alternate
