Pubsub
=======================================
Introduction
--------------
From `Wikipedia <https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern>`_
In software architecture, publish–subscribe is a messaging pattern where senders
of messages, called publishers, do not program the messages to be sent directly to
specific receivers, called subscribers, but instead categorise published messages
into classes without knowledge of which subscribers, if any, there may be.
Similarly, subscribers express interest in one or more classes and only receive
messages that are of interest, without knowledge of which publishers, if any, there are.

Publish–subscribe is a sibling of the message queue paradigm,
and is typically one part of a larger message-oriented middleware system.
Most messaging systems support both the pub/sub and message queue models in their API,
e.g. Java Message Service (JMS).
This pattern provides greater network scalability and a more dynamic network topology,
with a resulting decreased flexibility to modify the publisher and the structure of
the published data.


Use
---
In the convergence project, pubsub is used for 3 purposes:
- To trigger a loop
- To refresh settings of the .json file
- To refresh what is displayed on the UI

Loop trigger
~~~~~~~~~~~~
``{Loop code}`` is triggered automatically when a database request is made in db.py

.. list-table:: See the corresponding information trigger below
   :align: center

   * - |scheme_trigger_path|

Settings refresh
~~~~~~~~~~~~~~~~~
Every time a value is modified, it triggers the pubsub on the channel with a name corresponding to the value.

When a setting value is checked, it first checks the pubsub channel and if a message has been send,
it reads the file setting.json and refreshes the settings dictionary.

Object code
---------------------------
.. literalinclude:: ../../../pubsub.py
    :emphasize-lines: 1
    :language: python
    :linenos:
    :pyobject: Pubsub
