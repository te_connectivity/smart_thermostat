''' 
   Copyright 2020 TE Connectivity
   Distributed under MIT license.
   See file LICENSE for detail or copy at https://opensource.org/licenses/MIT
'''

def loop():
    import time
    import db
    import pubsub
    import settings
    import variables
    name = __name__
    Pub = pubsub.Pubsub()  # Initialize communication object
    Db = db.DB()  # Initialize data storage object
    St = settings.Settings()  # Initialize variable storage object
    Pub.get(name)  # Initialize comminication channel __name__
    last_ups = [[] for i in range(len(Db.get_sensors_id()))]  # Initialize the up_memory list
    time_loop = time.time()  # Start time loop (To regulate max FPS)
    while 1:  # Infinite loop start
        time.sleep(St.get("is_raspberry") * max(0, 1 / float(variables.FPS_controller) - (time.time() - time_loop)))  # Sleep time to not trigger the script more than the FPS ratio fixed
        time_loop = time.time()  # Start time loop (To regulate max FPS)
        if (Pub.get(name) or __name__ == "__main__") and St.get("AUTO") or not(St.get("is_raspberry")):  # triggered or test from computer (from convergence.py or from this file)
            sensors = Db.get_sensors_id()  # Get all sensor id
            actuators = Db.get_actuators_id()
            states = {i:0 for i in actuators}
            for i in range(len(sensors)):
                id_sensor = sensors[i]
                m = Db.get_sensor_value(id_sensor)  # Get value of the sensor
                target = Db.get_sensor_target_delta(id_sensor)  # Get target of the sensor
                outer = abs(Db.get_sensor_outer(id_sensor))  # Get first hysteresis relative-to-target value
                out = abs(Db.get_sensor_out(id_sensor))  # Get second hysteresis relative-to-target value
                out_inf = target - outer - out  # Calcul the lower threashold
                out_sup = target + outer + out  # Calcul the higher threashold
                outer_inf = target - outer  # Calcul the second lower threashold
                outer_sup = target + outer  # Calcul the second higher threashold

                if target == 0 and outer == 0 and out == 0:  # Default value, not configurate
                    continue
                # Calcul the actual "area" of the sensor
                is_out_inf = m < out_inf
                is_outer_inf = m >= out_inf and m < outer_inf
                is_in_inf = m >= outer_inf and m < target
                is_in_sup = m >= target and m <= outer_sup
                is_outer_sup = m > outer_sup and m <= out_sup
                is_out_sup = m > out_sup

                try:
                    last_up = last_ups[i]  # Get the last state (up or down)
                except IndexError:  # If sensors has been added
                    last_ups = [[] for i in range(len(Db.get_sensors_id()))]  # Initialize the up_memory list
                    last_up = last_ups[i]  # Get the last state (up or down)
                if last_up == []:
                    last_up = m <= target  # Default last state at the raspberry boot
                list_id_actuators = Db.get_actuator__sensors_id(id_sensor)  # Get actuators id
                for id_actuator in list_id_actuators:
                    _state_actuator = bool(int(Db.get_actuator_state(id_actuator)))  # Get actuator state
                    up = Db.get_actuator_up(id_actuator)  # Get possible action of the actuator
                    backup = Db.get_actuator_backup(id_actuator) # Get if actuator is a backup
                    help = False # Temps boolean for check activation of backup actuator
                    if is_out_inf:  # If far bellow the target
                        if up: # if actuator will increase measure
                            if backup:
                                help = True
                            else:
                                state = True
                        else: # if actuator will decrease measure
                            state = False
                    elif is_outer_inf:  # If close bellow the target
                        if up: # if actuator will increase measure
                            if last_up:  # if sensor in up mode
                                if backup:  # if actuator is a backup
                                    help = True
                                else:  # if actuator not a backup
                                    state = True
                            else:  # if sensor in down mode
                                state = False
                        else:  # if actuator will decrease measure
                            state = False
                    elif is_in_inf:  # If very close bellow the target
                        if backup:
                            help = _state_actuator
                        else:
                            state = _state_actuator
                    elif is_in_sup:  # If very close above the target
                        if backup:
                            help = _state_actuator
                        else:
                            state = _state_actuator
                    elif is_outer_sup:  # If close bellow the target
                        if up:
                            state = False
                        else:
                            if last_up:
                                state = False
                            else:
                                if backup:
                                    help = True
                                else:
                                    state = True
                    elif is_out_sup:  # If far above the target
                        if up:
                            state = False
                        else:
                            if backup:
                                help = True
                            else:
                                state = True
                    if help:
                        if _state_actuator:
                            state = True # If backup already active and needed
                        else:
                            n = variables.backup_steps
                            Y = Db.get_measures_date_sensor(id_sensor, time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time() - n / variables.FPS_sensor)))
                            if len(Y) < n / 2:
                                state = False
                            else:
                                derivative = 0
                                for j in range(len(Y) - 1):  # Y is a list containing the n last values of the sensor
                                    derivative += Y[j+1] - Y[j]
                                derivative /= len(Y) * variables.FPS_sensor #Average derivative of last 10 measures of the sensor
                                if derivative * (up * 2 - 1) > 0: # If derivative without backup is fine
                                    state = False  # Activate the backup actuator
                                else: # If derivative without backup not fine
                                    state = True  # Desactivate the backup actuator
                    if state != _state_actuator:
                        last_ups[i] = up  # Update last state of sensor
                    try:
                        states[id_actuator] = min(1, state + states[id_actuator])
                    except KeyError:
                        continue
            for id_actuator in states:
                _state_actuator = bool(int(Db.get_actuator_state(id_actuator)))
                if int(states[id_actuator]) != _state_actuator:
                    Db.set_actuator_value(id_actuator, int(states[id_actuator]))  # Set new state in the database
        if St.get("is_unittest") or __name__=="__main__":
            break
if __name__ == "__main__":
    import settings
    St = settings.Settings()  # Initialize variable storage object
    St.set("is_unittest",False)
    import convergence
    loop()
