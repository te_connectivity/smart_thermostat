Graphs
=========================

.. centered::
    |gif_graph|

Graph menu is used to display the historical average of the home property.
Graphs consist of four main parts:

  #. Axises,titles and types of graph
  #. Target boundaries
  #. Grid and axis ticks
  #. Curve

.. note::

   To show the graph after a few days of not using this option it takes some time. That's because the latest saved version of 
   graph points are not up-to-date anymore and they need the update. 

Full Code
---------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.graphs
    :linenos:
    :language: python

Axises, titles and types of graph
-------------------------------------

.. code-block:: python
    :linenos:

    # Drawing axis lines
    x_axis = Line(self.surface,
                  (self.width * 0.15, int(top_tab.box.size[1] + (self.height - top_tab.box.size[1]) * 0.8)),
                  (self.width * 0.95, int(top_tab.box.size[1] + (self.height - top_tab.box.size[1]) * 0.8)),
                  variables.black)
    y_axis = Line(self.surface,
                  (self.width * 0.15, int(top_tab.box.size[1] + (self.height - top_tab.box.size[1]) * 0.8)),
                  (self.width * 0.15, int(top_tab.box.size[1] + (self.height - top_tab.box.size[1]) * 0.1)),
                  variables.black)
    x_axis_length = x_axis.end_pos[0] - x_axis.start_pos[0]
    y_axis_length = y_axis.start_pos[1] - y_axis.end_pos[1]
    x_title = Text(self.surface, (0, 0), ["Time"], self.font_name_bold, 12, variables.black)
    x_title.pos = (x_axis.start_pos[0] + (x_axis_length - x_title.font_w) / 2, self.height - 1 - x_title.font_h)
    y_title = Text(self.surface, (0, 0), [self.home_property + " (" + self.list_sensor_dict[self.home_property]["unit"] + ")"],
                   self.font_name_bold, 12,
                   variables.black)
    y_title.pos = (max(y_axis.start_pos[0] - y_title.font_w / 2, int(0.05 * self.width)),
                   (top_tab.box.size[1] + y_axis.end_pos[1] - y_title.font_h) / 2)
    font_list = [self.font_name, self.font_name_bold]
    colour_list = [variables.light_green, variables.green]
    tab_texts = ["D", "W", "M"]
    tab_actions = [functools.partial(self.change_attribute, "graph", a) for a in tab_texts]
    # Drawing boxes for "D", "W" and "M"
    for i in range(len(tab_texts)):
        tab_box = Box(self.surface, (self.width / 2 + 40 * i, y_title.pos[1]), (30, y_title.font_h),
                      colour_list[0], colour_list[0], line_colour=variables.light_yellow)
        tab_text = Text(self.surface, (0, 0), [tab_texts[i]], font_list[0 + (self.graph == tab_texts[i])],
                        y_title.font_size, variables.white)
        graph_tab = Tab(tab_box, tab_text, tab_actions[i])
        graph_tab.show()
        graphs_active_objects.append(graph_tab)

.. code-block:: python
    :linenos:

    # showing axis and title
    x_axis.show()
    y_axis.show()
    x_title.show()
    y_title.show()

The main feature of the graph is that it changes according to home property.
The X-axis is always ``"Time"`` , but the Y-axis depends on the ``home property`` and in case of temperature also on a given unit.
This means that if you choose to have a temperature in Fahrenheit, on the graph it will be displayed using the same unit.

There are three types of graphs that can be displayed:
 - day (D)
 - week (W)
 - month (M)

 For each graph type, there is a different section in the average table.
 In ``variables.py`` you can change how many measures you want to group into one average value.

 .. note::

     The more precise average is, the more memory to store and time to process it takes.


Target boundaries
-----------------

.. code-block:: python
    :linenos:

    # creating outer_box and target_text
    if self.list_sensor_dict[self.home_property]["active"] and self.mode == "auto":
        actual_target = self.list_sensor_dict[self.home_property]["target"] + self.list_sensor_dict[self.home_property]["setting_delta"]
        y_pos = (y_axis_length * 0.9 / (max_y - min_y)) * (actual_target - min_y)
        target_line = Line(self.surface, (x_axis.start_pos[0], int(y_axis.start_pos[1] - 0.05 * y_axis_length - y_pos)),
                           (x_axis.end_pos[0], int(y_axis.start_pos[1] - 0.05 * y_axis_length - y_pos)),
                           variables.te_orange)
        outer = self.list_sensor_dict[self.home_property]["outer"]
        outer_inf_pos = (y_axis_length * 0.9 / (max_y - min_y)) * (actual_target - outer - min_y)
        outer_sup_pos = (y_axis_length * 0.9 / (max_y - min_y)) * (actual_target + outer - min_y)
        outer_box = Box(self.surface, (x_axis.start_pos[0],
                                       int(y_axis.start_pos[1] - 0.05 * y_axis_length - outer_sup_pos)),
                        (x_axis.end_pos[0] - x_axis.start_pos[0], outer_sup_pos - outer_inf_pos), variables.light_grey,
                        variables.light_grey, line_colour=None, )
        outer_box.show()
        # target_line.show()
        if self.home_property == "Temperature":
            val_str = str(round(self.temp_given_unit(actual_target) * 2) / 2)
        else:
            val_str = str(round(actual_target * 2) / 2)
        target_text = Text(self.surface, (0, 0),
                           ["Target : " + val_str + " " + self.list_sensor_dict[self.home_property]["unit"]],
                           self.font_name_bold_italic, 14, variables.te_orange)
        mid_y = (y_axis.start_pos[1] + y_axis.end_pos[1]) / 2
        if target_line.start_pos[1] > mid_y:
            target_text.pos = (int(0.75 * self.width - 0.5 * target_text.font_w),
                               int(outer_box.pos[1] - target_text.font_h))
        else:
            target_text.pos = (int(0.75 * self.width - 0.5 * target_text.font_w),
                               int(outer_box.pos[1] + outer_box.size[1]))
.. code-block:: python
    :linenos:

    # showing target text. We're doing it at the end to have it more visible (not covered by curve)
    try:
        target_text.show()
    except:
        print ("No target to show")

In auto mode on the graph apart from the curve, there will be a section connected to the target value of the home property.
It consists of two parts - area that shows target with its outer values and text with the exact target value.

.. note::

    Creating target_text and displaying it are separated in order to
    display it after all other parts of the graph (not overwrite it).

Grid and axis ticks
-------------------

.. code-block:: python
    :linenos:

    # Convert extrema of y to right unit, create and show y tics
    if self.home_property == "Temperature":
        min_y_axis = self.temp_given_unit(min_y)
        max_y_axis = self.temp_given_unit(max_y)
    else:
        max_y_axis = max_y
        min_y_axis = min_y
    scale = (max_y_axis - min_y_axis) / (y_axis_length * 0.95)  # in unit/ pixel °C/pixel [or °F/pixel]
    y_start_value = min_y_axis - 0.05 * y_axis_length * scale
    if_decimals = False
    grad = []
    if ((max_y_axis - min_y_axis) <= 1):
        grad_interval = 5
        step = 1
        value_grad = int(round(y_start_value,2)*100)
        if_decimals = True
    elif (1 < (max_y_axis - min_y_axis) <= 2):
        grad_interval = 50
        step = 10
        value_grad = round(y_start_value,1)*100
        if_decimals = True
    elif (2 < (max_y_axis - min_y_axis) <= 10):
        grad_interval = 1
        step = 0.5
        value_grad = round(y_start_value,0)
    elif 10 < (max_y_axis - min_y_axis) and (max_y_axis - min_y_axis) <= 20:
        grad_interval = 2
        step = 0.5
        value_grad = round(y_start_value,0)
    elif 20 < (max_y_axis - min_y_axis) <= 50:
        grad_interval = 5
        step = 1
        value_grad = round(y_start_value,0)
    elif 50 <= (max_y_axis - min_y_axis) <= 100:
        grad_interval = 10
        step = 5
        value_grad = round(y_start_value, -1)
    elif 100 < (max_y_axis - min_y_axis) < 500:
        grad_interval = 50
        step = 10
        value_grad = round(y_start_value, -1)
    elif 500 < (max_y_axis - min_y_axis) < 1000:
        grad_interval = 100
        step = 100
        value_grad = round(y_start_value, -2)
    elif 1000 < (max_y_axis - min_y_axis):
        grad_interval = 500
        step = 100
        value_grad = round(y_start_value, -2)
    while value_grad < max_y_axis or (value_grad < max_y_axis*100 and if_decimals == True):
        if value_grad % grad_interval == 0 and if_decimals == False:
            grad.append(round(value_grad, min(0, -1 * (grad_interval > 50))))
        elif (((value_grad) % (grad_interval)) == 0 and if_decimals == True):
            value_grad = round(value_grad,0)
            grad.append(value_grad/100)
        value_grad += step
    tic_size = 3
    for value in grad:
        if(if_decimals):
            tic_text = Text(self.surface, (0, 0), ["{:0.2f}".format(value)], self.font_name, 10, variables.black)
        else:
            tic_text = Text(self.surface, (0, 0), ["{:.0f}".format(value)], self.font_name, 10, variables.black)
        y_pos = y_axis.start_pos[1] - y_axis_length * 0.05 * 0 - (value - y_start_value) / scale
        tic = Line(self.surface, (y_axis.start_pos[0] - tic_size / 2, int(y_pos)),
                   (y_axis.start_pos[0] + tic_size / 2, int(y_pos)), variables.black)
        grid_y = Line(self.surface, (y_axis.start_pos[0] - tic_size / 2, int(y_pos)),
                   (y_axis.start_pos[0]+ x_axis_length , int(y_pos)), variables.grid_colour)
        tic_text.pos = (int(tic.start_pos[0] - 1.1 * tic_text.font_w), tic.start_pos[1] - tic_text.font_h / 2)
        if value >= y_start_value:
            if (variables.grid):
                grid_y.show()
            tic.show()
            tic_text.show()
    # Now we create and show the x tics
    min_x = time.mktime(self.start_graph)
    max_x = time.mktime(time.localtime())
    scale = (max_x - min_x) / (x_axis_length)  # in unit time/pixel
    grad = []
    delta_hour = 3 * (self.graph == "D")
    delta_day = (self.graph == "W")
    delta_week = (self.graph == "M")
    value_grad = time.mktime(self.start_graph)
    while value_grad <= max_x:
        grad.append(value_grad)
        value_grad += 3600 * (delta_hour + 24 * delta_day + 7 * 24 * delta_week)
    for value_time in grad:
        value = time.localtime(value_time)
        if self.graph == "D":
            tic_text = Text(self.surface, (0, 0), [str(value.tm_hour) + "h"], self.font_name, 10, variables.black)
        else:
            if self.date_format == "%d/%m":
                tic_text = Text(self.surface, (0, 0), [str(value.tm_mday) + "-" + str(value.tm_mon)],
                                self.font_name, 10, variables.black)
            elif self.date_format == "%m/%d":
                tic_text = Text(self.surface, (0, 0), [str(value.tm_mon) + "-" + str(value.tm_mday)],
                                self.font_name, 10, variables.black)
            else:
                # dd/mm time format will be used if self.date_fornat is not recognised
                tic_text = Text(self.surface, (0, 0), [str(value.tm_mday) + "/" + str(value.tm_mon)],
                                self.font_name, 10, variables.black)
        x_pos = x_axis.start_pos[0] + (value_time - min_x) / scale
        tic = Line(self.surface, (int(x_pos), int(x_axis.start_pos[1] - tic_size / 2)),
                   (int(x_pos), int(x_axis.start_pos[1] + tic_size / 2)), variables.black)
        grid_x = Line(self.surface, (int(x_pos), int(x_axis.start_pos[1] - tic_size / 2)),
                    (int(x_pos) , int(x_axis.start_pos[1] - y_axis_length)), variables.grid_colour)
        tic_text.pos = (int(tic.start_pos[0] - tic_text.font_w / 2), int(tic.start_pos[1] + tic_text.font_h * 0.5))
        if (variables.grid):
            grid_x.show()
        tic.show()
        tic_text.show()


A grid on the graph is optional and can be switched off by changing the variable ``grid`` in ``variables.py`` to **False**.
There is also a possibility to change the colour of the grid in the same file (variable ``grid_colour``)

X-axis ticks are fixed but Y-axis ticks are flexible.
They will adjust to **y_min** and **y_max** to have the optimal view.

Curve
-----

.. code-block:: python
    :linenos:

    self.X_database = []
    self.Y_database = []
    self.graph = "D"
    if self.graph == "D":
        self.start_graph = time.localtime(time.time() - 3600 * 24)
        self.max_measure_group = variables.av_day
        # self.measure_interval = (time)/ (points on the graph) = 
        # (36000*24)/(3600*24/(1/variables.FPS_sensor*variables.av_day))
        self.measure_interval = variables.av_day* 1/variables.FPS_sensor
    elif self.graph == "W":
        self.start_graph = time.localtime(time.time() - 3600 * 24 * 7)
        self.max_measure_group = variables.av_week
        self.measure_interval = variables.av_week* 1/variables.FPS_sensor
    elif self.graph == "M":
        self.start_graph = time.localtime(time.time() - 3600 * 24 * 30)
        self.max_measure_group = variables.av_month
        self.measure_interval = variables.av_month* 1/variables.FPS_sensor
    
    self.measures = self.db.get_row_from_graphs(self.list_sensor_dict[self.home_property]["sensor_id"],
                                          self.max_measure_group)
    # uploading data to display
    try:
        # trying to pull the data from "graph" table in database
        self.X_database =[float(i) for i in self.measures[0][0].split(',')]
        self.Y_database =[float(i) for i in self.measures[0][1].split(',')]
        update = self.db.set_graph(self.list_sensor_dict[self.home_property]["sensor_id"],
                                            self.max_measure_group,
                                            datetime.utcfromtimestamp(int(self.X_database[-1])).strftime('%Y-%m-%d %H:%M:%S')) 
        self.X_database = self.X_database + [time.mktime(time.strptime(element[0], "%Y-%m-%d %H:%M:%S")) for element in update]
        self.Y_database = self.Y_database + [element[1] for element in update]
        min_y = min(self.Y_database)
        while (time.mktime(self.start_graph) >= self.X_database[0]):
            self.X_database.pop(0)
            self.Y_database.pop(0)
    except:
        # if there are no records in "graph" table pulling data straight from "measure" table
        self.measures = self.db.set_graph(self.list_sensor_dict[self.home_property]["sensor_id"],
                                            self.max_measure_group,
                                            time.strftime("%Y-%m-%d %H:%M:%S", self.start_graph))
        self.X_database = [time.mktime(time.strptime(element[0], "%Y-%m-%d %H:%M:%S")) for element in self.measures]
        self.Y_database = [element[1] for element in self.measures]
    # update of "graph" table
    self.db.insert_graph(str(self.X_database).strip('[]'), str(self.Y_database).strip('[]'),self.list_sensor_dict[self.home_property]["sensor_id"], self.max_measure_group)
    self.values = vars(self)

.. note::

    The above piece of code is not a part of graph method itself! You can find it in **__init__** and **change_attribute** methods.     

.. code-block:: python
    :linenos:

    # uploading X and Y 

    X = self.X_database
    Y = self.Y_database
    
    try:
        min_y = min(Y)
        max_y = max(Y)
        # checking if Y have at least two elements
        at_least_two_values = Y[1]
        if self.list_sensor_dict[self.home_property]["active"] and self.mode == "auto":
            out = self.list_sensor_dict[self.home_property]["out"]
            actual_target = self.list_sensor_dict[self.home_property]["target"] + self.list_sensor_dict[self.home_property]["setting_delta"]
            min_y = min(min_y, actual_target - out)
            max_y = max(max_y, actual_target + out)
        if max_y == min_y:
            max_y += 1  # Avoids division by 0 error further down
        min_x = min(X)
        min_x = min(min_x, time.mktime(self.start_graph))
        max_x = max(X)
        max_x = max(max_x, time.mktime(time.localtime()))
    except:
        graph_error_text = Text(self.surface, (0, 0), ["Not enough data to display this graph"],
                                self.font_name, 13, variables.black)
        graph_error_text.pos = ((self.width - graph_error_text.font_w) / 2, (self.height + top_tab.box.size[1]) / 2)
        graph_error_text.show()
        if self.coord is not None and self.py_touch:
            for active_object in graphs_active_objects:
                active_object.act(self.coord)
            self.start = time.time()
        return 4


.. code-block:: python
    :linenos:

    # creating curve
    start_x = int(x_axis.start_pos[0] + (x_axis_length / (max_x - min_x)) * (X[0] - min_x))
    start_y = int(y_axis.start_pos[1] - 0.05 * y_axis_length - (y_axis_length * 0.95 / (max_y - min_y)) * (Y[0] - min_y))
    start_pos = (start_x, start_y)
    for i in range(len(X) - 1):
        end_x = (x_axis_length / (max_x - min_x)) * (X[i + 1] - min_x)
        end_y = (y_axis_length * 0.95 / (max_y - min_y)) * (Y[i + 1] - min_y)
        end_pos = (int(x_axis.start_pos[0] + end_x), int(y_axis.start_pos[1] - 0.05 * y_axis_length - end_y))
        if (X[i + 1] - X[i]) < 2 * (self.measure_interval):
            curve_section = Line(self.surface, start_pos, end_pos, variables.black)
            curve_section.show()
        start_pos = end_pos
    self.db.insert_graph(str(X).strip('[]'), str(Y).strip('[]'),self.list_sensor_dict[self.home_property]["sensor_id"], self.max_measure_group)
    if self.coord is not None and self.py_touch:
        for active_object in graphs_active_objects:
            active_object.act(self.coord)
        self.start = time.time()


Of course, the main part of the menu is curve itself.
To show the graph for the first time our program will upload data from the ``measure`` table. 
After that, X and Y tables will be stored in the ``graph`` table.
The ``Graph`` table in our case works as a cache - it stores previously used points (X, Y). 
That means that the next time to show the graph we just need pull coordinates from the ``Graph`` table 
and update it with the newest measure from ``measure`` table.
That helps to display graphs faster than with calculating average each time the graph is displayed. 
If there are less than two elements to display, there will be "Not enough data to display this graph" text shown.