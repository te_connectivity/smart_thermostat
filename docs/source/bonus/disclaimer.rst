Disclaimer
==========

TE Connectivity or any person working on this projectis not responsible for any damage caused by:

* The mounting of components
* The use of this software
* Damage caused to any actuators

Note that downloading and mounting TE Smart Thermostat is done at your own risk.
All the code and components come with no warrranty  to the extent permittable by applicable law.

.. warning::

    Make sure you always understand what you are doing during the process in order to avoid mistakes that could cause any damages.

    In particular, be aware that there are safe range of values associated with certain properties that you may want to set a target for.
    For example, a temperature set too high or too low might be lethal. The same goes for :math:`CO_2` or VOC, supposing you have
    appropriate actuators to set them.
