The Box class
==================

This class is used to draw a box on the screen. The box may have an horizontal colour gradient,
as well as rounded corners.

__init__ method
----------------

.. literalinclude:: ../../../../gui.py
    :linenos:
    :pyobject: Box.__init__
    :language: python

List of attributes :
^^^^^^^^^^^^^^^^^^^^^^^^

* *self.surface*: As discussed previously this is generally the screen surface.

* | *self.pos*: This is a tuple corresponding the position of the top-left corner of the box in pixels.
  |             The code uses the python function ``int()`` to make sure the coordinates are integers, as is expected by pygame built-in draw.rect function. This argument of the ``__init__`` method may therefore be a real number that is not an integer (as often occurs when we centre a box).

  .. note::
      Be aware that if a box has rounded corners,
      the point corresponding to *self.pos*, is not part of the box itself,
      but rather where the top-left corner of the box would be with no rounding.

      For example, on this image, this point is the intersection of the 2 dashed lines.

      .. image:: /_static/round_button.svg
         :align: center
         :alt: alternate text

* | *self.size*: A tuple corresponding to the width and height of the box (from its top-left corner to its bottom right corner)
  | minding the above note. Be aware that the tuple values are also passed through the int function.

* *self.colour1*: This is the colour desired for the leftmost part of the box.

* | *self.colour2*: This is the colour desired for the rightmost part of the box.
  | If self.colour1 and self.colour2 are different, the show method will show a gradient of
  | colour between the left and the right of the rectangle. The following image shows a
  | box with attribute ``self.colour1 = variables.green`` and ``self.colour2 = variables.lightblue0``.

  .. image:: /_static/example_grad.png
     :align: center
     :alt: alternate text

* | *self.rounding*: **This attribute should be a real number between 0 and 1 inclusive.**
  | By default it is set to 0, which means no rounding, while 1 corresponds to maximum rounding,
  | that is the smallest side of the box becomes a full semi-circle (as in the image in the previous note).

* | *self.line_colour*: This attribute sets the colour of the line around the border of the box object.
  | **It is set to black by default and should be set to** ``None`` **for no border.**

* | *self.line_width*: This attribute defines the width of the line around the box's border.
  | Note that the border only grows towards the inside of the box.

show method
----------------


.. literalinclude:: ../../../../gui.py
    :pyobject: Box.show
    :linenos:
    :language: python

This method is called to display the box on the screen. It makes extensive use of the
`pygame.draw.rect method <https://www.pygame.org/docs/ref/draw.html#pygame.draw.rect>`_,
which takes 3 arguments: a pygame surface, a colour, and a pygame rect - a 4 elements tuple of the form
(pos[0], pos[1], size[0], size[1]) in the scheme we have.

Code flow
^^^^^^^^^^^

The code first defines the rect associated to the object.

The code then checks if *self.colour1* and *self.colour2* are identical, and if *self.rounding* is 0.

If that is the case, our object is a uniform coloured rectangle, so we use pygame.draw.rect to draw it in one go.

If it is not the case and there is rounding the radius of the rounded corners is calculated as
:math:`rad = self.rounding \times \text{min_side} \times \frac{1}{2}`, where min_side is the length of the smallest side of
the box object. This ensures that we do not have discontinuities when the object is shown.
The program also defines a few dimensions (x0, y0, length, y1), and extracts the RGB values of *self.colour1*
and *self.colour2*

Next is a for loop that will draw several *small rectangles* of width 1 pixel starting from the left-hand side,
and ending on the right-hand side. For each of these small rectangles, we are able to set the following:

1. | The rectangle's colour, calculated so that a "continuous" colour gradient may appear from left to right:
   | Each RGB value is calculated as a value between the associated RGB values of *self.colour1* and *self.colour2*.
   | The weighing of each value changes linearly the closer we are from the appropriate side of the rectangle.
   | For example, to calculate the value of the red value, the formula is:
   | :math:`r = r0 \times (1 - \frac{x}{length}) + r1 \times \frac{x}{length}`, where r0 is the level of red in *self.colour1*,
   | r1 that of *self.colour2*, x an integer corresponding to the position of the small rectangle about to be drawn (it is between 0 and length).
   | The rectangle's colour is then just ``(r, g, b)``.

2. | The small rectangle's y position and its height (these only vary between 2 small rectangles if there is rounding involved).
   | If the small rectangle being drawn is closer than the distance ``rad`` to the left or to the right of the box object, a variable ``not_drawn_h`` is
   | calculated. For example, consider the case of drawing the small red rectangle of width 1 pixel. We need to determine the value of ``y0`` and ``y1``:

   .. image:: /_static/rounding.svg
      :align: center
      :alt: alternate

   |
   | From the `equation of a circle <https://www.mathopenref.com/coordgeneralcircle.html>`_ we can calculate ``not_drawn_h`` as
   | :math:`\text{not_drawn_h} = rad - \sqrt{|rad^2 - (x - rad)^2|}`
   |
   | Then, as can be seen from the image,
   | :math:`y0 = self.pos[1] + \text{not_drawn_h}` and
   | :math:`y1 = self.size[1] - 2 \times \text{not_drawn_h}`.

The code then draws the small rectangle of width 1 using pygame.draw.rect and moves on to the next one.

Lastly, the code draws the border around the box, if *self.line_colour* is not ``None``.
If there is no rounding, this is easily accomplished with the pygame.draw.rect function, specifying
*self.line_width* as the width of the drawn rectangle.
However, if there is rounding, the border is drawn using a combination of straight line and arcs to match
the outside of the object displayed.

.. note::
    Rounding error often occurs with arcs and lines,
    causing borders of rounded boxes to often appear as discontinuous.
    We suggest setting *self.line_colour* to ``None`` when working with rounded box objects.

Final example
------------------

.. code-block:: python
    :linenos:

    example = Box(self.surface, (100, 100), (150, 90), variables.green, variables.light_blue0,
                  rounding=0.5, line_colour= variables.black, line_width=1)
    example.show()

This code produces:

.. image:: /_static/box_example.png
    :align: center
    :alt: alternate
