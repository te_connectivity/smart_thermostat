''' 
   Copyright 2020 TE Connectivity
   Distributed under MIT license.
   See file LICENSE for detail or copy at https://opensource.org/licenses/MIT
'''

import os
import sqlite3
import datetime
import queue
import tempfile
import time
class SQLiteSubscriber:
    def __init__(self, cursor, topics, timestamp):
        self.messages = queue.Queue()
        self.timestamp = timestamp
        self.topics = list(topics)
        self.cursor = cursor
    def getAll(self):
        for topic in self.topics:
            self.cursor.execute('''
                SELECT message, timestamp 
                FROM mps_messages 
                WHERE topic=:topic and timestamp>:timestamp
            ''', {
                    "topic": topic,
                    "timestamp": self.timestamp
                }
            )
            data = self.cursor.fetchall()
            for each_record in data:
                self.messages.put_nowait(each_record[0])
        self.timestamp = datetime.datetime.now()
        items = []
        maxItemsToRetreive = self.messages.qsize()
        for numOfItemsRetrieved in range(0, maxItemsToRetreive):
            try:
                if numOfItemsRetrieved == maxItemsToRetreive:
                    break
                items.append(self.messages.get_nowait())
                self.messages.task_done()
            except queue.Empty:
                break
        return items
class SQLitePubSub:
    def __init__(self):
        self.connection = sqlite3.connect(os.path.join(tempfile.gettempdir(), 'pubsub.db'))  # Create tempfile database
        self.cursor = self.connection.cursor()
        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS mps_messages(
                topic VARCHAR(100),
                message VARCHAR(1000),
                timestamp VARCHAR(100)
            );
        ''')  # Create tables of the database
    def publish(self, topic, message):
        try:
            self.cursor.execute('''
                DELETE FROM mps_messages 
                WHERE topic='{0}';
            '''.format(topic))
        except:
            pass
        self.cursor.execute('''
            INSERT INTO mps_messages
            VALUES (
                :topic,
                :message,
                :timestamp
            );
        ''', {
            "topic": topic,
            "message": message,
            "timestamp": datetime.datetime.now()
        })  # Insert new message in the database
        self.connection.commit()
    def subscribe(self, *topics):
        timestamp = datetime.datetime.now()
        subscriber = SQLiteSubscriber(self.cursor, topics, timestamp)
        return subscriber
class Pubsub:
    def __init__(self):
        self.pubsub = SQLitePubSub()  # Initialise object with temp database
        self.result = ""
    def set(self, channel, message = " "):  # Create a message on the channel
        self.get(channel)
        self.pubsub.publish(channel, message)
    def get(self, channel):  # tell if new message on the database
        try:
            exec("chan=self." + channel)
            exec("self.result = chan.getAll()")
            return len(self.result) > 0
        except AttributeError:
            exec("self." + channel + " = self.pubsub.subscribe('" + str(channel) + "')")
            time.sleep(0.001)
            return self.get(channel)
    def get_value(self,channel):
        try:
            exec("self.result=self."+channel+".getAll()")
            return self.result[-1]
        except AttributeError:
            exec("self."+channel+"=self.pubsub.subscribe('"+ channel + " ')")
            time.sleep(0.001)
            self.get_value(channel)