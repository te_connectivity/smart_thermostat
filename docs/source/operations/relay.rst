Relay loop
=======================================

How it works
------------
The relay loop is listening to the pubsub channel "relay",
when it is triggered, the following procedure is triggered:

Procedure
    - Get all the GPIO pin number and states from the database
    - Check the state of all the relevant GPIO pins on the Raspberry Pi
    - If there is any difference, correct it to match what is in the database

.. note:: This loop is triggered every second in order to have a high reactivity

Loop code
---------------------------
.. literalinclude:: ../../../relay.py
    :emphasize-lines: 1
    :language: python
    :linenos:
    :pyobject: loop
