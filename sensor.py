def loop():
    import time
    import db
    import pubsub
    import settings
    import variables
    from datetime import datetime
    import ambimate_python_driver.ambimate

    try: 
        name = __name__
        Pub = pubsub.Pubsub()  # Initialize communication object
        Db = db.DB()  # Initialize data storage object
        amb = ambimate_python_driver.ambimate.Ambimate()  # Initialize sensor object
        St = settings.Settings()  # Initialize variable storage object
        amb.SampleTime = 0  # Set sample time to 0 (no waiting after get_all())
        amb.BoolTemperatureFahrenheit = False  # Set temperature in celcius (every thing is stored in celcius)
        amb.disable_all_sensor()  # Disable all sensors
        # amb.setup_sensors()
        sensorConfig=[]
        for i in variables.sensors:
            sensorConfig.append(i)
        if "Temperature" in sensorConfig:
            amb.BoolEnableTemperature = True
        if "Humidity" in sensorConfig:
            amb.BoolEnableHumidity = True
        if "CO2" in sensorConfig:
            amb.BoolEnableCO2 = True
        if "VOC" in sensorConfig:
            amb.BoolEnableVOC = True
        if "Light" in sensorConfig:
            amb.BoolEnableLight = True
        if "Audio" in sensorConfig:
            amb.BoolEnableAudio = True
        if "Light" in sensorConfig:
            amb.BoolEnableAudio = True
        amb.get_all()
        print(amb.get_all())
        names_sensor = Db.get_sensors_name()
        for sensor in amb.data:
            if not(sensor in names_sensor):
                Db.insert_sensor(
                    sensor,
                    variables.sensors[sensor][0],
                    variables.sensors[sensor][1],
                    variables.sensors[sensor][2],
                    variables.sensors[sensor][3]
                )
        names_actuator=Db.get_actuators_name()
        for actuator in variables.actuators:
            if not(actuator in names_actuator):
                Db.insert_actuator(actuator, variables.actuators[actuator][0])
                id_actuator = Db.get_actuator_id(actuator)
                sensors = variables.actuators[actuator][1]
                for sensor, up, backup in sensors:
                    id_sensor = Db.get_sensor_id(sensor)
                    Db.insert_actuator__sensor(id_sensor, id_actuator, up, backup)
        Pub.get(name)  # Initialize comminication channel __name__
        time_loop = time.time()  # Start time loop (To regulate max FPS)
        while 1:  # Infinite loop start
            time.sleep(St.get("is_raspberry") * max(0, 1 / float(variables.FPS_sensor) - (time.time() - time_loop)))  # Sleep time to not trigger the script more than the FPS ratio fixed
            time_loop = time.time()  # Start time loop (To regulate max FPS)
            try:
               if True or __name__ == "__main__" or St.get("is_unittest"):
                   data = amb.get_all()  # Get all activated sensor datas
                   # print(data)
                   for measure in data:
                       Db.set_sensor_value(Db.get_sensor_id(measure),data[measure][0])  # Add the value to the database
               if St.get("is_unittest") or __name__=="__main__":
                   break
               variables.ambimate_visible = True
            except:
                variables.ambimate_visible = False
                print("No Ambimate!")
    except :
        print("No Ambimate!")
        variables.ambimate_visible = False
if __name__ == "__main__":
    import settings
    St = settings.Settings()  # Initialize variable storage object
    St.set("is_unittest", False)
    loop()
