Tests
=====

To realize theses tests, open the console of the raspberry pi (ssh / keyboard + HDMI)

Ambimate test
-------------

You can test your ambimate module by this way:

Version
~~~~~~~

Run the command: ``sudo python3 /home/pi/smart_thermostat/ambimate_python_driver/examples/getAmbimateDetails.py``

If the example display your ambimate version, it works.

Values
~~~~~~

Run the command: ``sudo python3 /home/pi/smart_thermostat/ambimate_python_driver/examples/getAllValues.py``

If the example display your ambimate sensors values like :

``{'Temperature': (23.4, '°C'), 'Humidity': (33.4, '%'), 'Light': (4, 'lx'), 'CO2': (466, 'ppm'), 'VOC': (10, 'ppb'), 'Voltage': (3.36, 'Volts'), 'EventMotion': (False, 'PIRMOTION')}``

then it works.

Relay test
----------

You can test the relay board by this way:

.. warning:: For this test, unplug your actuators (230 V) from the relay board.

Run the command: ``sudo python3``

Then run this script:

.. code-block:: python
    :linenos:

    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    
    GPIO.setup(5, GPIO.OUT)
    GPIO.setup(6, GPIO.OUT)
    GPIO.setup(13, GPIO.OUT)
    GPIO.setup(26, GPIO.OUT)
    
    GPIO.output(5, GPIO.HIGH)
    GPIO.output(6, GPIO.HIGH)
    GPIO.output(13, GPIO.HIGH)
    GPIO.output(26, GPIO.HIGH)
    
If all your relays turn ON, it works.