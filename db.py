''' 
   Copyright 2020 TE Connectivity
   Distributed under MIT license.
   See file LICENSE for detail or copy at https://opensource.org/licenses/MIT
'''

import sys
import sqlite3
import pubsub
import os
import shutil
import settings
import time
import variables
import datetime
from datetime import timedelta

class DB:
    def __init__(self):
        dir = sys.path[0]
        self.path = os.path.join(dir, "..", "db.db")
        self.St = settings.Settings()
        self.con = sqlite3.connect(self.path)
        self.cur = self.con.cursor()
        self.printQuerry = False
        self.printResult = False
        self.Pb = pubsub.Pubsub()
        self.querry('''
            CREATE TABLE IF NOT EXISTS "action" (
                "id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                "id_actuator"	INTEGER NOT NULL,
                "value"	INTEGER NOT NULL,
                "timeDate"	TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP
                );
        ''')
        self.querry('''
            CREATE TABLE IF NOT EXISTS "actuator" (
                "id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                "name"	TEXT NOT NULL UNIQUE,
                "gpio"	INTEGER NOT NULL UNIQUE,
                "value"	REAL NOT NULL DEFAULT 0
            );
        ''')
        self.querry('''
            CREATE TABLE IF NOT EXISTS "actuator_sensor" (
                "id_actuator"	INTEGER NOT NULL,
                "id_sensor"	INTEGER NOT NULL,
                "up"	INTEGER NOT NULL DEFAULT 0,
                "backup"	INTEGER NOT NULL DEFAULT 0
            );
        ''')
        self.querry('''
            CREATE TABLE IF NOT EXISTS "graphs" (
                "id_sensor"	INTEGER NOT NULL,
                "id_graph"	INTEGER NOT NULL,
                "X"	TEXT NOT NULL DEFAULT 0,
                "Y"	TEXT NOT NULL DEFAULT 0,
                PRIMARY KEY (id_sensor, id_graph)
            );
        ''')
        self.querry('''
            CREATE TABLE IF NOT EXISTS "measure" (
                "id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                "value"	REAL NOT NULL,
                "timeDate"	TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
                "id_sensor"	INTEGER NOT NULL
            );
        ''')
        self.querry('''
            CREATE TABLE IF NOT EXISTS "sensor" (
                "id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                "name"	TEXT NOT NULL DEFAULT 'Sensor',
                "unit"	TEXT NOT NULL DEFAULT 'Unknown',
                "target"	REAL NOT NULL DEFAULT 0,
                "outer"	REAL NOT NULL DEFAULT 0,
                "out"	REAL NOT NULL DEFAULT 0,
                "value"	REAL NOT NULL DEFAULT 0,
                "derivative_up"	REAL NOT NULL DEFAULT 0,
                "derivative_down"	REAL NOT NULL DEFAULT 0,
                "date_change"	TEXT NOT NULL DEFAULT 0
            );
        ''')
        self.querry('''
            CREATE TABLE IF NOT EXISTS "setting" (
                "id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                "id_sensor"	INTEGER NOT NULL,
                "delta"	REAL NOT NULL DEFAULT 0,
                "start"	INTEGER NOT NULL DEFAULT 0,
                "end"	INTEGER NOT NULL DEFAULT 24,
                "days"	INTEGER NOT NULL DEFAULT 255,
                "months"	INTEGER NOT NULL DEFAULT 4095
            );
        ''')
    def querry(self, q = ""):
        if self.printQuerry:
            print(q)
        try:
            self.cur.execute(q) 
            self.con.commit()
        except:
            time.sleep(0.1)
            self.querry(q)
            self.con.commit()
        _return = self.cur.fetchall()
        if self.printResult:
            if _return != []:
                print(("Result: " + str(_return)) * min(1, len(_return)))
        return _return
    def add_action(self, id_actuator = 0, value = 0.0, date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))):
        self.querry('''
            INSERT INTO action (id_actuator,value,timeDate)
            VALUES ('{0}','{1}','{2}');
        '''.format(int(id_actuator), float(value), str(date)))
        self.Pb.set("relay")
        self.Pb.set("actuator")
    def add_measure(self, id_sensor = 0, value = 0.0, date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))):
        self.querry('''
            INSERT INTO measure (id_sensor,value,timeDate)
            VALUES ('{0}','{1}','{2}');
        '''.format(int(id_sensor), float(value), str(date)))
        self.Pb.set("controller")
        self.Pb.set("analyser")
        self.Pb.set("graph")
    def add_setting(self, id_sensor = 0, delta = 0.0, start = 0, end = 0, days = 0, months = 0):
        self.querry('''
        INSERT INTO setting (id_sensor, delta, start, end, days, months)
        VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}');
        '''.format(int(id_sensor), float(delta), int(start), int(end), int(days), int(months)))
        self.Pb.set("table_setting")
    def clear_after_reboot(self):
        minDate=self.querry('''
            SELECT MIN(timeDate)
            FROM measure;
        ''')[0][0]

        if minDate==None:
            minDate=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

        dateRemove = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time() - variables.remaining_data))

        self.querry('''
            DELETE FROM measure
            WHERE datetime(timeDate) < datetime('{0}');
        '''.format(dateRemove))
        self.querry('''
            DELETE FROM action
            WHERE datetime(timeDate) < datetime('{0}');
        '''.format(dateRemove))
    def clear_old(self):
        minDate=self.querry('''
            SELECT MIN(timeDate)
            FROM measure;
        ''')[0][0]

        if minDate==None:
            minDate=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

        dateRemove = datetime.datetime.strptime(str(datetime.datetime.strptime(minDate, '%Y-%m-%d %H:%M:%S') + datetime.timedelta(seconds=variables.auto_reboot)), "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")

        self.querry('''
            DELETE FROM measure
            WHERE datetime(timeDate) < datetime('{0}');
        '''.format(dateRemove))
        self.querry('''
            DELETE FROM action
            WHERE datetime(timeDate) < datetime('{0}');
        '''.format(dateRemove))
    def clear_bdd(self):
        self.remove_measures()
        self.remove_actions()
        self.remove_settings()
        self.remove_sensors()
        self.remove_actuators()
    def get_action_date(self, id_actuator = 0):
        return self.querry('''
            SELECT timeDate
            FROM action
            WHERE id_actuator='{0}'
            ORDER BY timeDate DESC
            LIMIT 1;
        '''.format(int(id_actuator)))[0][0]
    def get_actuator_names(self):
        list_tuples = self.querry('''
            SELECT actuator.name
            FROM actuator;
        ''')
        return [name_tuple[0] for name_tuple in list_tuples]
    def get_actuator__sensors_id(self, id_sensor = 0):
        _list = self.querry('''
            SELECT id_actuator
            FROM actuator_sensor
            WHERE id_sensor='{0}';
        '''.format(int(id_sensor)))
        _return=[]
        for _element in _list:
            _return.append(_element[0])
        return _return
    def get_actuator_id(self, name = ""):
        return self.querry('''
            SELECT id
            FROM actuator
            WHERE name='{0}';
        '''.format(str(name)))[0][0]
    def get_actuator_state(self, id = 0):
        return int(self.querry('''
            SELECT value
            FROM actuator
            WHERE id='{0}';
        '''.format(int(id)))[0][0])
    def get_actuator_name(self, id = 0):
        return self.querry('''
            SELECT name
            FROM actuator
            WHERE id='{0}';
        '''.format(int(id)))[0][0]
    def get_actuator_up(self, id_actuator = 0):
        return bool(int(self.querry('''
            SELECT up
            FROM actuator_sensor
            WHERE id_actuator='{0}';
        '''.format(int(id_actuator)))[0][0]))
    def get_actuator_backup(self, id_actuator = 0):
        return bool(int(self.querry('''
            SELECT backup
            FROM actuator_sensor
            WHERE id_actuator='{0}';
        '''.format(int(id_actuator)))[0][0]))
    def get_actuators_id(self):
        _list = self.querry('''
            SELECT id
            FROM actuator;
        ''')
        _return = []
        for _element in _list:
            _return.append(_element[0])
        return _return
    def get_actuators_name(self):
        _list = self.querry('''
            SELECT name FROM actuator;
        ''')
        _return = []
        for _element in _list:
            _return.append(_element[0])
        return _return
    def get_actuators_gpio_value(self):
        return self.querry('''
            SELECT gpio,value,id,name
            FROM actuator;
        ''')
    def get_measures_date_sensor(self,id_sensor = 0,date = ""):
        _list = self.querry('''
            SELECT value
            FROM measure
            WHERE
                id_sensor='{0}' AND
                datetime(timeDate) > datetime('{1}')
            ORDER BY timeDate ASC;
        '''.format(int(id_sensor),str(date)))
        _return = []
        for _element in _list:
            _return.append(_element[0])
        return _return
    def get_measures_dates_sensor(self, id_sensor = 0, date = ""):
        _list = self.querry('''
            SELECT timeDate, value
            FROM measure
            WHERE
                id_sensor='{0}' AND
                datetime(timeDate) > datetime('{1}')
            ORDER BY timeDate ASC;
        '''.format(int(id_sensor), str(date)))
        return _list
    def get_sensor_all_info(self, id_sensor = 0):
        return self.querry('''
            SELECT name,value,derivative_up, derivative_down,date_change 
            FROM sensor
            WHERE id='{0}';
        '''.format(int(id_sensor)))[0]
    def get_sensor_unit(self, id_sensor = 0):
        return self.querry('''
            SELECT unit
            FROM sensor
            WHERE id='{0}';
        '''.format(int(id_sensor)))[0][0]
    def get_sensor_value(self, id_sensor = 0):
        return self.querry('''
            SELECT value
            FROM sensor
            WHERE id='{0}';
        '''.format(int(id_sensor)))[0][0]
    def get_sensor_out(self, id_sensor = 0):
        return self.querry('''
            SELECT out
            FROM sensor
            WHERE id='{0}';
        '''.format(int(id_sensor)))[0][0]
    def get_sensor_name(self, id = 0):
        return self.querry('''
            SELECT name
            FROM sensor
            WHERE id='{0}';
        '''.format(int(id)))[0][0]
    def get_sensor_id(self, name = ""):
        return self.querry('''
            SELECT id
            FROM sensor
            WHERE name='{0}';
        '''.format(str(name)))[0][0]
    def get_sensor_target(self, id_sensor = 0):
        return self.querry('''
            SELECT target
            FROM sensor
            WHERE id='{0}';
        '''.format(int(id_sensor)))[0][0]
    def get_sensor_target_time(self, id_sensor = 0):
        return self.querry('''
            SELECT date_change
            FROM sensor
            WHERE id='{0}';
        '''.format(int(id_sensor)))[0][0]
    def get_sensor_target_delta(self, id_sensor = 0):
        return self.querry('''
            SELECT target
            FROM sensor
            WHERE id='{0}';
        '''.format(int(id_sensor)))[0][0] + self.get_setting_delta(id_sensor)
    def get_setting_parameters(self):
        sublist = self.querry('''
            SELECT id, id_sensor, delta,start,end,days,months
            FROM setting;
        ''')
        return sublist
    def get_setting_delta(self, id_sensor = 0):
        _list=self.querry('''
            SELECT delta,start,end,days,months
            FROM setting
            WHERE id_sensor='{0}';
        '''.format(int(id_sensor)))
        settings = []
        for _element in _list:
            settings.append(_element)
        date = time.localtime(time.time())
        date_day = 2 ** int(date.tm_wday)
        date_hour = int(date.tm_hour)
        date_month = 2 ** (int(date.tm_mon) - 1)
        delta = 0
        for setting in settings:
            setting_delta = setting[0]
            setting_start = setting[1]
            setting_end = setting[2]
            setting_days = int(setting[3])
            setting_months = int(setting[4])
            if date_hour < setting_start:
                continue
            if  date_hour > setting_end:
                continue
            if setting_days & date_day == 0:
                continue
            if setting_months & date_month == 0:
                continue
            delta += setting_delta
        return round(delta, 1)
    def get_sensor_outer(self, id_sensor = 0):
        return self.querry('''
            SELECT outer
            FROM sensor
            WHERE id='{0}';
        '''.format(int(id_sensor)))[0][0]
    def get_sensors_id(self):
        _list = self.querry('''
            SELECT id FROM sensor;
        ''')
        _return = []
        for _element in _list:
            _return.append(_element[0])
        return _return
    def get_sensors_name(self):
        _list = self.querry('''
            SELECT name FROM sensor;
        ''')
        _return = []
        for _element in _list:
            _return.append(_element[0])
        return _return
    def get_sensor_derivative_up(self, id_sensor):
        return self.querry('''
            SELECT derivative_up
            FROM sensor
            WHERE id = '{0}';
        '''.format(int(id_sensor)))[0][0]
    def get_sensor_derivative_down(self, id_sensor = 0):
        return self.querry('''
            SELECT derivative_down
            FROM sensor
            WHERE id = '{0}';
        '''.format(int(id_sensor)))[0][0]
    def get_number_rows(self, table_name = ""):
        # this method returns the number of rows of a specified table
        return self.querry('''
            SELECT COUNT(*)
            FROM ('{0}');
        '''.format(str(table_name)))[0][0]
    def get_row_from_graphs(self, id_sensor = 0, n = 0):
        '''
        This querry returns rows from graphs table
        for specified sensor and graph (day, week, month)
        This is useful to plot graphs
        '''
        if n == variables.av_day: m = 1
        elif n == variables.av_week: m = 2
        elif n == variables.av_month: m = 3
        try:
            return self.querry('''
                SELECT X,Y
                FROM graphs
                WHERE  id_sensor = {0} and id_graph = {1}
            '''.format(int(id_sensor), int(m)))
        except sqlite3.OperationalError:
            return []
    def insert_actuator(self, name = "", gpio = 0):
        self.querry('''
            INSERT INTO actuator (name, gpio)
            VALUES ('{0}','{1}');
        '''.format(str(name), int(gpio)))
    def insert_graph(self, X = "",Y = "", id_sensor = 0, n = 0):
        if n == variables.av_day: m = 1
        elif n == variables.av_week: m = 2
        elif n == variables.av_month: m = 3
        self.querry('''
        REPLACE INTO graphs (X,Y,id_sensor, id_graph)
        VALUES ('{0}','{1}','{2}',{3});
        '''.format(str(X),str(Y),int(id_sensor), int(m)))
    def insert_sensor(self, name = "", target = 0.0, outer = 0.0, out = 0.0, unit = ""):
        self.querry('''
            INSERT INTO sensor (name,target,outer,out,unit)
            VALUES ('{0}','{1}','{2}','{3}','{4}');
        '''.format(str(name), float(target), float(outer), float(out), str(unit)))
    def insert_actuator__sensor(self, id_sensor = 0, id_actuator = 0, up = 0, backup = 0):
        self.querry('''
            INSERT INTO actuator_sensor (id_sensor,id_actuator,up,backup)
            VALUES ('{0}','{1}','{2}','{3}');
        '''.format(int(id_sensor),int(id_actuator),int(up),int(backup)))
    def remove_setting(self, id = 0):
        self.querry('''
            DELETE FROM setting
            WHERE id='{0}';
        '''.format(int(id)))
        self.Pb.set("table_setting")
    def remove_measures(self):
        self.querry('''DELETE FROM measure;''')
    def remove_actions(self):
        self.querry('''DELETE FROM action;''')
    def remove_settings(self):
        self.querry('''DELETE FROM setting;''')
    def remove_sensors(self):
        self.querry('''DELETE FROM sensor;''')
    def remove_actuators(self):
        self.querry('''DELETE FROM actuator;''')
    def reset_actuators_state(self):
        self.querry('''
            UPDATE actuator
            SET value=0;
        ''')
    def set_actuator_value(self,id_actuator = 0, value = 0.0):
        self.querry('''
            UPDATE actuator
            SET value='{1}'
            WHERE id='{0}';
        '''.format(int(id_actuator),float(value)))
        self.add_action(id_actuator, value)
    def set_graph (self, id_sensor = 0, n = 0, start_date = ""):
        '''
        This querry updates graphs table
        to store most recent X and Y points for graphs.
        '''
        try:
            return self.querry('''
                SELECT *
                FROM (
                SELECT timeDate, ROUND(AVG(ALL value),1) as value, COUNT(*) as counting_all
                FROM
                ((
                SELECT ROW_NUMBER() OVER (ORDER BY measure.timeDate DESC)/'{1}' AS num, 
                measure.*
                FROM measure WHERE measure.id_sensor ={0} AND datetime(measure.timeDate) > datetime('{2}')
                ))
                Group by num
                )
                WHERE   
                counting_all >= {1}
                ORDER BY timeDate ASC
            '''.format(int(id_sensor), n, str(start_date)))
        except sqlite3.OperationalError:
            return [] 
    def set_sensor_value(self, id_sensor = 0, value = 0.0):
        self.querry('''
            UPDATE sensor
            SET value={1}
            WHERE id='{0}';
        '''.format(int(id_sensor),float(value)))
        self.add_measure(id_sensor, value, time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())))
    def set_sensor_target(self, id_sensor = 0, value = 0.0):
        self.querry('''
        UPDATE sensor
        SET target = "{1}"
        WHERE id = "{0}";
        '''.format(int(id_sensor), float(value)))
    def set_sensor_out(self, id_sensor = 0, value = 0.0):
        self.querry('''
        UPDATE sensor
        SET out = "{1}"
        WHERE id = "{0}";
        '''.format(int(id_sensor), float(value)))
    def set_sensor_outer(self, id_sensor = 0, value = 0.0):
        self.querry('''
        UPDATE sensor
        SET outer = "{1}"
        WHERE id = "{0}";
        '''.format(int(id_sensor), float(value)))
    def set_sensor_unit(self, id_sensor = 0, unit = ""):
        self.querry('''
            UPDATE sensor
            SET unit = '{1}'
            WHERE id = '{0}';
        '''.format(int(id_sensor), str(unit)))
    def set_sensor_date_change(self, id_sensor = 0, date = ""):
        self.querry('''
            UPDATE sensor
            SET date_change = '{1}'
            WHERE id = '{0}';
        '''.format(int(id_sensor), str(date)))
    def set_sensor_derivative_up(self, id_sensor = 0, derivative = 0.0):
        self.querry('''
            UPDATE sensor
            SET derivative_up = '{1}'
            WHERE id = '{0}';
        '''.format(int(id_sensor), float(derivative)))
    def set_sensor_derivative_down(self, id_sensor = 0, derivative = 0.0):
        self.querry('''
            UPDATE sensor
            SET derivative_down = '{1}'
            WHERE id = '{0}';
        '''.format(int(id_sensor), float(derivative)))
    def set_actuator_gpio(self, id_actuator = 0, gpio = 0):
        self.querry('''
            UPDATE actuator
            SET gpio='{1}'
            WHERE id='{0}';
        '''.format(int(id_actuator),int(gpio)))
if __name__ == "__main__":
    import settings
    st = settings.Settings()
    st.set("is_unittest",False)
    Db = DB()
