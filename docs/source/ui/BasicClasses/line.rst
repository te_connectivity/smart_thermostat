The Line Class
===================

This class is used to display a line with a certain colour, width,
density and dash size on the screen.

__init__ method
----------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Line.__init__
    :linenos:
    :language: python

List of attributes :
^^^^^^^^^^^^^^^^^^^^^^^^

* *self.surface*: As discussed previously this is generally the screen surface.

* *self.start_pos*: A tuple giving the coordinates of the start point of the line to be drawn.

* *self.end_pos*: A tuple giving the coordinates of the end point of the line.

* *self.colour*: The colour of the line.

* *self.width*: The line's width.

* | *self.dash_size*: This attribute corresponds to the
  | length in pixel of an individual dash of a dashed line.
  | It should be **set to 0 for a continuous or solid line (as it is by default).**
  | It should also be smaller than the total length of the line.

* | *self.density*: This should be an integer greater than 1. If it is equal to N,
  | 1 in N dashes will be drawn, the rest will be left blank. **By default,**
  | **it is set to 2, corresponding to one half of the dashes being drawn.**

show method
--------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Line.show
    :linenos:
    :language: python

This method makes use of the `pygame.draw.line <https://www.pygame.org/docs/ref/draw.html#pygame.draw.line>`_ function.
If *self.dash_size* is set to 0, corresponding to a solid line, the method uses pygame.draw.line to draw the line to the surface.
Otherwise, the program calculates the `distance between the two ends of the line <https://www.mathsisfun.com/algebra/distance-2-points.html>`_ in pixel.
It then calculates the number of dashes that could fit in that length and generates
a list of points on the line, separated by a distance of *self.dash_size*.
Lastly, 1 in *self.density* of the dashes between adjacent points are drawn, giving the
overall effect of a dashed line.

Final example
------------------

.. code-block:: python
    :linenos:

    example_line = Line(self.surface, (20, 30), (300, 200), variables.black, 1, 10, 4)
    example_line.show()

This code produces:

.. image:: /_static/example_line.png
    :align: center
    :alt: alternate
