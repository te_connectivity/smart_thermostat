``display``, ``refresh``, and ``get_coord`` methods
============================================================

These three methods are used every iteration of the ``while True`` loop of **refresh.py**.

Together, they form the mechanism that allow the UI to switch from one screen to the other,
according to the user's actions.

display method
---------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.display
    :linenos:
    :language: python


This method first updates some of the attributes of the WhatToShow class,
in accordance with the latest information coming from the rest of the programs and the Ambimate sensors.
This is achieved via the ``change_attribute`` methods (see later).

It will then determine whether there is a need to go back to the default screen, i.e, whether the time difference
``time.time() - self.start`` is greater than the ``back_to_default`` variable of **variables.py**.
If that is the case, it will make the value of *self.menu* be the default menu method ``self.default``.

This method then ends by calling *self.menu* (if it callable), which is normally always the case,
as *self.menu* is always a menu method.

Detailed code flow of the updating process
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The method first checks whether, ``time.time() - self.last_measure`` is greater than
:math:`1 / \text{FPS_sensor}`. If that is the case, then a new measure must have been made by the Ambimate module and stored in the database.
The method therefore iterates through the list *self.list_sensor_displayed* and for each displayed sensor, fetches the
new values for the dictionnaries in *self.list_sensor_dict_displayed*, by updating the keys "current", "derivative_up",
"derivative_down", and "target_time".
After this fetching process, *self.last_measure* is reupdated to the current time: ``self.last_measure = time.time()``.

The method then checks the value of the attribute *self.changed_menu*. Recall that this is a boolean, that indicates
whether a new menu was entered, it is set in the ``go_to`` method, triggered by the top blue tabs.
If a new menu was entered, the program checks two channels of the pubsub, using ``self.pub.get("channel")``.
The first channel is "actuator", that indicates whether there was a change in an actuator state, that is set by the ``add_action`` method
of the ``DB`` object in **db.py**.
The second channel is "table_setting", that indicates whether a setting was added or deleted from the database (table *setting*).
This channel is set by the ``remove_setting`` and ``add_setting`` methods of the ``DB`` object in **db.py**.

Depending on the value of the channels, the WhatToShow attributes *self.actuator_dict* (dictionary of actuators and their values)
and *self.table_setting* (list of all settings) are updated accordingly.
Note that *self.setting_page* is also updated to not be an index out of the range of the list *self.table_setting*.
Lastly, after this updating process, the attribute *self.changed_menu* is reset to ``False``, until the next time the user changes menu.

refresh method
--------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.refresh
    :linenos:
    :language: python

This method is the one that actually changes what is seen on the screen.
All the menu methods simply write to the *self.surface*, what it should be, but this one actually
assigns new values to the pixels of the screen.
This method is handled differently depending on whether the program is ran on a Raspberry Pi or
on a laptop (hence the ``if self.is_raspberry*:`` condition at the beginning of the code).

Code flow
^^^^^^^^^^^^^^^

The program first stores the current Unix time in the variable ``start_refresh``.
The program will sleep (``time.sleep``) at the end of the refresh process
if it took less time than :math:`1 / \text{variables.FPS_gui}`, thereby keeping a constant frame rate.

If the program is ran on a Raspberry Pi, the program will directly write to the ` <https://en.wikipedia.org/wiki/Framebuffer>`_ of the screen.
This is first done by opening the framebuffer file in binary, write mode as f with
``f = open("/dev/fb1", "wb")``.

.. note::

  This assumes that the framebuffer file of your screen will be in the location /dev/fb1.
  While this appears to be true for all Waveshare 2.8 inch touchscreens connected to a Raspberry Pi 0,
  you may need to change "/dev/fb1" to another location in your file system.

We then `convert <https://www.pygame.org/docs/ref/surface.html#pygame.Surface.convert>`_ the *self.surface* to 16-bit depth - instead of the 24 used by pygame - and then
gets the buffer object associated with *self.surface* (in 16-bit depth) with the `get_buffer <https://www.pygame.org/docs/ref/surface.html#pygame.Surface.get_buffer>`_ method.
This buffer object is written to the framebuffer file with the python `write method <https://www.w3schools.com/python/ref_file_write.asp>`_.
We then close the framebuffer file with the `close method <https://www.tutorialspoint.com/python/file_close.htm>`_.

.. note::

    This method for updating the screen was also based on the content of Attila's Stack Overflow post:
    https://stackoverflow.com/questions/54778105/python-pygame-fails-to-output-to-dev-fb1-on-a-raspberry-pi-tft-screen/54986161#54986161.

If the program is not ran on a Raspberry Pi, we simply use the pygame method `pygame.display.update <https://www.pygame.org/docs/ref/display.html#pygame.display.update>`_
to update our display.

Lastly, the program calculates the time it took to refresh the screen and sotres it in ``refresh_time``.
Should that time be shorter that :math:`1 / \text{FPS_gui} = 1/30` of a second,
the program will use the `sleep method <https://www.programiz.com/python-programming/time/sleep>`_ to suspend
its activity for the amount of time required so that the overall time taken by the ``refresh`` method will be
:math:`1 / \text{FPS_gui} = 1/30` of a second, minimum.

.. warning::

    You may find that your program is using your device's capacities too intensely, especially if it is a Raspberry Pi 0.
    In that case, we suggest that you comment the last 3 lines of the ``refresh`` method, by placing a # sign at the beginning,
    and uncomment the last line ``time.sleep(1 / variables.FPS_gui)``, by deleing the # sign at its beginning.

    This will force the program to suspend its activity at the end of each refresh, which might make your UI go slighlty slower
    but will not use your device's capacities as intensely.


get_coord method
----------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.refresh
    :linenos:
    :language: python

This method is called to fetch the coordinates of a touch to the screen in the form of a list of the form [x_coord, y_coord]
if there is a touch to the screen. Otherwise, it returns ``None`` if the screen is not touched at the moment of execution. The way to fetch
the coordinates greatly differs depending on whether a Raspberry pi is used or not.

Code flow
^^^^^^^^^^^^^^^

This method first starts by storing the value of *self.coord* (if it is not ``None``) in
*self.last_coord*, i.e. *self.last_coord* will always be a list of coordinates and never ``None`` again
after the ``__init__`` method.

Then, we check if the time elapsed since the last touch, effectively stored in *self.start*, is smaller than 0.1 second.
If it is smaller, we consider the last touch to be too recent for there to be a new touch, and simply return ``None``.

if it is not, we initialise a new list coord as ``coord = [-1, -1]``. Its two elements
will take the values of the touch coordinates by the end of the method and it will then be returned.

We store its first element in the variable ``x_touch``, and its second element in ``y_touch``.

We now initialise two tuple variables: ``orig`` and ``end``, as ``(3735, 130)`` and ``(3835, 195)``, respectively.

.. note::

  The values of ``orig`` and ``end`` are used to map from the touch coordinates of the screen obtained with evdev
  to pygame surface coordinates - between 0 and 320 for the x coordinate and between 0 and 240 for the y coordinate by default.
  These values were obtained **empirically** by touching the corners of the screen and comparing the evdev coordinates with what
  they should be. While this *should* work for the default Waveshare 2.8 inch touchscreen, you may need to modify these values
  if you are using a different screen, otherwise the touch would be off.
  Uncomment the line that reads ``# print(x_touch, y_touch)`` to find out the value of your screen evdev coordinates
  and adapt the values of ``orig`` and ``end`` accordingly. This can be done by clicking points with known coordinates:
  the four corners of your screen.

If the code is ran on a Raspberry Pi, we use the `select.select <https://docs.python.org/2/library/select.html#select.select>`_
function to wait until *self.touch* is ready for reading. The associated list will be a list of all the evdev events
that occured to the screen, such as a touch. However, should no event have occured to the screen (as is often the case),
the select.select function will time out after 0.1 second and raise a BlockingIOError.
If that error is raised, we simply return ``None`` for the coord value: indicating that there was no touch in this iteration of the
``while True`` loop.

If there is no time out, we iterate through all the events as ``evt`` associated with *self.touch*, obtained with ``self.touch.read()``.
You might need to read further about the `documentation of evdev.events <https://python-evdev.readthedocs.io/en/latest/apidoc.html>`_
to fully understand what is being done next.

Each event has a type, a code and a value associated to it.
We check that our event is of the type ``EV_ABS``, i.e. an absolute axis event, that describes a touch on the touchscreen.
We also check that the event code is not "24", which is the evdev code associated with the value of the pressure on the
touchscreen, which is irrelevant when reading touch coordinates.

If both these conditions are met, the event must be an event describing either the potision along the X axis (event code 1),
or along the Y axis (event code 0).
We therefore check the event code and if the associated coordinate (``x_touch`` or ``y_touch``) is still ``-1``, i.e., if we have not collected any information
about the associated axis, we take the event value and store it in the appropriate variable (``x_touch`` or ``y_touch``).

Once both ``x_touch`` and ``y_touch`` are no longer ``-1``, we `break <https://www.tutorialspoint.com/python/python_break_statement.htm>`_ out of our for loop
as we now have fetched the evdev coordinates associated to the touch.

Next, if ``x_touch`` and ``y_touch`` are no longer ``-1``, we convert them to surface coordinates -
``x_touch`` must be in the range :math:`[0, 320]` and ``y_touch`` must be in the range range :math:`[0, 240]`.

Our formula for the new value of the X coordinate is :math:`\frac{ \text{x_touch} - \text{orig[1]}}{\text{orig[0]} - \text{orig[1]}} \times self.width`
Our formula for the new value of te Y coordinate is :math:`\frac{ \text{end[0]} - \text{y_touch}}{\text{end[0]} - \text{end[1]}} \times self.height`

.. note::

  Once again, this method for fetching the screen evdev coordinates was also based on the content of Attila's Stack Overflow post:
  https://stackoverflow.com/questions/54778105/python-pygame-fails-to-output-to-dev-fb1-on-a-raspberry-pi-tft-screen/54986161#54986161.

If the code is **not** ran on a Raspberry Pi, we simply use the pygame built in `pygame.mouse.get_pos <https://www.pygame.org/docs/ref/mouse.html#pygame.mouse.get_pos>`_
to get the position of the cursor. Note however that this returns the position of the cursor at all times and not
simply when there is a touch, hence the need for the *self.py_touch* attribute that says if there was a touch
in the current iteration of the ``while True`` loop of the **refresh.py** file.

If ``x_touch`` and ``y_touch`` are then no longer ``-1``, we redefine coord as
``coord = [int(x_touch), int(y_touch)]``. Note the conversion to integers, as our coordinate may not be
integers following our scaling if the code is ran on a Raspberry Pi.

If the code is ran on a Raspberry Pi, we then only return ``coord`` under the peculiar condition:
``self.last_coord is None or (abs(coord[0] - self.last_coord[0]) > 0.01 * self.width or abs(coord[1] - self.last_coord[1]) > 0.01 * self.height)``.



The ``self.last_coord is None`` is present so that the rest of our condition does not make the program crash when *self.last_coord* is ``None`` before the first touch
after the ``__init__`` method.
The second part of our condition checks whether the difference between our last touch coordinates (*self.last_coord*) and coord
is more than 1% of the screen dimensions both for the X axis and for the Y axis.
If that were not the case we would simply return ``None``.
The goal of this line is that a touch lasting longer than an iteration of the ``while True`` loop (as most touch do),
is taken into account **once only**. Otherwise, at least in our experience, the Raspberry Pi often interprets one touch
as a double touch resulting in painful results such as a CheckBox object being checked and unchecked when clicked "once".
You may wish to change the 1% threshold in that condition to more or less (even 0, which would take all touch events into acount),
but beware of the consequences.

If the program is not ran on a Raspberry Pi, we simply return the pygame coordinates.

Overall flow
--------------

Here is a picture of what goes on each iteration of the ``while True`` loop in the **refresh.py** file.

.. image:: /_static/while_loop_refresh.svg
  :align: center
  :alt: alternate
