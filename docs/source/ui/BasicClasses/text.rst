The Text class
====================

This class is used to have text displayed on the screen. One may choose the font (which can be italic or bold),
as well as its size, colour. Useful to display many lines of text at once (in the same font).
This class makes use of the pygame.font submodule, which is why it starts with pygame.font.init().

__init__ method
----------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Text.__init__
    :linenos:
    :language: python

List of attributes :
^^^^^^^^^^^^^^^^^^^^^^^^

* *self.surface*: As discussed previously this is generally the screen surface.

* | *self.pos*: A tuple that determines the position of the top-left corner of the upmost
  | line of text.

* | *self.text*: **This attribute is a list of strings to be displayed with 1 string displayed/line**.
  | This implies that, should a user only want to display the text "example", *self.text* should be set to
  | ``["example"]``, and **not** ``"example"``.

* | *self.font_name*: This is the path of the font file (usually ending in **.ttf**) relative to the working directory.
  | For the smart_thermostat, we have created a fonts directory that stores various font files.
  | This allows users to download new files and use them.

* | *self.font_size*: This is the size of the font. Note that this is entirely dependent on the font used.
  | The smart_thermostat gui.py file assumes that the program is ran with OpenSans Regular by default. The font sizes are
  | therefore set with that in mind. A user wishing to modify the font size should change most absolute font sizes.

* | *self.font*: This is not set by the user, it is a pygame font object that is created from the
  | font file and the size of the font.

* | *self.font_w*: In trying to centre text, we need to know how wide and tall a text object is. *self.font_w* tells us the maximum
  | width in pixel of a line of text for our text object.

* | *self.font_h*: This is the height in pixel of **the** tallest line of text in the text object.
  | If we are displaying the text *self.text* = ["1st line", "2nd line"] over 2 lines, we would find the total height
  | in pixels occupied by the text as *self.font_h* :math:`\times` len(*self.text*).

* | *self.font_colour*: The colour of the displayed text.

text_surface method
-------------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Text.text_surface
    :linenos:
    :language: python


This method returns a list of surfaces and a list of `pygame rects <https://www.pygame.org/docs/ref/rect.html>`_. There is an item in each list
for each line of text. We iterate through each element of the list *self.text*.
For each element (each line of text), we create a new surface using the `render method <https://www.pygame.org/docs/ref/font.html?highlight=render#pygame.font.Font.render>`_
of pygame font objects and then set the position of its top left corner.
The x coordinate of the function is constant, however,
note that for more than one line of text, the y coordinate should be increased by *self.font_h* each time.
We append the relevant surface and rect to the corresponding lists.


show method
--------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Text.show
    :linenos:
    :language: python

This method first calls the text_surface method. It then iterates through the list of surfaces and rects,
and blits these to the main *self.surface* (generally the screen).
The `blit method <https://www.pygame.org/docs/ref/surface.html?highlight=blit#pygame.Surface.blit>`_ allows us to
assign a value to each pixel on our screen surface.

Final example
------------------

.. code-block:: python
    :linenos:
    
    example_text = Text(self.surface, (50, 50), ["1st line of text", "2nd line of text"],
                          self.font_name_bold_italic, 20, variables.black)
    example_text.show()

Provided that *self.font_name_bold_italic* points to the right location, this code produces:

.. image:: /_static/example_text.png
    :align: center
    :alt: alternate
