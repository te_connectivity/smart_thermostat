import sys
import os
import shutil
import datetime

dir=os.path.abspath('')
source=os.path.join(dir,"..","..","ambimate_python_driver")
destination=os.path.join(dir,"ambimate_python_driver")
print('source',source)
print('destination',destination)

try:
    shutil.rmtree(destination)
except:
    pass
shutil.copytree(source, destination)

sys.path.append(os.path.abspath("./_ext"))

project = 'Smart Thermostat'

author = 'Vincent Bénet, David Levi & Marta Opalińska'

copyright = 'TE connectivity \n Written by '+author
version = 'V 1.0'

master_doc='index'
extensions = ['sphinx.ext.autosectionlabel']

templates_path = ['_templates']
lib_path = ['_lib']
exclude_patterns = []
html_theme = 'sphinx_rtd_theme'
html_logo = "_static/logo.svg"
html_theme_options = {
    'style_nav_header_background': '#e77617',
    'prev_next_buttons_location': 'both'
}
html_favicon = "_static/favicon.ico"
html_scaled_image_link = False
autosectionlabel_prefix_document = True
html_static_path = ['_static']
html_show_sphinx=True
html_last_updated_fmt=datetime.datetime.now().strftime("%A %d %B %Y - %Hh%M")
html_js_files = ['java.js']

rst_prolog = """

.. |x| image:: /_static/icon_x.svg
    :width: 15
    :height: 15
.. |o| image:: /_static/icon_o.svg
    :width: 15
    :height: 15
.. |scheme_database| image:: /_static/scheme_database.svg
.. |scheme_cablage| image:: /_static/scheme_cablage.svg
.. |scheme_threads_run| image:: /_static/scheme_threads_run.svg
.. |scheme_loop_structure| image:: /_static/scheme_loop_structure.svg
.. |scheme_trigger_path| image:: /_static/scheme_trigger_path.svg
.. |scheme_main_menu| image:: /_static/scheme_main_menu.svg
.. |scheme_welcome_menu| image:: /_static/scheme_welcome_screen.svg
.. |scheme_graphs_menu| image:: /_static/scheme_graphs.svg
.. |scheme_option_menu_1| image:: /_static/scheme_options1.svg
.. |scheme_option_menu_2| image:: /_static/scheme_options2.svg
.. |scheme_settings_menu| image:: /_static/scheme_settings.svg
.. |screen_fileZilla_connection| image:: /_static/screen_fileZilla_connection.png
.. |screen_fileZilla_transfert| image:: /_static/screen_fileZilla_transfert.png
.. |graph_regulation_AC| image:: /_static/graph_regulation_AC.png
.. |gif_components| image:: /_static/gif_components.gif
.. |gif_connections| image:: /_static/gif_connections.gif
.. |gif_front_vi| image:: /_static/gif_front_vi.gif
    :width: 80%
.. |gif_back_vi| image:: /_static/gif_back_vi.gif
     :width: 80%
.. |gif_middle_vi| image:: /_static/gif_middle_vi.gif
     :width: 80%
.. |gif_moon| image:: /_static/gif_moon.gif
     :width: 80%
.. |gif_step_3_ras| image:: /_static/gif_step_3_ras.gif
.. |gif_step_6_ras| image:: /_static/gif_step_6_ras.gif
.. |gif_step_7_ras| image:: /_static/gif_step_7_ras.gif
.. |gif_step_1_pac| image:: /_static/gif_step_1_pac.gif
.. |gif_step_2_pac| image:: /_static/gif_step_2_pac.gif
.. |gif_step_1_user| image:: /_static/gif_step_1_user.gif
.. |gif_step_2_user| image:: /_static/gif_step_2_user.gif
.. |gif_step_3_user| image:: /_static/gif_step_3_user.gif
.. |gif_step_4_user| image:: /_static/gif_step_4_user.gif
.. |pic_te| image:: /_static/poster_te.png
.. |pic_components| image:: /_static/pic_components.png
.. |pic_components_optional| image:: /_static/pic_components_optional.png
.. |graph_analyser_1| image:: /_static/graph_analyser_1.png
.. |graph_analyser_2| image:: /_static/graph_analyser_2.png
.. |graph_analyser_3| image:: /_static/graph_analyser_3.png
.. |graph_analyser_4| image:: /_static/graph_analyser_4.png
.. |graph_regulation_HE| image:: /_static/graph_regulation_HE.png
.. |pic_step_5_con| image:: /_static/pic_step_5_con.JPG
.. |pic_step_final| image:: /_static/1_pic_step_17_con.JPG
.. |pic_step_1_ras| image:: /_static/pic_step_1_ras.PNG
.. |pic_step_2_ras| image:: /_static/pic_step_2_ras.png
.. |pic_step_5_ras| image:: /_static/pic_step_5_ras.PNG
.. |pic_step1_workshop| image:: /_static/pic_step1_workshop.jpg
.. |pic_step2_workshop| image:: /_static/pic_step2_workshop.jpg
.. |pic_step3_workshop| image:: /_static/pic_step3_workshop.jpg
.. |pic_step4a_workshop| image:: /_static/pic_step4a_workshop.jpg
.. |pic_step4b_workshop| image:: /_static/pic_step4b_workshop.jpg
.. |pic_step5_workshop| image:: /_static/pic_step5_workshop.jpg
.. |pic_step6a_workshop| image:: /_static/pic_step6a_workshop.jpg
.. |pic_step6b_workshop| image:: /_static/pic_step6b_workshop.jpg
.. |pic_step6c_workshop| image:: /_static/pic_step6c_workshop.jpg
.. |pic_step6d_workshop| image:: /_static/pic_step6d_workshop.jpg
.. |pic_step6e_workshop| image:: /_static/pic_step6e_workshop.jpg
.. |gif_waking_up| image:: /_static/gif_waking_up.gif
.. |gif_switching_temperatureORhumidityORetc| image:: /_static/gif_switching_temperatureORhumidityORetc.gif
.. |gif_changing_target| image:: /_static/gif_changing_target.gif
.. |gif_changing_to_fahrenheid| image:: /_static/gif_changing_to_fahrenheid.gif
.. |gif_changing_to_manual_mode| image:: /_static/gif_changing_to_manual_mode.gif
.. |gif_graph| image:: /_static/gif_graph.gif
.. |gif_settings| image:: /_static/gif_settings.gif
.. |gif_display_setings| image:: /_static/gif_display_setings.gif

.. |badge_pipeline| image:: https://gitlab.com/te_connectivity/smart_thermostat/badges/master/pipeline.svg
    :target: https://gitlab.com/te_connectivity/smart_thermostat/commits/master
.. |badge_coverage| image:: https://te_connectivity.gitlab.io/smart_thermostat/coverage.svg
    :target: https://te_connectivity.gitlab.io/smart_thermostat/coverage.html

.. image:: /_static/logo_orange.svg
    :align: right
    :width: 20%

.. |Filezilla| raw:: html

   <a href="https://filezilla-project.org/" target="_blank">Filezilla</a>

.. |DB| raw:: html

   <a href="https://sqlitebrowser.org/dl/" target="_blank">DB browser</a>

.. |Ambimate| raw:: html

   <a href="https://www.te.com/usa-en/product-CAT-AM16-M8797.html?q=ambimate&source=header" target="_blank">Ambimate MS4 Multi-sensor module</a>

.. |Relay| raw:: html

   <a href="https://www.bc-robotics.com/shop/raspberry-pi-4-channel-relay-hat/" target="_blank">Raspberry Pi 4 Channel Relay HAT (RAS-075) </a>

.. |Zero| raw:: html

   <a href="https://thepihut.com/products/raspberry-pi-zero?src=raspberrypi" target="_blank">Raspberry Pi Zero</a>

.. |Waveshare| raw:: html

   <a href="https://www.waveshare.com/2.8inch-rpi-lcd-a.htm" target="_blank">Waveshare 2.8inch RPI LCD screen</a>

.. |Camera| raw:: html

   <a href="https://thepihut.com/products/zerocam-camera-for-raspberry-pi-zero" target="_blank">Pi Zero Camera</a>

.. |Connector| raw:: html

   <a href="https://www.digikey.co.uk/product-detail/en/te-connectivity-amp/102387-9/A25909-ND/289282?utm_adgroup=&mkwid=sjkovOiAN&pcrid=338227911347&pkw=&pmt=&pdv=c&productid=289282&slid=&gclid=EAIaIQobChMI8_Ps3bqW5AIVy7TtCh29Fw2_EAQYAyABEgIxCPD_BwE" target="_blank">Female connector (2 row x 20 pins)</a>

.. |Power| raw:: html

   <a href="https://thepihut.com/products/official-raspberry-pi-universal-power-supply?src=raspberrypi/" target="_blank">Raspberry Pi Power Supply</a>

.. |Connector 2| raw:: html

   <a href="https://www.digikey.com/product-detail/en/te-connectivity-amp-connectors/9-102975-0-13/A34346-13-ND/1141860" target="_blank">Male right angle connector (2 row x 13 pins)</a>
"""

rst_epilog = """

.. image:: /_static/header.svg
    :width: 100%


"""
