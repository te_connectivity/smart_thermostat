import inspect
import os
import sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))  # Get current dir
parentdir = os.path.dirname(currentdir)  # get parent dir
dir = parentdir + "/"
sys.path.insert(0, dir)  # insert path of parent dir in import settings

from convergence import *

class Test(unittest.TestCase):

    def setUp(self):
        St=settings.Settings()
        St.set("is_unittest",True)
        self.Db = db.DB()
        self.target = 10**3
        self.outer = 10**2
        self.out = 10**1
        self.Db.clear_bdd()
        self.Db.insert_sensor("test_sensor", self.target, self.outer, self.out)
        self.Db.insert_actuator("test_actuator")
        self.id_sensor = self.Db.get_sensor_id("test_sensor")
        self.id_actuator = self.Db.get_actuator_id("test_actuator")
        self.Db.insert_actuator__sensor(self.id_sensor, self.id_actuator ,1, 0)
    
    def test_analyse(self):
        temps=time.time() - variables.backup_steps / variables.FPS_sensor
        self.Db.add_action(self.id_actuator,1,time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(0)))
        value=10
        for i in range(variables.backup_steps):
            value = 200 + i / variables.backup_steps * 300
            temps += 1 / variables.FPS_sensor
            date=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(temps))
            self.Db.add_measure(self.id_sensor, value, date)
            
        analyser.loop()
        date_target=self.Db.get_sensor_target_time(self.id_sensor)
        print(date_target,time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
        self.assertEqual(date_target>time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(temps)), True)
        
if __name__ == '__main__':
    unittest.main()