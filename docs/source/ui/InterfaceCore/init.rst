The __init__ method
=========================

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.__init__
    :linenos:
    :language: python

Initialisation of the touchscreen (if the program runs on a Raspberry Pi)
---------------------------------------------------------------------------

We start by defining the attribute *self.set* as the class ``Settings`` of
the **settings.py** file.
This will allow us to set and get system settings, such as the mode - auto or manual - that
the UI should run with or the temperature unit (°C or °F) to use upon initialisation.

We immediately use that attribute to know what device the program is ran on,
with ``self.is_raspberry = self.set.get("is_raspberry")``, that stores the boolean value
of ``is_raspberry`` in the attribute *self.is_raspberry*.

Next, we check that the program is running on a Raspberry Pi, and if it is, we use the evdev
module to fetch a list of the devices connected to the Raspberry Pi.

This use of the evdev module to make pygame and the touch events work on the Waveshare touchscreen
was based on a post by Attila on Stack Overflow:
https://stackoverflow.com/questions/54778105/python-pygame-fails-to-output-to-dev-fb1-on-a-raspberry-pi-tft-screen/54986161#54986161.

We first check that the evdev module was indeed imported at the top of the file and print out an error message
("evdev module NOT imported") if that is not the case.
We then create a list of all the devices connected to the Raspberry Pi using evdev and store it in the variable ``devices``.
You may want to refer to an example in the `evdev module tutorial <https://python-evdev.readthedocs.io/en/latest/tutorial.html#listing-accessible-event-devices>`_.

We then initialise the variable ``not_touch`` as ``0``. This variable will be a counter that will count
the number of devices connected to the Raspberry Pi that are **not** the Waveshare touchscreen.

We then iterate through the list ``devices``.
For each device, we check whether the name of the device is the string "ADS7846 Touchscreen".
If that is the case, we have found the Waveshare touchscreen, and we assign the evdev device to the attribute
*self.touch*.
Otherwise, the device is not the Waveshare touchscreen, so we increment the variable ``not_touch`` by ``1``.

After we have finished iterating through our list of devices, we check whether
``len(devices)`` is the same as ``not_touch``. If that is the case, it implies that we have iterated
through all our list of devices and have not found the touchscreen to be any of the devices.
We therefore print an error message ("No ADS7846 Touchscreen device connected.") and quit the program
with the line ``sys.exit()``: there is no point in displaying a GUI if no screen in connected.

.. warning::

    This code makes the strong assumption that your Raspberry Pi touchscreen will be a
    Waveshare touchscreen detected by evdev as having the name "ADS7846 Touchscreen" (a string object).
    This may not be the case if you are using a different Raspberry Pi touchscreen or even if Waveshare were to
    change the configuration of the 2.8 inch screen.

    Should you repeatedly get the output: "No ADS7846 Touchscreen device connected.", after checking your connection,
    you could try executing the following code taken from the evdev tutorial on your Raspberry Pi, via SSH or differently.

    .. code-block::
        :linenos:

        import evdev
        devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
        for device in devices:
            print(device.path, device.name, device.phys)

    The output should be at least one line long (provided at least one device is properly connected to your Raspberry Pi).
    If the right touchscreen is connected to you Raspberry Pi, you would get the following.

    .. code-block:: console
        :linenos:

        /dev/input/event0 ADS7846 Touchscreen spi0.1/input0

    If you see anything different, for ``device.name`` than "ADS7846 Touchscreen" in your output,
    such as

    .. code-block:: console
        :linenos:

        /dev/input/event0 Other Touchscreen spi0.1/input0

    then simply replace the line ``if device.name == "ADS7846 Touchscreen"`` with
    ``if device.name == "Other Touchscreen"`` in **gui.py**, do not forget the quotation marks.

Lastly, assuming our device has been found, and *self.touch* exists, we make sure only this program
will be handling events from the found screen with the line ``self.touch.grab()``.
You may wish to read more about the `grab method <https://python-evdev.readthedocs.io/en/latest/apidoc.html?highlight=grab#evdev.device.InputDevice.grab>`_.

List of other attributes
----------------------------

Before storing the paths to our font files, we first initialise the pygame.font module
with `pygame.font.init <https://www.pygame.org/docs/ref/font.html#pygame.font.init>`_, which will allow us
to be able to use all the functions in that module.

.. note::

    This function is also called by ``pygame.init``, but we call it here as well to be on the safe side.

* | *self.font_name* : The path to the font file to be used as the regular font in the UI.

* | *self.font_name_bold*: The path to the font file to be used as the **bold** font in the UI.

* | *self.font_name_italic*: The path to the font file to be used as the *italic* font in the UI.

* | *self.font_name_bold_italic*: The path to the font file to be used as the **bold** and *italic* font in the UI.

.. note:: About fonts

    We use the `os.path.join <https://www.geeksforgeeks.org/python-os-path-join-method/>`_ function to make our paths,
    to ensure compatibility with different operating systems.

    The default font in the TE Smart Thermostat UI is `Open Sans (Regular) <https://fonts.google.com/specimen/Open+Sans>`.
    Font directories are supposed to be stored in the **fonts** subdirectory of the **files** subdirectory of the **smart_thermostat** directory.
    You may wish to download your own fonts from `Google Fonts <https://fonts.google.com/>` or elsewhere and use them instead of Open Sans.

    .. warning::

        Be aware that if you choose your own font, some text might appear slightly differently to how it should.
        You might need to individually change the font size of some Text objects accordingly.

* | *self.surface*: This attribute takes the value of ``surface``, the only argument of the __init__ method.
  | It will from then on be considered as the screen surface on which everything is displayed.
  | Its dimensions should therefore be taken as the size of the screen in pixel - 320 by 240 pixels on the Waveshare 2.8 inch screen.

  .. note::

      If you choose to use a screen with other dimensions, change the ``screen_dimensions`` tuple in **variables.py** accordingly.

* | *self.width*: The width of the *self.surface* surface, obtained with its ``get_width`` method.
  | This is used a a reference length to set the dimensions of UI objects.

* | *self.height*: The width of the *self.surface* surface, obtained with its ``get_height`` method.
  | This is used a a reference length to set the dimensions of UI objects.

* | *self.time_format*: Time format (from `strftime abbreviations <https://docs.python.org/3/library/time.html#time.strftime>`) used to display time on the default screen of the UI, taken from **variables.py**.
  | By default, its value is ``%H:%M`` corresponding to 24 hour HH:MM format. To change its value check out the file **variables.py**, UI section.

* | *self.date_format*: Date format (from `strftime abbreviations <https://docs.python.org/3/library/time.html#time.strftime>`) used to display date on the default screen and graphs of the UI, taken from **variables.py**.
  | By default, its value is ``%d/%m`` corresponding the European dd/mm format. To change its value check out the file **variables.py**, UI section.

* | *self.db*: The ``DB`` object of the **db.py** file. This allows the UI to interact with the database **db.db** (outside of the *smart_thermostat* directory).

* | *self.pub*: The ``Pubsub`` object of the **pubsub.py** file. This allows the UI to subscribe to a channel and publish content to a channel.
  | You may want to read more about a `publish-subscribe pattern <https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern>`_.

* | *self.changed_menu*: A boolean that indicates if we just switched from one menu to another.
  | If that is the case, the UI will read content from the pubsub channels, and update its data accordingly, before resetting *self.changed_menu* to ``False`` (end of the ``display`` method).

  .. note::

    This attribute allows to not check the pubsub channels at every refresh of the screen (normally working at 30 FPS).
    However, it may cause a screen to not update as fast as expected to reflect the reality of the overall system.
    For example, if a user is looking at the Home screen, in AUTO mode and the state of an actuator is changed, this is not
    updated in real time.
    To make sure you are getting the latest information on a screen, you might need to click the associated tab on the top left of the screen again.

* | *self.menu*: This attribute stores the menu method associated with the menu to be dislayed. Upon initialisation, it is therefore set to ``self.default``.
  | Whenever the ``display`` method is called, it ends by calling *self.menu*, thereby entering the correct method and displaying what is required to the screen.

* | *self.start*: This attribute is meant to store the `Unix time <https://en.wikipedia.org/wiki/Unix_time>`_ of the last touch to the screen.
  | In the __init__ method, since no touch event have occured so far, we simply initialise it as the current time, with ``time.time()``.
  | Every time the ``display`` method is called, it checks whether the time elapsed since *self.start* (``time.time() - self.start``) is greater than the variable ``back_to_default`` of **variables.py**.
  | If that is the case, the screen is considered to have been inactive for too long and the default screen is displayed.
  | By default, ``back_to_default`` has a value of ``2 * 60`` seconds, i.e. 2 minutes. You may edit its value in **variables.py**, UI section.

  .. note::

      In the ``default`` menu method, *self.start* is used further: when the time since the default screen
      started to be displayed exceeds the ``default_te`` variable of **variables.py**, the default screen
      becomes the `TE Connectivity <https://www.te.com/usa-en/home.html>`_ logo on a white background.
      By default, ``default_te`` is set to ``20 * 60`` seconds, i.e. 20 minutes.
      You can completely disable the display of the TE logo by making the value of
      ``default_te`` be ``0`` in **variables.py**.

* | *self.last_coord*: This attribute is a tuple of the coordinates of the previous touch to the screen -
  | that corresponds to the last tuple value of ``self.coord``.
  | This is used to check whether a touch is different from the last touch on the Raspberry Pi screen.
  | This allow a "long click" in one position to not be considered as several clicks in the same place.
  | Check the ``get_coord`` method for more details.
  | The value of this attribute is initialised as ``None``.

* | *self.coord*: This attribute is a tuple that stores the coordinates of the current touch to the screen - meaning the touch fetched in the current iteration of the ``while True`` loop
  | of the **refresh.py** file, if there is one.
  | Otherwise, if there is no current touch it takes the value ``None`` (see the ``get_coord`` method).
  | It is initialised with value ``None``.

* | *self.mode*: This attribute takes the value "auto" or "manual". While its 2 values suggest that it could be a boolean,
  | it has been left as a string to allow for the creation of other modes.
  | It fetches the value it should be in from the **settings.json** file (located outside the *smart_thermostat* directory).

  .. note::

      In "auto" mode, which is the default, the user does **not** have direct control over the actuators connected to
      the Raspberry Pi. The user can simply set the target and the **analyser.py** and **relay.py** will take care
      to autonomously set the state of the actuators.

      However, in "manual" mode, the user may click the actuator tabs at the bottom of the Home menu,
      and directly control the state of the actuators from there. This however implies that there is no concept
      of a "target value" to be reached and leaves the user in full control.

      The following are possible applications:

          * | If you leave your house for a few weeks, there is no use in keeping your system working.
            | You might want to switch the mode to manual and switch all acutators off.

          * | If upon coming back in your house, you find out that the value of one of the measured
            | properties is very different to what you expected, you might want to switch to manual mode and
            | set the actuators value yourself.

* | *self.py_touch*: This attribute is only useful when operating the UI on a laptop.
  | It is normally ``False`` and only becomes ``True`` when a click is detected by pygame.
  | Otherwise, everytime the mouse goes over the UI window on a laptop, it would interpret this as a touch at this point.
  | This is obviously not practical, so we make sure that a click is detected by pygame before interpreting any
  | coordinates as coordinates of a touch, when running on a laptop.
  | Otherwise, when the UI is operated on a Raspberry Pi, the value of *self.py_touch* is reset to ``True`` at the beginning and at the end
  | of the ``while True`` loop in **refresh.py**.

* | *self.actuator_dict*: As you might guess this attribute is a dictionary that has ``key: value`` pairs of the
  | form ``"actuator name": actuator state``, for all the actuator names in the column *name* of the *actuator* table of **db.db**.

* | *self.list_sensor_names*: This attribute is a python list of all the sensors' name stored
  | in the column *name* of the *sensor* table in **db.db**.

  .. note::

      The values stored in this column are generally the name of the property that
      the sensor measures. For example, it would be "Temperature" for the temperature sensor.

* | *self.list_sensor_displayed*: This attribute is a python list of the name of all the sensors that
  | the user wishes to have information about in the UI. It is initialised as equal to *self.list_sensor_names*.
  | Whether a given sensor's info is displayed may be chosen from the second screen of the Options menu.
  | We henceforth refer to the sensors in that list as *displayed sensors*.

* | *self.list_sensor_id*: This attribute is a python list of all the sensors' id as stored in the database. It uses python `list comprehension <https://www.pythonforbeginners.com/basics/list-comprehensions-in-python>`_
  | and is defined as ``[self.db.get_sensor_id(name) for name in self.list_sensor_names]``
  | and therefore has a one on one correspondence with *self.list_sensor_names*: for example, a sensor that
  | has its name stored as ``self.list_sensor_names[0]`` would have its database id equal to ``self.list_sensor_id[0]``.

* | *self.list_sensor_id_displayed*: This attribute is a python list of all the displayed sensors' id as stored in the database.
  | Note that this attribute is to *self.list_sensor_displayed*, what *self.list_sensor_id* is to *self.list_sensor_names*.

* | *self.list_sensor_dict*: This attribute is a dictionary used to store all the data relating to one sensor.
  | Its keys are the elements of *self.list_sensor_names*, i.e. the names of all the sensors recorded in the database.
  | Its values are other dictionaries of the form:

  .. math::

      \{\text{"name": the name of the sensor},
        \text{"sensor_id": the id of the sensor},
        \text{"unit": the unit associated with the property of the sensor},
        \text{"current": the last value measured by the sensor},
        \text{"target": the target value set for the sensor},
        \text{"setting_delta": the current value of the delta with the target value, calculated from the setting table},
        \text{"target_time": the calculated time taken to reach the target},
        \text{"derivative_up": the average positive rate of change of the property in unit / second },
        \text{"derivative_down": the average negative rate of change of the property in unit / second},
        \text{"outer": the value in the "outer" column of the sensor table for this sensor},
        \text{"out": the value in the "out" column of the sensor table for this sensor},
        \text{"active": a boolean value that indicates whether the sensor's value can be modulated by any actuator (True if yes)}
        \}

* | *self.list_sensor_dict_displayed*: This attribute is equivalent to *self.list_sensor_dict* but with its keys
  | being te elements of *self.list_sensor_displayed* rather than *self.list_sensor_names*.

.. note::

    After these attributes are initialised, the program will check what the value of the unit for temperature is,
    if temperature is one of the sensors in the database. It will then store this value in the file **settings.json**,
    located outside of the *smart_thermostat* directory, as the value of the key ``"temp_unit"``.

* | *self.last_measure*: This attribute is meant to give the Unix time (see *self.start*) of the time when the values of the
  | Ambimate sensors was last fetched from the database. The database stores new time values every :math:`1 / variables.FPS_sensor`
  | so when ``time.time() - self.last_measure`` exceeds that value, the program fetches the new values of the sensor from the database,
  | and reinitialises *self.last_measure* as ``time.time()``.

* | *self.default_property*: This should be the name of the sensor that the user wishes to see in the Default menu, as entered in the database.
  | We have crudely assumed that users would want that property to be temperature, hence the initial value of ``"Temperature"``, but it
  | may be changed to any other property name in *self.list_sensor_names*. This attribute takes its initial value from
  | the variable ``default_propery`` in **variables.py**.

* | *self.home_property*: This attribute is the name of the property being displayed in the Home menu.
  | Setting it to ``"Temperature"`` in the ``__init__`` method simply means it will be the first property seen when entering the Home
  | menu after a reboot.

* | *self.options_screen*: This attribute takes the value ``0`` or ``1`` depending on which screen of the Options menu should be displayed,
  | either the one to set the unit of temperature and the mode (value ``0``), or the one to set which property are to be displayed in the home menu (value ``1``).
  | Upon initialisation, it is set to ``0``.

* | *self.options_screen1_displaying*: This attribute corresponds to which page of the checklist in the Options menu (screen ``1``, as discussed above).
  | should be displayed.

* | *self.setting_step*: This attributes correspond to the number of settings that may be displayed at once in the Settings menu.
  | It is set to 1 for the Waveshare 2.8 inch screen, but its value may be increased for users who wish to use a larger screen.

* | *self.table_setting*: This attribute is a list of list of tuples. Each tuple corresponds to one line of the *setting* table of **db.db**.
  | Each sublist contains as many tuples as *self.setting_step*, so 1 tuple by default.
  | Note that we filter any ``None`` values from that table to account for the fact that no settings might have been entered by the user.

* | *self.setting_page*: This attribute is an index for the list *self.table_setting*. In effect, all settings displayed on a
  | given page of the Settings menu are in the list ``self.table_setting[self.setting_page]``.
  | It is set to ``0`` by default, corresponding to displaying the first page of settings.

* | *self.new_setting_dict*: This attribute is a dictionary used to temporarily store new values of settings when a new setting is being
  | entered in the New Setting menu. Once the new setting has been fully entered, a new line is added in the *setting* table in the database,
  | and this attribute reverts to its initial value.

* | *self.X_database*: Variable to store X coordinate for displaying the graph.

* | *self.Y_database*: Variable to store Y coordinate for displaying the graph.

* | *self.graph*: This attribute may take the value ``"D"``, ``"W"``, or ``"M"``, which correspond to "day", "week", or "month", respectively.
  | It dictates whether the user wishes to display a graph for the last 24 hours, last 7 days, or last 30 days.

* | *self.start_graph*: This attribute takes the value of the Unix time of the point where the graph should start.
  | For example, if *self.graph* is ``"W"``, its value should be ``time.localtime(time.time() - 3600 * 24 * 7)``.

* | *self.max_measure_group*: This attribute corresponds to the number of instances from ``measure`` table averaged into one value. Graphs menu.
  | Its value comes from the variable ``av_day`` in **variables.py**, Ui section. It may be changed.

* | *self.measure_interval*: Time interval between each average. Calculated as time divided by the number of points on the graph. 

* | *self.measures*: This is a list of tuples of the form (date and time, data from the ``graph`` table of the sensor shown in Home at this date and time).
  | It should be evenly spread in the time interval between self.start_graph and time.time() (the cureent value of Unix time).

  .. note::

    Notice the try/except KeyError around the initialisation of this attribute.
    This is present because, upon the first start of your system, there may not be any data in the *measure* table of the database.
    Should that be the case, a KeyError is raised and the program will ``sleep`` for a time :math:`1 / variables.FPS_sensor`, waiting for the sensors to log a measure in the *measure table*.

* | *self.values*: This attribute is a dictionary with key: value pairs of the form "attribute name": value of attribute.
  | This is present for convenience only, allowing us to easily change the value of any attribute in conjunction with the ``change_attribute`` method.
  | Note that is it defined as ``self.values = vars(self)``. You may want to read about the `vars function <https://www.geeksforgeeks.org/vars-function-python/>`_.
