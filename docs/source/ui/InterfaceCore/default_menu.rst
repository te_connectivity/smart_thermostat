Default Menu
====================

We now move on to our 6 menu methods. Recall that these methods are used to write
"Basic Classes" objects to *self.surface*. They are called at the end of the ``display`` method such that on the next call of
the ``refresh`` method, the screen is updated to reflect what should has been written to the surface.

Without further ado, we examine the default method, which can produce the following menu to the screen:

.. image:: /_static/pic_gui_default_screen.PNG
    :align: center
    :alt: alternate

Code flow
------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.default
    :linenos:
    :language: python

The idea of this menu is that it appears to the screen after a prolonged period of inactivity,
inactivity here meaning no touch event detected by the screen.
There are in fact two default screens: either this one, which we will refer to as default screen 1:

.. image:: /_static/pic_gui_default_screen.PNG
    :align: center
    :alt: alternate

or this one, which we will refer to as default screen 2:

.. image:: /_static/example_image.png
    :align: center
    :alt: alternate

Which default screen is displayed on the screen depends on how long the screen has been inactive for.
Default screen 1 is displayed when the screen has been inactive for more than the value (in seconds) of the variable
``back_to_default`` and less than the value of ``default_te + back_to_default``, both found in **variables.py**. By default ``back_to_default`` is set to :math:`2 \times 60` seconds, that is 2 minutes,
and ``default_te`` is set to :math:`20 \times 60` seconds, that is 20 minutes.
You can change these values at will.

.. note::

    If you want to never display default screen 2 with the TE Connectivity logo, you can do so
    by setting the value of ``default_te`` to 0.

Default Screen 1
^^^^^^^^^^^^^^^^^^^^^^^^^^

The program first starts by fetching the dictionary containing all information relative to the "default property".
Recall that this value comes from the ``default_property`` variable, in **variables.py**, UI section.
This fetching is done by ``default_dict = self.list_sensor_dict[self.default_property]``.
We will then porgressively write the screen element to *self.surface* and the screen will be updated accordingly,
on the next call of the ``refresh`` method.

We first initialise a the background as a Box object (``box``). This is done again many times thoughout the user interface.
Our standard background is a box with a colour gradient from ``variables.dark_grey`` to ``variables.light_grey``.
Our box is directly shown by calling the the ``show`` method.
Next we initialise a Clock object to display the time and the date on the default screen.
Note that the position of the text object drawn by the ``show_time`` and ``show_date`` methods, depend on the
dimensions of said text. This allows us to position them so that the time text is 5% of the width to the right of the screen left border,
and so that the date text is 5% of the width to the left of the screen right border.

We then fetch the unit and the current value of the "default_property", by using
``temp_unit = default_dict["unit"]``
and ``current_temp = round(self.temp_given_unit(default_dict["current"]), 1)``.
Note the if condition on the default property. Indeed, if the default property is not
temperature, the value ``default_dict["current"]`` should not be run through the ``temp_given_unit`` method.
You may want to check the documentation of the built in `round <https://docs.python.org/3/library/functions.html#round>`_
Python function. Note that here we are rounding to 1 significant digit after the decimal point, which reflects the precision of the
Ambimate sensor module.

We then seek to display the current value of the "default property" on the default screen.
Since the value displayed may be quite large, rather than choosing one font size, we seek to find the maximum
font size for this text that will not make our text go off screen.

We therefore initialise a variable ``font_size`` as 1, and some booleans ``in_width`` and ``in_height``, that describe
whether the text fits in 90% of the screen's width and half of the remaining height (after the clock text has been written).
We then initialise a Text object with text attribute ``[str(current_temp) + " " + temp_unit]``. This would effectively write to the screen
something like "33.2 %" when the default property is set to ``"Humidity"``.
We then increment our ``font_size`` in steps of 1 to make our text object as big as possible.
Lastly, we place that text at the centre of the remaining space of the screen.

Then, if the default_property is "active", meaning it can be controlled by one of the actuators,
and if the program is operated in "auto" mode, we will then display the target and the
time to reach the target as calculated from the ``get_time_to_target`` method.

This is done in a font size that is about half the font size used to display the default sensor current value.
Note that the target displayed is the **actual target** at the time, that is the base target + any delta from the currently
applied settings.
We only display the time to target if the value returned by ``get_time_to_target`` is not ``"..."``, which
corresponds to an error in the data or to a situation where the analyser loop is still calculating the time
it will take to reach the target.

We also make use of our Image class to put an icon of a stopwatch before the time to target string.

Default Screen 2
^^^^^^^^^^^^^^^^^^^^^^^^

If the time in seconds since the screen was last active has exceeded ``default_te + back_to_default``,
the second default screen is shown.
This one is much easier to understand: it is simply the TE Connectivity logo on a white background.
We first initialise and show a white Box object filling the screen.
We then rescale our logo to appropriately fit on the screen, leaving the right amount of space for its border.
The logo is centred on the screen and displayed.

End of the method
^^^^^^^^^^^^^^^^^^^^^^^^^

The default method ends with a few crucial lines of code:

.. code-block:: python
    :linenos:

    if self.coord is not None and self.py_touch:
        self.go_to(self.home)
        self.start = time.time()

What does this do?
Our if condition amounts to "Is a click detected by the screen?", indicated by the fact that *self.coord* is not ``None``, but a list
containing some coordinates, and *self.py_touch* is ``True``.
Recall that *self.py_touch* is always ``True`` when the program is ran on a Raspberry Pi,
but if the program is ran on a laptop, it only becomes ``True`` when there is a click.

If there was indeed a click, we call the ``go_to`` method with the home menu method as its argument.
This means that on the next iteration of the ``while True`` loop, the screen shown will be the Home menu.
Note that we also reinitialise the attribute *self.start* to the current value of the Unix time.
Recall that this value is used to store the time when the screen was last touched.
