User guide
===========

Fisrt step is to move from screensaver to settings. To do so we need just one click on the screen.
In all setting after changing there is no need to approve changes.

.. note::
  After few seconds screen automatically will return to screensaver.

Change a sensor target
----------------------

.. list-table::
  :align: center

  * - |gif_step_1_user|

Changing target value take place in **home menu** - navigate there by arrows.

Change units and mode
-----------------------

.. list-table::
  :align: center

  * - |gif_step_2_user|

Changing target value take place in **option menu**. We can choose as units Celsius or Fahrenheit and as mode manual or automatic.
In automatic mode every actuator is set according to the target temperatur, in manual one target value is ommited and user sets all
actuators by himself.

Change actuators settings
---------------------------

.. list-table::
  :align: center

  * - |gif_step_3_user|


Setting schedule for temperature is available in **Settings menu**. First, we choose what we want to set, then what months and days of the week,
starting and finish time, and finally the change.

Consult a sensor graph
----------------------

.. list-table::
  :align: center

  * - |gif_step_4_user|

In **Graphs menu** you can see the garphs generated from measurements.
