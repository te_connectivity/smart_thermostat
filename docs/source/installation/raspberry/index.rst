.. _pi_conf:

Raspberry Pi configuration
==========================

.. toctree::
    :caption: Content:

    Rasbian setup <raspbian>
    SD card setup <sd>
    SSH setup <ssh>
    