The CheckBox Class
===========================

This class is used to display a checkbox on the screen, which may be checked or unchecked
and with a label to its right. Checkboxes mainly become handy as part of a CheckList object (see later).

__init__ method
----------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: CheckBox.__init__
    :linenos:
    :language: python

List of attributes :
^^^^^^^^^^^^^^^^^^^^^^^^

* *self.surface*: As discussed previously this is generally the screen surface.

* *self.pos*: A tuple giving the coordinates of the top left corner of the checkbox.

* *self.name*: A Text object that will be shown as the label of the checkbox to its right.

* | *self.action*: A function that is called (if callable) when the checkbox is checked.

* | *self.value*: A boolean representing the value of the Checkbox object.
  | When it is ``True``, the checkbox is checked, when it is ``False``, the checkbox is unchecked.

* | *self.box*: A square Box object. It is initialised as a white square box, with a black line
  | around it of default width (normally 1), and has side length equal to the height of the text
  | of *self.name*. Note that it will be reinitialised later and is included here for clarity.
  | This will be the "actual" checkbox as seen by the user.

show method
----------------

.. literalinclude:: ../../../../gui.py
    :pyobject: CheckBox.show
    :linenos:
    :language: python

Code flow
^^^^^^^^^^^^^^^

We first start by recreating the object *self.box* then calculate the position of
*self.name* with respect to *self.box* (half a side length to its right and at the same height),
and display *self.name*.

We then look at *self.value*. If it is ``True``, we change the colour of the box to green
and the colour of its border to white. We also create two white Line objects corresponding to the
two diagonals of the checkbox.

Once this is done, we display the checkbox to the screen and if *self.value* is ``True``,
the two diagonal lines.

act method
---------------

.. literalinclude:: ../../../../gui.py
    :pyobject: CheckBox.act
    :linenos:
    :language: python

This method takes coordinates as its argument. It first check whether the coordinates
correspond to a position within the checkbox, with the booleans ``in_width``
and ``in_height``. If both are ``True``, i.e., if the checkbox was touched,
the attribute *self.value* switches value (from ``True`` to ``False`` or vice versa).
Then, the new value of *self.value* is checked and if it is ``True`` and *self.action*
is callable, *self.action* is called.

Final example
-----------------


.. code-block:: python
    :linenos:

    cheese_choice = False
    dessert_choice = True
    cheese_label = Text(self.surface, (0, 0), ["Cheese"],
                        self.font_name, 13, variables.black)
    dessert_label = Text(self.surface, (0, 0), ["Dessert"],
                         self.font_name, 13, variables.black)
    cheese_checkbox = CheckBox(self.surface, (100, 30), cheese_label,
                        "action", cheese_choice)
    dessert_checkbox = CheckBox(self.surface, (50, 60), dessert_label,
                             "action", dessert_choice)
    cheese_checkbox.show()
    dessert_checkbox.show()

This code produces:

.. image:: /_static/example_checkbox.png
    :align: center
    :alt: alternate


.. note::

    Since the values ``cheese_choice`` and ``dessert_choice`` are redefined before
    initialising the checkbox, their attribute *self.value* could not be changed
    with this code.
    See the Options menu and new settings menu for examples of how to change the value
    of checkboxes.
