Hardware setup
=======================================

.. toctree::
   :caption: Content:

   
   Default configuration <configuration>
   User guide <guide>
   Change database <database>
   Tests <tests>
