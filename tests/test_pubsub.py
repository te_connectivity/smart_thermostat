import os
import sys
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))  # Get current dir
parentdir = os.path.dirname(currentdir)  # get parent dir
dir = parentdir + "/"
sys.path.insert(0, dir)  # insert path of parent dir in import settings

from convergence import *

class Test(unittest.TestCase):

    def setUp(self):
        self.St=settings.Settings()
        self.St.set("is_unittest",True)
        
    def test_set(self):
        self.pub = pubsub.Pubsub()
        message="message_test"
        self.pub.set("pub_test",message)
        self.assertEqual(message,sqlite3.connect(os.path.join(tempfile.gettempdir(),'pubsub.db')).cursor().execute(
        '''
            SELECT message 
            FROM mps_messages
            ORDER BY timestamp DESC
        '''
        ).fetchall()[0][0])
        
if __name__ == '__main__':
    unittest.main()