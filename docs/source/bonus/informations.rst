Informations
============

Auto Reboot
-----------
The thermostat will reboot automatically after 24h of use.

.. code-block:: python
    :linenos:

    start_time = time.time()
    while time.time() - start_time <= 60 * 60 * 24:
        time.sleep(1)
    os.system("sudo reboot")

DataBase content deletion
-------------------------
The memory of the SD card is limited,
it means that ``measure`` ``action`` tables have to be flush when the SD full or data is older than one month.
At every boot, the program measure the database size and the free size on the SD card.

In that case, it will erase the reboot time of data on those tables.
It permits to have a sustainable system.

Pubsub content deletion
-------------------------
At every reboot, ``pubsub.db`` in the tempfile of your machine is deleted.

.. code-block:: python
    :linenos:

    try:
        os.remove(os.path.join(tempfile.gettempdir(), 'pubsub.db'))
    except:
        pass

Moreover, at every message sent, the old content of the topic is erased.
It permits to make sure that there will be no overflow of data.

.. code-block:: python
    :linenos:

    def publish(self, topic, message):
        self.cursor.execute('''
            DELETE FROM mps_messages
            WHERE topic='{0}';
        '''.format(topic))
        self.cursor.execute('''
            INSERT INTO mps_messages
            VALUES (
                :topic,
                :message,
                :timestamp
            );
        ''', {
            "topic": topic,
            "message": message,
            "timestamp": datetime.datetime.now()
        })  # Insert new message in the database
        self.connection.commit()
