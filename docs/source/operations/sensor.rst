Sensor loop
=======================================

DB setup
--------
This file permit to fill tables ``sensors``, ``actuators`` and links between them automatically,
Sensors available from the ambimate are added automatically to the database.

Get all sensors
---------------

In the ``While`` loop, all sensors available from the ambimate are scanned and added to the database.

Loop code
---------

.. literalinclude:: ../../../sensor.py
    :emphasize-lines: 1
    :language: python
    :linenos:
    :pyobject: loop
