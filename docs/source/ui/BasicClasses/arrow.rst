The Arrow Class
========================

This class is to draw an isosceles triangle pointing in one of the 4 following directions:

             * Vertically upwards ("North")
             * Horizontally to the right hand side ("East")
             * Vertically downwards ("South")
             * Horizontally to the left hand side ("West")

It may also trigger an action when clicked. Note that, in essence, an Arrow object is like a Tab object,
except it does not have text on it and has a different shape.

__init__ method
----------------------

.. literalinclude:: ../../../../gui.py
    :linenos:
    :pyobject: Arrow.__init__
    :language: python

List of attributes :
^^^^^^^^^^^^^^^^^^^^^^^^

* *self.surface*: As discussed previously this is generally the screen surface.



* *self.colour*: The colour of the inside of the triangle.



* | *self.direction*: One of the four text strings: ``"East"``, ``"North"``, ``"West"``, ``"South"``.
  |                   This defines the direction the triangle is pointing in, as defined at the top of this page.

* *self.pos*: A tuple giving the top left corner of the touch zone.



* *self.size*: A tuple giving the size of the touch zone.



* *self.action*: A python function, that will be triggered when the object's touch zone is touched.



* | *self.line_colour*: The colour of the border enclosing the triangle.
  | By default, this colour is set to ``variables.black``.


* | *self.line_width*: The width of the border enclosing the triangle.
  | By default, the line width is set to 1 pixel.

.. note::

    1. Due to the nature of the triangle, *self.pos* may not necessarily be a corner of the triangle.

    2. | The touch zone that triggers *self.action* is always a rectangle, and therefore includes
       | zones that are not part of the triangle shown.

    .. image:: /_static/triangle_precision.svg
        :align: center
        :alt: alternate

show method
---------------

.. literalinclude:: ../../../../gui.py
    :linenos:
    :pyobject: Arrow.show
    :language: python

This method first calculates the coordinates of the three triangle vertices.
These points always lie on the rectangular boundary of the touch zone, but their
position on the boundary depends on *self.direction*.

Then, the method makes use of the `pygame.draw.polygon <https://www.pygame.org/docs/ref/draw.html#pygame.draw.polygon>`_ function.
The function is used once, to draw the full triangle, and a second time to draw its boundary,
by adding *self.line_width* as a fourth argument.

act method
-------------

.. literalinclude:: ../../../../gui.py
    :linenos:
    :pyobject: Arrow.act
    :language: python

This method calls the function *self.action* (if it is callable) when a touch is detected in the touch zone.
Please refer to the documentation of the Tab.act method for more details.

Final example
-----------------

.. code-block:: python
    :linenos:

    example_triangle = Arrow(self.surface, variables.light_yellow, "North",
                             (100, 20), (50, 150), "action", variables.light_green, 2)
    example_triangle.show()

This code produces:

.. image:: /_static/example_arrow.png
    :align: center
    :alt: alternate
