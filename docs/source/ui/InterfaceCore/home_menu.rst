Home Menu
===============

.. image:: /_static/pic_gui_main_screen.PNG
    :align: center
    :alt: alternate

This menu is used to display the value of all sensors and actuators connected to the Raspberry Pi.
You can also set a target for a sensor property from this menu.

Code flow
------------

.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.home
    :linenos:
    :language: python

Like in the default menu, we start by creating the background Box object and displaying it.
We then initialise an empty list ``home_active_objects``.
All objects with an ``act`` method will be appended to this list. At the end of the menu, we will iterate through
all those objects and call the act method for each with argument *self.coord*.
This will trigger the appropriate action depending on the user touch to the screen.

Note that should the home property have been taken out of the displayed sensor in the second Options screen,
we take the first value of the list *self.list_sensor_displayed* as the new home property.
We then fetch the dictionary of the property to be displayed as ``home_dict = self.list_sensor_dict_displayed[self.home_property]``.

Top left tabs
^^^^^^^^^^^^^^^^^^

This piece of code is common to all the remaining menus and is used to display tabs for each menu in the top left corner
of the screen.

.. code-block:: python
    :linenos:

    # Define top left tabs, show them
    font_list = [self.font_name, self.font_name_bold]
    colour_list = [variables.light_blue0, variables.light_blue1]
    tab_texts = ["Home", "Options", "Settings", "Graphs"]
    tab_actions = [functools.partial(self.go_to, a) for a in [self.home, self.options, self.settings, self.graphs]]
    tab_w = int(self.height / 4)  # 60 by default
    for i in range(len(tab_texts)):
        tab_box = Box(self.surface, (tab_w * i, 0), (tab_w, tab_w / 2), colour_list[0], colour_list[1],
                      line_colour=variables.light_blue2)
        tab_text = Text(self.surface, (0, 0), [tab_texts[i]], font_list[0 + (i == 0)], 15, variables.white)
        top_tab = Tab(tab_box, tab_text, tab_actions[i])
        top_tab.show()
        home_active_objects.append(top_tab)

We define a list of fonts, colour, string texts and actions, as well as a width for our tabs.

.. note::

  The ``tab_actions`` list makes uses of the functools.partial function.
  If you do not remember its use, you should read over the documentation of the `change_displaying method <file:///C:/Users/TE392816/Documents/smart_thermostat/docs/build/html/ui/BasicClasses/checklist.html#change-displaying-method>`_.

We then iterate through our list ``tab_texts`` with index i.
For each text, we create a Box object ``tab_box`` at the appropriate position (``tab_w * i`` for the ith tab's X position).
We then create a text object ``tab_text`` with the appropriate text. Notice that the font is set to bold for whichever tab corresponds to
the current menu.
Lastly, we create a Tab object with using the defined Box object, Text object and appropriate action.
This tab is then displayed using the ``show`` method and appended to ``home_active_objects``.

We then fetch the home property's unit, current value and target from the ``home_dict``.
Notice our rounding of the form ``temp_target = round(home_dict["target"] * 2) / 2``.
This ensures that the value of the target shown is displayed to the nearest half unit.

We then create and show a blue property box on which the information about the property is displayed.
We create the title of this box (the name of the home property) and place it at the top of this box, before showing it.
Next, we create a string of the form "Current : 33.2 %", if Humidity was the current home property.

We proceed by evaluating whether the current home property is "active", i.e. whether it can be controlled by any of the actuators.

If that is the case, we will create and display many more objects in our blue property box.
We fetch the current delta with the base target from the settings, and the value of the average positive rate of change of the home property,
the average negative rate of change of the home property.

.. note::

    We will only display the above fetched properties if certain conditions are met:

    * | We always display the delta with the base target.

    * | We only display a rate of change value if it is non-zero (that is if it has been calculated).

    * | We only display the target time if the value returned by the ``get_time_to_target`` method is not "...".

We then create a multiline Text object with the appropriate strings and display it.

Next, we display the base target on the right hand side of our blue property box with 2 arrows to change it.
We first create a Text object with the target value.
We then surround this Text object with two Arrow objects with *self.action* attributes ``increment_target`` and ``reduce_target``.
These are created, placed, appended to ``home_active_objects`` and shown.

If there are more than one sensor to be displayed, we also create side arrows to navigate through the different properties.
Note that their action uses the ``change_attribute`` method to change *self.home_property* to the next or previous value
in *self.list_sensor_displayed*.

Lastly, we process to creating the actuator tabs that will be displayed at the bottom of the screen.
The names for the tabs are directly taken as the keys of *self.actuator_dict*.
Depending on *self.mode* our Tab objects will be slighlty different.
If the mode is set to "manual", our text will be black and the action of each tab will be to toggle the corresponding actuator.
If the mode is set to "auto", there is no control over the actuators for the user, so the text is greyed out and
the action attribute of the tabs is a string, i.e., not callable.
