.. raw:: html

    <script src="../../_static/_lib/src/lib/stats.js"></script>
    <script src="../../_static/_lib/src/lib/detector.js"></script>
    <script src="../../_static/_lib/src/lib/three.min.js"></script>
    <script src="../../_static/_lib/src/Madeleine.js"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="../../_static/_lib/src/css/Madeleine.css">

.. _printing:

Printing the case
=======================================

Here you can find downloadable 3D models for the case. 

STL file :download:`front part <../../_downloads/front_part_v_I.stl>`

.. centered::
  |gif_front_vi|

STL file :download:`back part <../../_downloads/back_part_v_I.stl>`

.. centered::
  |gif_back_vi|

STL file :download:`middle part <../../_downloads/middle_part_v_I.stl>`

.. centered::
  |gif_middle_vi|

STL file :download:`half-moon <../../_downloads/moon_light.stl>`

.. centered::
  |gif_moon|