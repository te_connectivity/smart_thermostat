The CheckList Class
==========================

This class uses a box object and a list of Checkbox objects. It works in a similar fashion to Radio objects,
except that more than one item in the list may be selected at once and that the associated action does not
necessarily take effect immediately. If the checklist is too long for the given box, arrow objects are added at
its top and its bottom to allow the user to browse between different checklist pages. The page number is
displayed in the top right corner of the box.

__init__ method
----------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: CheckList.__init__
    :linenos:
    :language: python

List of attributes :
^^^^^^^^^^^^^^^^^^^^^^^^

* | *self.box*: The box object in which the checklist will appear.

  .. note::

      This attribute should not be confused with the *self.box* attribute of each CheckBox object.

* | *self.list_checkbox*: a python list of Checkbox objects.
  | Together, they make up the checklist.

* | *self.check_height*: The side length of the first checkbox.
  | Used for reference spacing between the different checkboxes.

* *self.total_check*: The total number of checkboxes in the checklist.

* *self.check_per_box*: The number of checkboxes that can fit in one *self.box*.

* | *self.number_of_box*: The number of pages that the checklist will need to be divided in
  | accounting for the fact that only a certain number can fit in *self.box* at once.

* | *self.displaying*: An index corresponding to the number of the checklist page being displayed.
  .. note::
      Note that in python, indexing starts at 0, so to display the fifth page of a checklist,
      *self.displaying* should be set to 4.

* | *self.super_list*: This attribute only exists when *self.number_of_box* is greater than 1,
  | i.e., when all the checkboxes cannot fit in one *self.box*. It is a list of lists of checkboxes
  | (henceforth called sublists). Each sublist has a length of *self.check_per_box*,
  | except for the last one, which has a maximum length of *self.check_per_box*.
  | Note that ``self.super_list[self.displaying]`` effectively gives us a list of the checkboxes
  | on the checklist page number n where :math:`n = self.displaying + 1`.
  | Note also that ``len(self.super_list)`` and ``len(self.number_of_box)`` should be equal.

* | *self.triangle_list*: Initialised as an empty list.
  | If *self.number_of_box* is greater than 1, it will be a list of two Arrow objects,
  | which will allow the user to navigate between the checklist pages when touched.

show method
----------------

.. literalinclude:: ../../../../gui.py
    :pyobject: CheckList.show
    :linenos:
    :language: python

Code flow
^^^^^^^^^^^

This method works differently depending on if all the checklist can fit
in one *self.box* or not, i.e. depending on if *self.number_of_box* is
greater than 1 or not.

If :math:`\text{self.number_of_box} \leq 1`:

1. *self.box* is displayed to the screen.

2. | The code iterates through *self.list_checkbox* with index i.
   | The :math:`i^{th}` checkbox's pos attribute is set such that it is
   | :math:`\text{self.check_height} \times 2` lower than the previous one.
   | This implies that two adjacent checkboxes' topleft corner are a distance :math:`\text{self.check_height} \times 2`
   | apart and that the distance between two adjacent checkboxes is *self.check_height*.
   | The first checkbox is a distance *self.check_height* to the right of the left border
   | of *self.box* and a distance of twice *self.check_height* below the top border of *self.box*.
   | The rest of the checkboxes are vertically aligned with the first one.
   | Once its pos is set, the :math:`i^{th}` checkbox is displayed to the screen.

3. | A new Text object called ``info_text`` is initialised.
   | This text is positioned in the top right corner of *self.box* and informs
   | the user of how many pages the checklist has and which one the user is seeing.
   | In this case, when :math:`\text{self.number_of_box} \leq 1`, this text will normally always show
   | "Page 1/1".

Otherwise, if :math:`\text{self.number_of_box} > 1`:

1. *self.box* is displayed to the screen.

2. | The variable ``current_list`` takes the value of the sublist of checkboxes
   | that corresponds to the current page of the checklist. This is easily achievable with
   | the line ``current_list = self.super_list[self.displaying]``.

3. | The code iterates through ``current_list`` (and **not** as before through *self.list_checkbox*)
   | with index i.
   | Exactly like when :math:`\text{self.number_of_box} \leq 1`, the pos of the :math:`i^{th}`
   | checkbox is set and it is displayed.
   | Note the added step of storing the first and last pos in ``first_pos`` and ``last_pos``, respectively.

4. | Exactly like before, the ``info_text`` Text object is initialised, positioned and displayed.
   | Since indexing starts at 0, note that the page number is calculated as
   | :math:`\text{self.displaying} + 1`.

5. | We now create two Arrow objects ``prev_triangle`` and ``next_triangle``,
   | that will allow the user to navigate between checklist pages.
   | ``prev_triangle`` is a black triangle pointing upwards ("North"), above the first
   | checkbox (its position is calculated from ``first_pos``), and contained in a square
   | of the same dimension as the first CheckBox object.
   | ``next_triangle`` is a white triangle pointing downwards ("South"), below the
   | last checkbox (its position is calculated from ``last_pos``), and it has the same
   | dimensions as ``prev_triangle``.
   | The *action* attributes of each triangle are discussed at the end of the section about the ``change_displaying``
   | method, for now, you should simply know that when touched, ``prev_triangle`` will decrease the value
   | of *self.displaying* by 1 and ``next_triangle`` will increase it by 1.
   | This would allow to change the displayed page.
   | Both triangle are shown and appended to the *self.triangle_list* attribute.

change_displaying method
----------------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: CheckList.change_displaying
    :linenos:
    :language: python

This method takes an argument: ``increment``. It will increment the value of
*self.displaying* by the value of ``increment`` and then take the modulo of it by
*self.number_of_box* to ensure that *self.displaying* is always an integer in the range :math:`[0, \text{ self.number_of_box} - 1]`.
For example, if :math:`\text{self.number_of_box} = 6` and *self.displaying* is 0, if ``increment`` is -1,
it becomes -1 after the "increment", and after the modulo operation becomes 5. The index is therefore not out of range when taking
``self.super_list[self.displaying]``.

.. note::

    The attribute *self.action* of both Arrow objects created when :math:`\text{self.number_of_box} > 1`
    is of the form ``functools.partial(self.change_displaying, x)``. This function of the
    functools module is used extensively throughout the TE Smart Thermostat UI, and it is
    worth understanding it.

    functools.partial returns a new `"partial object" <https://docs.python.org/3/library/functools.html#partial-objects>`_
    that behaves like a new function object which is the function entered as the first argument
    of functools.partial but taking all the values entered after as arguments.

    Say we define the function ``sum(a, b)`` that prints the sum ``a + b``, as below:

    .. code-block:: python
        :linenos:

        def sum(a, b):
            print(a + b)

    For example, ``sum(2, 3)`` will print ``5``.
    Now say that we want to make a new function ``add_two(x)``, that prints the sum ``x + 2`` for any x.
    We can do that from the ``sum`` function using ``functools.partial`` as below:

    .. code-block:: python
        :linenos:

        import functools
        add_two = functools.partial(sum, 2)

    Now, ``add_two(4)`` will print ``6`` and ``add_two(1)`` will print ``3``.

    Furthermore, say we wanted a new function that always prints 7 when called.
    While this could be achieved more easily, we could use the ``sum`` function and ``functools.partial`` as below:

    .. code-block:: python
        :linenos:

        import functools
        print_seven = functools.partial(sum, 4, 3)

    Now, ``print_seven()`` will always print ``7``.

    Therefore, the *self.action* attribute of ``prev_triangle`` is a function decreasing *self.displaying* by 1 when called,
    and the *self.action* attribute of ``next_triangle`` is a function increasing *self.displaying* by 1 when called.

    You may want to `read the official documentation <https://docs.python.org/3/library/functools.html#functools.partial>`_ of functools.partial.


act method
----------------

.. literalinclude:: ../../../../gui.py
    :pyobject: CheckList.act
    :linenos:
    :language: python

This method takes the coordinate of a touch as an argument: ``coord``
This method first defines a list of active objects made up of all the checkboxes in the checklist
and of the two Arrow objects, if they exist (that is, if :math:`\text{self.number_of_box} > 1`).
It then iterates through this list and calls the act method with argument ``coord`` for each of those active objects.

Final example
---------------

.. code-block:: python
    :linenos:

    list_dessert_strings = ["Gingerbread man", "Pudding", "Lemon Tart",
                            "Raspberry pie", "Ice cream", "Tiramisu",
                            "Cheesecake", "Mango sticky rice", "Apple crumble",
                            "Fruits", "Mochi", "Waffle", "Chocolate cake",
                            "Apple tart", "Churros"]
    list_dessert_text = [Text(self.surface, (0, 0), [dessert_string],
                              self.font_name, 13, variables.black)
                         for dessert_string in list_dessert_strings]
    list_checkboxes = [CheckBox(self.surface, (0, 0), name, "action", False)
                       for name in list_dessert_text]
    checklist_box = Box(self.surface, (50, 50), (200, 170),
                        variables.light_blue0, variables.light_blue3)
    dessert_checklist = CheckList(checklist_box, list_checkboxes, 2)
    dessert_checklist.show()

Note that *self.displaying* is initialised as 2, meaning that the page shown will be the third.
The code produces:

.. image:: /_static/example_checklist.png
    :align: center
    :alt: alternate


.. note::

    Since the attribute *self.value* of each checkbox is redefined to ``False``,
    the checkboxes could not appear checked with this code.
    See the Options menu and new settings menu for examples of how to collect and change
    the values of checkboxes in a checklist.
