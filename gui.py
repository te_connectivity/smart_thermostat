''' 
   Copyright 2020 TE Connectivity
   Distributed under MIT license.
   See file LICENSE for detail or copy at https://opensource.org/licenses/MIT
'''

import calendar
try:
    import evdev
except ModuleNotFoundError:
    pass
import functools
import math
import os
import sys
import select
import time
import db
import pubsub
import settings
import variables
from datetime import datetime

with open(os.devnull, 'w') as f:
    oldstdout = sys.stdout
    sys.stdout = f
    import pygame
    sys.stdout = oldstdout


class WhatToShow:
    def __init__(self, surface):
        self.set = settings.Settings()
        self.is_raspberry = self.set.get("is_raspberry")
        if self.is_raspberry:
            if "evdev" in sys.modules:
                pass
                # print("evdev module imported")
            else:
                print("evdev module NOT imported")
            # We use evdev to read events from our touchscreen
            devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
            not_touch = 0
            for device in devices:
                if device.name == "ADS7846 Touchscreen":
                    self.touch = evdev.InputDevice(device.path)
                    # print("Touchscreen found")
                else:
                    not_touch += 1
            if not_touch == len(devices):
                print("No ADS7846 Touchscreen device connected.")
                sys.exit()

            # We make sure the events from the touchscreen will be handled only by this program
            self.touch.grab()

        pygame.font.init()
        self.font_name = os.path.join(sys.path[0], "files", "fonts", "Open_Sans", "OpenSans-Regular.ttf")
        self.font_name_bold = os.path.join(sys.path[0], "files", "fonts", "Open_Sans", "OpenSans-Bold.ttf")
        self.font_name_italic = os.path.join(sys.path[0], "files", "fonts", "Open_Sans",
                                             "OpenSans-RegularItalic.ttf")
        self.font_name_bold_italic = os.path.join(sys.path[0], "files", "fonts", "Open_Sans",
                                                  "OpenSans-BoldItalic.ttf")
        self.surface = surface
        self.width = self.surface.get_width()
        self.height = self.surface.get_height()
        self.time_format = variables.time_format
        self.date_format = variables.date_format
        self.db = db.DB()
        self.pub = pubsub.Pubsub()
        self.changed_menu = True
        self.menu = self.default
        self.start = time.time()
        self.last_coord = None
        self.coord = None
        if self.set.get("AUTO"):
            self.mode = "auto"
        else:
            self.mode = "manual"
        self.py_touch = False
        self.actuator_dict = {name: (self.db.get_actuator_id(name),
                              self.db.get_actuator_state(self.db.get_actuator_id(name)))
                              for name in self.db.get_actuator_names()}
        self.list_sensor_names = self.db.get_sensors_name()
        self.list_sensor_displayed = self.list_sensor_names
        self.list_sensor_id = [self.db.get_sensor_id(name) for name in self.list_sensor_names]
        self.list_sensor_id_displayed = [self.db.get_sensor_id(name) for name in self.list_sensor_displayed]
        self.list_sensor_dict = {self.list_sensor_names[i]: {
                                 "name": self.list_sensor_names[i],
                                 "sensor_id": self.list_sensor_id[i],
                                 "unit": self.db.get_sensor_unit(self.list_sensor_id[i]),
                                 "current": self.db.get_sensor_value(self.list_sensor_id[i]),
                                 "target": self.db.get_sensor_target(self.list_sensor_id[i]),
                                 "setting_delta": self.db.get_setting_delta(self.list_sensor_id[i]),
                                 "target_time":
                                 self.get_time_to_target(self.db.get_sensor_target_time(self.list_sensor_id[i])),
                                 "derivative_up": self.db.get_sensor_derivative_up(self.list_sensor_id[i]),
                                 "derivative_down": self.db.get_sensor_derivative_down(self.list_sensor_id[i]),
                                 "outer": self.db.get_sensor_outer(self.list_sensor_id[i]),
                                 "out": self.db.get_sensor_out(self.list_sensor_id[i]),
                                 "active": len(self.db.get_actuator__sensors_id(self.list_sensor_id[i])) > 0}
                                 for i in range(len(self.list_sensor_names))}
        for i in range(len(self.list_sensor_names)):
            print(self.db.get_sensor_target_time(self.list_sensor_id[i]))
        self.list_sensor_dict_displayed = {k: self.list_sensor_dict[k] for k in self.list_sensor_displayed}
        if "Temperature" in self.list_sensor_dict:
            self.list_sensor_dict["Temperature"]["unit"] = self.set.get("temp_unit")
        self.last_measure = time.time()
        self.default_property = variables.default_property
        self.home_property = "Temperature"
        self.options_screen = 0
        self.options_screen1_displaying = 0
        self.setting_step = 1
        # fetch settings, break in chunks of 2, take out empty elements
        self.table_setting = self.db.get_setting_parameters()
        self.table_setting = [self.table_setting[x:x + self.setting_step]
                              for x in range(0, len(self.table_setting), self.setting_step)]
        self.table_setting = list(filter(None, self.table_setting))
        self.setting_page = 0
        self.new_setting_dict = {"confirm": False}
        self.X_database = []
        self.Y_database = []
        self.graph = "D"
        if self.graph == "D":
            self.start_graph = time.localtime(time.time() - 3600 * 24)
            self.max_measure_group = variables.av_day
            # self.measure_interval = (time)/ (points on the graph) = 
            # (36000*24)/(3600*24/(1/variables.FPS_sensor*variables.av_day))
            self.measure_interval = variables.av_day* 1/variables.FPS_sensor
        elif self.graph == "W":
            self.start_graph = time.localtime(time.time() - 3600 * 24 * 7)
            self.max_measure_group = variables.av_week
            self.measure_interval = variables.av_week* 1/variables.FPS_sensor
        elif self.graph == "M":
            self.start_graph = time.localtime(time.time() - 3600 * 24 * 30)
            self.max_measure_group = variables.av_month
            self.measure_interval = variables.av_month* 1/variables.FPS_sensor
        
        self.measures = self.db.get_row_from_graphs(self.list_sensor_dict[self.home_property]["sensor_id"],
                                              self.max_measure_group)
        # uploading data to display
        try:
            # trying to pull the data from "graph" table in database
            self.X_database =[float(i) for i in self.measures[0][0].split(',')]
            self.Y_database =[float(i) for i in self.measures[0][1].split(',')]
            update = self.db.set_graph(self.list_sensor_dict[self.home_property]["sensor_id"],
                                                self.max_measure_group,
                                                datetime.utcfromtimestamp(int(self.X_database[-1])).strftime('%Y-%m-%d %H:%M:%S')) 
            self.X_database = self.X_database + [time.mktime(time.strptime(element[0], "%Y-%m-%d %H:%M:%S")) for element in update]
            self.Y_database = self.Y_database + [element[1] for element in update]
            min_y = min(self.Y_database)
            while (time.mktime(self.start_graph) >= self.X_database[0]):
                self.X_database.pop(0)
                self.Y_database.pop(0)
        except:
            # if there are no records in "graph" table pulling data straight from "measure" table
            self.measures = self.db.set_graph(self.list_sensor_dict[self.home_property]["sensor_id"],
                                                self.max_measure_group,
                                                time.strftime("%Y-%m-%d %H:%M:%S", self.start_graph))
            self.X_database = [time.mktime(time.strptime(element[0], "%Y-%m-%d %H:%M:%S")) for element in self.measures]
            self.Y_database = [element[1] for element in self.measures]
        # update of "graph" table
        self.db.insert_graph(str(self.X_database).strip('[]'), str(self.Y_database).strip('[]'),self.list_sensor_dict[self.home_property]["sensor_id"], self.max_measure_group)
        self.values = vars(self)

    @property
    def values(self):
        return self._values

    @values.setter
    def values(self, new_values):
        self._values = new_values

    def display(self):
        # print("Displaying")
        if time.time() - self.last_measure > 1 / (variables.FPS_sensor):
            for i in range(len(self.list_sensor_displayed)):
                sensor_info = self.db.get_sensor_all_info(self.list_sensor_id_displayed[i]),
                self.change_attribute("list_sensor_dict",
                                      sensor_info[0][1],
                                      sensor_info[0][0],
                                      "current")
                self.change_attribute("list_sensor_dict",
                                      sensor_info[0][2],
                                      sensor_info[0][0],
                                      "derivative_up")
                self.change_attribute("list_sensor_dict",
                                      sensor_info[0][3],
                                      sensor_info[0][0],
                                      "derivative_down")
                self.change_attribute("list_sensor_dict",
                                      self.get_time_to_target(sensor_info[0][4]),
                                      sensor_info[0][0],
                                      "target_time")

            self.last_measure = time.time()
        if self.changed_menu:
            if self.pub.get("actuator"):
                self.actuator_dict = {name: (self.db.get_actuator_id(name),
                                      self.db.get_actuator_state(self.db.get_actuator_id(name)))
                                      for name in self.db.get_actuator_names()}
            if self.pub.get("table_setting"):
                self.table_setting = self.db.get_setting_parameters()
                # The setting_step dictates how many settings are displayed at once
                # on the settings screen. We have chosen 1 for the small 2.8 inch screen
                # but users with a larger screen could make it larger
                self.table_setting = [self.table_setting[x:x + self.setting_step]
                                      for x in range(0, len(self.table_setting) + 1, self.setting_step)]
                self.table_setting = list(filter(None, self.table_setting))
                self.setting_page = min(len(self.table_setting) - 1, self.setting_page)
                self.setting_page = max(0, self.setting_page)
            self.changed_menu = False

        if (time.time() - self.start) > variables.back_to_default:
            self.menu = self.default
        if callable(self.menu):
            self.menu()
        return

    def default(self):
        # print("Entered default")
        if (time.time() - self.start) < (variables.default_te + variables.back_to_default) or variables.default_te == 0:
            default_dict = self.list_sensor_dict[self.default_property]
            box = Box(self.surface, (0, 0), (self.width, self.height),
                      variables.dark_grey, variables.light_grey, 0)
            box.show()
            clock = Clock(self.surface, self.time_format, self.date_format, self.font_name, 20)
            clock.show_time((box.pos[0] + box.size[0] * 0.05, box.pos[1] + clock.time_text.font_h * 0.5), variables.black)
            clock_text_dim = (clock.date_text.font_w, clock.date_text.font_h)
            clock.show_date((box.pos[0] + box.size[0] * 0.95 - clock_text_dim[0], box.pos[1] + clock_text_dim[1] * 0.5),
                            variables.black)
            temp_unit = default_dict["unit"]
            if self.default_property == "Temperature":
                current_temp = round(self.temp_given_unit(default_dict["current"]), 1)
            else:
                current_temp = round(default_dict["current"], 1)

            font_size = 1
            temp_text = Text(self.surface, (0, clock_text_dim[1] * 2), [str(current_temp) + " " + temp_unit],
                             self.font_name, font_size, variables.black)
            in_width = temp_text.font_w < box.size[0] * 0.9
            in_height = temp_text.font_h < (box.size[1] - clock_text_dim[1] * 2) / 2
            while in_height and in_width:
                font_size += 1
                temp_text = Text(self.surface, (0, clock_text_dim[1] * 2), [str(current_temp) + " " + temp_unit],
                                 self.font_name, font_size, variables.black)
                in_width = temp_text.font_w < box.size[0] * 0.9
                in_height = temp_text.font_h * len(temp_text.text) < (box.size[1] - clock_text_dim[1] * 2) * 0.5
            temp_text.pos = ((box.size[0] - temp_text.font_w) / 2, clock_text_dim[1] * 2)

            font_size = round(font_size / 2)
            if default_dict["active"] and self.mode == "auto":
                temp_text.pos = (temp_text.pos[0], (clock_text_dim[1] * 2 + box.size[1] / 2 - temp_text.font_h) / 2)
                if self.default_property == "Temperature":
                    target_temp = round(self.temp_given_unit(default_dict["target"] + default_dict["setting_delta"]) * 2) / 2
                else:
                    target_temp = round(default_dict["target"] + default_dict["setting_delta"] * 2) / 2
                target_string = "Target : " + str(target_temp) + " " + temp_unit
                target_text = Text(self.surface, (0, 0), [target_string], self.font_name, font_size, variables.black)
                target_text.pos = ((box.size[0] - target_text.font_w) / 2, box.size[1] / 2)
                target_text.show()
                if default_dict["target_time"] != "...":
                    target_time = Text(self.surface, (0, 0), [" " + default_dict["target_time"]],
                                       self.font_name, font_size, variables.black)
                    time_image = Image(self.surface, os.path.join(sys.path[0], "files", "icons", "watch.png"), (0, 0),
                                       (target_time.font_h, target_time.font_h))
                    time_image.pos = ((box.size[0] - (time_image.size[0] + target_time.font_w)) / 2,
                                      target_text.pos[1] + target_text.font_h * 1.1)
                    target_time.pos = (time_image.pos[0] + time_image.size[0], target_text.pos[1] + target_text.font_h)
                    time_image.show()
                    target_time.show()
            temp_text.show()
        else:
            back_box = Box(self.surface, (0, 0), (self.width, self.height),
                           variables.white, variables.white)
            back_box.show()
            logo_border_w = (136 / (max(1597 / self.width, 1147 / self.height)))
            te_logo = Image(self.surface, os.path.join(sys.path[0], "files", "icons", "te_logo.png"),
                            (0, 0), (self.width - 2 * logo_border_w, "x"))
            te_logo.pos = (int((self.width - te_logo.size[0]) / 2), int((self.height - te_logo.size[1]) / 2))
            te_logo.show()

        if self.coord is not None and self.py_touch:
            self.go_to(self.home)
            self.start = time.time()
        
        if variables.ambimate_visible == False:
            ambimate_text = Text(self.surface, (280, 180),["!"],
                            self.font_name_bold_italic, 60, variables.red)
            ambimate_text.show()
        return 0

    def home(self):
        # print("Entered home")
        back_box = Box(self.surface, (0, 0), (self.width, self.height), variables.dark_grey, variables.light_grey, 0)
        back_box.show()
        #checking if ambimate module is visible
        if variables.ambimate_visible == False:
            ambimate_text = Text(self.surface, (300, 180),["!"],
                            self.font_name_bold_italic, 50, variables.red)
            ambimate_text.show()
        # initialise empty active objects list
        home_active_objects = []
        if not(self.home_property in self.list_sensor_dict_displayed):
            self.home_property = self.list_sensor_displayed[0]
        home_dict = self.list_sensor_dict_displayed[self.home_property]

        # Define top left tabs, show them
        font_list = [self.font_name, self.font_name_bold]
        colour_list = [variables.light_blue0, variables.light_blue1]
        tab_texts = ["Home", "Options", "Settings", "Graphs"]
        tab_actions = [functools.partial(self.go_to, a) for a in [self.home, self.options, self.settings, self.graphs]]
        tab_w = int(self.height / 4)  # 60 by default
        for i in range(len(tab_texts)):
            tab_box = Box(self.surface, (tab_w * i, 0), (tab_w, tab_w / 2), colour_list[0], colour_list[1],
                          line_colour=variables.light_blue2)
            tab_text = Text(self.surface, (0, 0), [tab_texts[i]], font_list[0 + (i == 0)], 15, variables.white)
            top_tab = Tab(tab_box, tab_text, tab_actions[i])
            top_tab.show()
            home_active_objects.append(top_tab)

        # Fetch temperature
        temp_unit = home_dict["unit"]
        if home_dict["name"] == "Temperature":
            temp_current = round(self.temp_given_unit(home_dict["current"]), 1)
            temp_target = round(self.temp_given_unit(home_dict["target"] * 2)) / 2
        else:
            temp_current = round(home_dict["current"], 1)
            temp_target = round(home_dict["target"] * 2) / 2

        # Create temperature box, display it
        temp_box = Box(self.surface, (top_tab.box.size[1], self.height / 6),
                       (self.width * (13 / 16), self.height * (5 / 8)),
                       variables.light_blue3,
                       variables.opposite_te, line_colour=variables.white)
        temp_box.show()

        # Create and display box title
        temp_box_title = Text(self.surface, (temp_box.pos[0] + 5, temp_box.pos[1] + 5),
                              [self.home_property.capitalize()],
                              self.font_name, 25, variables.blue)
        temp_box_title.show()
        # Create and display box info
        temp_current_str = "Current : " + str(temp_current) + " " + temp_unit
        if home_dict["active"] and self.mode == "auto":

            temp_box_info = Text(self.surface, (temp_box_title.pos[0], temp_box.pos[1] + temp_box_title.font_h),
                                 [temp_current_str], self.font_name, 15, variables.black)
            temp_box_info.show()
            # Create and display time to target estimate, as well as the average derivative up and down
            if home_dict["name"] == "Temperature":
                delta = round(self.temp_diff_given_unit(home_dict["setting_delta"]) * 2) / 2
                slope_up = abs(round(self.temp_diff_given_unit(home_dict["derivative_up"]) * 3600))
                slope_down = abs(round(self.temp_diff_given_unit(home_dict["derivative_down"]) * 3600))
            else:
                delta = round(home_dict["setting_delta"] * 2) / 2
                slope_up = abs(round(home_dict["derivative_up"] * 3600))
                slope_down = abs(round(home_dict["derivative_down"] * 3600))
            if slope_up != 0:
                slope_up_str = "+ " + str(slope_up) + " " + home_dict["unit"] + "/h"
            else:
                slope_up_str = " "
            if slope_down != 0:
                slope_down_str = "- " + str(slope_down) + " " + home_dict["unit"] + "/h"
            else:
                slope_down_str = " "
            if home_dict["target_time"] != "...":
                target_str = "Time to target : " + home_dict["target_time"]
            else:
                target_str = " "
            temp_box_target_info = Text(self.surface, (0, 0),
                                        ["Delta: " + "{0:+}".format(delta) + " " + home_dict["unit"],
                                         slope_up_str,
                                         slope_down_str,
                                         target_str],
                                        self.font_name, temp_box_info.font_size, variables.black)
            temp_box_target_info.pos = (temp_box_title.pos[0], temp_box.pos[1] + temp_box.size[1] - temp_box_target_info.font_h * len(temp_box_target_info.text))
            temp_box_target_info.show()
            # Create temperature text
            temp_set_text = Text(self.surface, (0, 0), [str(temp_target) + " " + temp_unit],
                                 self.font_name, 30, variables.black)
            if temp_set_text.font_w >= temp_box.size[0] / 2:
                temp_set_text = Text(self.surface, (0, 0), [str(temp_target), temp_unit],
                                     self.font_name, 15, variables.black)
            temp_set_text.pos = (temp_box.pos[0] + temp_box.size[0] * 0.75 - temp_set_text.font_w / 2,
                                 temp_box.pos[1] + temp_box.size[1] / 2 - temp_set_text.font_h * len(temp_set_text.text) / 2)
            temp_set_text.show()
            # Create up triangle and down triangle
            triangle_size = min(temp_set_text.font_w, temp_box.size[1] / 2 - temp_set_text.font_h) * 0.70
            triangle_x = temp_box.pos[0] + temp_box.size[0] * 0.75 - triangle_size / 2

            up_triangle = Arrow(self.surface, variables.black, "North",
                                (triangle_x, temp_set_text.pos[1] - triangle_size - temp_set_text.font_h * 0.25),
                                (triangle_size, triangle_size), self.increment_target)
            home_active_objects.append(up_triangle)
            down_triangle = Arrow(self.surface, variables.white, "South",
                                  (triangle_x, temp_set_text.pos[1] + temp_set_text.font_h * len(temp_set_text.text) + temp_set_text.font_h * 0.25),
                                  (triangle_size, triangle_size), self.reduce_target, variables.white)
            home_active_objects.append(down_triangle)

            up_triangle.show()
            down_triangle.show()
        else:
            temp_box_info = Text(self.surface, (temp_box_title.pos[0], temp_box.pos[1] + temp_box_title.font_h * 1.5),
                                 [temp_current_str], self.font_name, 15, variables.black)
            temp_box_info.show()

        # Create and display arrows to toggle between properties, if there is more than 1 property
        if len(self.list_sensor_displayed) > 1:
            side_size = (self.width - temp_box.size[0]) / 2
            arrows_size = 0.6 * side_size
            i = self.list_sensor_displayed.index(home_dict["name"])
            next_triangle = Arrow(self.surface, variables.white, "East",
                                  (temp_box.pos[0] + temp_box.size[0] + 0.2 * side_size,
                                   temp_box.pos[1] + 0.25 * temp_box.size[1]),
                                  (arrows_size, ) * 2,
                                  functools.partial(self.change_attribute, "home_property",
                                                    self.list_sensor_displayed[(i + 1) % len(self.list_sensor_displayed)])
                                  , variables.white)

            home_active_objects.append(next_triangle)
            prev_triangle = Arrow(self.surface, variables.black, "West",
                                  (temp_box.pos[0] - arrows_size - 0.2 * side_size,
                                   temp_box.pos[1] + 0.25 * temp_box.size[1]),
                                  (arrows_size, ) * 2,
                                  functools.partial(self.change_attribute, "home_property",
                                                    self.list_sensor_displayed[(i - 1) % len(self.list_sensor_displayed)]))
            home_active_objects.append(prev_triangle)
            next_triangle.show()
            prev_triangle.show()

        # Define bottom tabs, show them
        # Fetch boolean values and convert to strings
        tab_texts = [act_name + " " + self.convert_bool(self.actuator_dict[act_name][1])
                     for act_name in self.actuator_dict.keys()]
        if self.mode == "auto":
            tab_text = [Text(self.surface, (0, 0), [tab_text], self.font_name, 15, variables.dark_grey)
                        for tab_text in tab_texts]
            tab_actions = ["toggle"] * len(tab_texts)
        elif self.mode == "manual":
            tab_text = [Text(self.surface, (0, 0), [tab_text], self.font_name, 15, variables.black)
                        for tab_text in tab_texts]
            tab_actions = [functools.partial(self.toggle_actuator, act_name)
                           for act_name in self.actuator_dict.keys()]
        tab_w = 1.2 * max([element.font_w for element in tab_text])
        tab_h = 1.5 * max([element.font_h for element in tab_text])
        for i in range(len(tab_texts)):
            tab_box = Box(self.surface, (0, 0),
                          (tab_w, tab_h), variables.light_grey, variables.light_grey, 0.75, None)
            side_margin = int((self.width - len(tab_texts) * tab_w) / (len(tab_texts) + 1))
            bottom_margin = int((self.height - temp_box.pos[1] - temp_box.size[1] - tab_box.size[1]) / 2)
            tab_box.pos = (side_margin * (i + 1) + tab_w * i, self.height - bottom_margin - tab_h)
            bottom_tab = Tab(tab_box, tab_text[i], tab_actions[i])
            home_active_objects.append(bottom_tab)
            bottom_tab.show()

        if self.coord is not None and self.py_touch:

            for active_object in home_active_objects:
                active_object.act(self.coord)
            self.start = time.time()
        return 1

    def options(self):
        # print("Entered options")
        back_box = Box(self.surface, (0, 0), (self.width, self.height), variables.dark_grey, variables.light_grey, 0)
        back_box.show()

        # initialise empty active objects list
        options_active_objects = []

        # Define top left tabs, show them
        font_list = [self.font_name, self.font_name_bold]
        colour_list = [variables.light_blue0, variables.light_blue1]
        tab_texts = ["Home", "Options", "Settings", "Graphs"]
        tab_actions = [functools.partial(self.go_to, a) for a in [self.home, self.options, self.settings, self.graphs]]
        tab_w = int(self.height / 4)  # 60 by default
        for i in range(len(tab_texts)):
            tab_box = Box(self.surface, (tab_w * i, 0), (tab_w, tab_w / 2), colour_list[0], colour_list[1],
                          line_colour=variables.light_blue2)
            tab_text = Text(self.surface, (0, 0), [tab_texts[i]], font_list[0 + (i == 1)], 15, variables.white)
            top_tab = Tab(tab_box, tab_text, tab_actions[i])
            top_tab.show()
            options_active_objects.append(top_tab)

        # Define options box
        not_tab = (self.height - tab_box.size[1])
        options_box = Box(self.surface, (0, 0), (int(0.8 * self.width), int(not_tab * 0.8)),
                          variables.light_blue3, variables.opposite_te)
        # centre and display
        options_box.pos = (int((self.width - options_box.size[0]) / 2),
                           int(tab_box.size[1] + (not_tab - options_box.size[1]) / 2))
        options_box.show()

        if self.options_screen == 0:
            # Create and display unit title
            options_box_unit_title = Text(self.surface, (options_box.pos[0] + 5, options_box.pos[1] + 5),
                                          ["Unit"], self.font_name, 25, variables.blue)
            options_box_unit_title.show()

            # Text before Radio
            temp_radio_text = Text(self.surface, (0, 0), ["Display temperature in"], self.font_name, 15, variables.black)
            temp_radio_text.pos = (options_box_unit_title.pos[0],
                                   options_box_unit_title.pos[1] + options_box_unit_title.font_h)
            temp_radio_text.show()

            # Initialise and show radio for temperature units
            text_c = Text(self.surface, (0, 0), ["°C"], self.font_name, 15, variables.black)
            text_f = Text(self.surface, (0, 0), ["°F"], self.font_name, 15, variables.black)
            list_function = [functools.partial(self.change_attribute, "list_sensor_dict", unit, "Temperature", "unit")
                             for unit in ["°C", "°F"]]
            temp_unit_radio = Radio(self.surface, (int(temp_radio_text.pos[0] + temp_radio_text.font_w * 1.05),
                                    int(temp_radio_text.pos[1] - 1 * temp_radio_text.font_h)),
                                    [text_c, text_f], list_function, self.list_sensor_dict["Temperature"]["unit"])
            options_active_objects.append(temp_unit_radio)
            temp_unit_radio.show()

            # Create and display mode title
            options_box_mode_title = Text(self.surface, (options_box.pos[0] + 5,
                                          options_box.pos[1] + options_box.size[1] / 2),
                                          ["Mode"], self.font_name, 25, variables.blue)
            options_box_mode_title.show()

            # Text before Radio
            mode_text = Text(self.surface, (0, 0), ["Set mode to manual", "to control actuators"],
                             self.font_name, 15, variables.black)
            mode_text.pos = (options_box_mode_title.pos[0], options_box_mode_title.pos[1] + options_box_mode_title.font_h)
            mode_text.show()

            # Initialise and show radio for mode
            text_auto = Text(self.surface, (0, 0), ["Auto"], self.font_name, 15, variables.black)
            text_manual = Text(self.surface, (0, 0), ["Manual"], self.font_name, 15, variables.black)
            list_function = [functools.partial(self.change_attribute, "mode", a) for a in ["auto", "manual"]]
            mode_radio = Radio(self.surface, (int(mode_text.pos[0] + mode_text.font_w * 1.05),
                               mode_text.pos[1] - mode_text.font_h),
                               [text_auto, text_manual], list_function, self.mode.capitalize())
            options_active_objects.append(mode_radio)
            mode_radio.show()
        elif self.options_screen == 1:

            # Create and display checklist for which screen to show in home menu
            list_property_checks = [CheckBox(self.surface,
                                             (0, 0),
                                             Text(self.surface, (0, 0), [name.capitalize()], self.font_name, 13, variables.black),
                                             "Display associated property in home",
                                             name in self.list_sensor_dict_displayed.keys()) for name in self.list_sensor_names]
            display_property_checklist = CheckList(options_box, list_property_checks, self.options_screen1_displaying)
            options_active_objects.append(display_property_checklist)
            display_property_checklist.show()

            # Create and display explenatory text
            explain_text = Text(self.surface, (0, 0),
                                ["Select which", "properties",
                                 "should be", "displayed on",
                                 "the home screen"],
                                self.font_name_italic,
                                13, variables.black)
            explain_text.pos = (options_box.pos[0] + options_box.size[0] - explain_text.font_w * 1.05,
                                (options_box.pos[0] + options_box.size[1] - explain_text.font_h) / 2)
            explain_text.show()

        # Create and display arrows to toggle between option screens
        side_size = (self.width - options_box.size[0]) / 2
        arrows_size = 0.6 * side_size
        i = self.options_screen
        next_triangle = Arrow(self.surface, variables.white, "East",
                              (options_box.pos[0] + options_box.size[0] + 0.2 * side_size,
                               options_box.pos[1] + 0.25 * options_box.size[1]),
                              (arrows_size, ) * 2,
                              functools.partial(self.change_attribute, "options_screen",
                                                (i + 1) % 2)
                              , variables.white)
        # Note that the % 2 implies that we assume there would only ever be 2 options screen

        options_active_objects.append(next_triangle)
        prev_triangle = Arrow(self.surface, variables.black, "West",
                              (options_box.pos[0] - arrows_size - 0.2 * side_size,
                               options_box.pos[1] + 0.25 * options_box.size[1]),
                              (arrows_size, ) * 2,
                              functools.partial(self.change_attribute, "options_screen",
                                                (i - 1) % 2))
        options_active_objects.append(prev_triangle)
        next_triangle.show()
        prev_triangle.show()

        if self.coord is not None and self.py_touch:
            for active_object in options_active_objects:
                active_object.act(self.coord)
            self.start = time.time()

        try:
            list_property_checks
        except NameError:
            pass
        else:
            self.list_sensor_displayed = [self.list_sensor_names[i]
                                          for i in range(len(self.list_sensor_names))
                                          if list_property_checks[i].value]
            if len(self.list_sensor_displayed) == 0:
                self.list_sensor_displayed = [self.list_sensor_names[0]]
            self.list_sensor_id_displayed = [self.db.get_sensor_id(name) for name in self.list_sensor_displayed]
            self.list_sensor_dict_displayed = {k: self.list_sensor_dict[k] for k in self.list_sensor_displayed}
            self.options_screen1_displaying = display_property_checklist.displaying

    def settings(self):
        # print("Entered settings")
        for i in range(len(self.list_sensor_id_displayed)):
            self.change_attribute("list_sensor_dict",
                                  self.db.get_setting_delta(self.list_sensor_id_displayed[i]),
                                  self.db.get_sensor_name(self.list_sensor_id_displayed[i]),
                                  "setting_delta")
        back_box = Box(self.surface, (0, 0), (self.width, self.height), variables.dark_grey, variables.light_grey, 0)
        back_box.show()

        # initialise empty active objects list
        settings_active_objects = []

        # Define top left tabs, show them
        font_list = [self.font_name, self.font_name_bold]
        colour_list = [variables.light_blue0, variables.light_blue1]
        tab_texts = ["Home", "Options", "Settings", "Graphs"]
        tab_actions = [functools.partial(self.go_to, a) for a in [self.home, self.options, self.settings, self.graphs]]
        tab_w = int(self.height / 4)  # 60 by default
        for i in range(len(tab_texts)):
            tab_box = Box(self.surface, (tab_w * i, 0), (tab_w, tab_w / 2), colour_list[0], colour_list[1],
                          line_colour=variables.light_blue2)
            tab_text = Text(self.surface, (0, 0), [tab_texts[i]], font_list[0 + (i == 2)], 15, variables.white)
            top_tab = Tab(tab_box, tab_text, tab_actions[i])
            top_tab.show()
            settings_active_objects.append(top_tab)

        if len(self.table_setting) == 0:
            no_setting_text = Text(self.surface, (int(0.05 * self.width), top_tab.box.size[1] * 2),
                                   ["There are no settings to display"], self.font_name, 13, variables.black)
            no_setting_text.show()
        else:
            prev_triangle = Arrow(self.surface, variables.black, "North", (0, 0),
                                  (0.5 * top_tab.box.size[1],) * 2, functools.partial(self.change_setting_page, -1))
            prev_triangle.pos = ((self.width - prev_triangle.size[0]) / 2, int(1.25 * top_tab.box.size[1]))
            next_triangle = Arrow(self.surface, variables.white, "South", (0, 0),
                                  (0.5 * top_tab.box.size[1],) * 2, functools.partial(self.change_setting_page, +1),
                                  line_colour=variables.white)
            next_triangle.pos = ((self.width - next_triangle.size[0]) / 2,
                                 int(self.height - 0.25 * top_tab.box.size[1] - next_triangle.size[1]))
            i = 0
            setting_h = 0
            settings_active_objects.append(prev_triangle)
            settings_active_objects.append(next_triangle)
            prev_triangle.show()
            next_triangle.show()
            page_info_text = Text(self.surface, (0, 0),
                                  ["Page " + str(self.setting_page + 1) + "/" + str(len(self.table_setting))],
                                  self.font_name_italic, 13, variables.black)
            page_info_text.pos = (int(self.width * 0.05), prev_triangle.pos[1])
            page_info_text.show()
            for setting_tuple in self.table_setting[self.setting_page]:
                if setting_tuple[1] == self.list_sensor_dict["Temperature"]["sensor_id"]:
                    signed_value = "{0:+}".format(round(self.temp_diff_given_unit(setting_tuple[2]) * 2) / 2)
                else:
                    signed_value = "{0:+}".format(round((setting_tuple[2]) * 2) / 2)
                for value in self.list_sensor_dict.values():
                    if value["sensor_id"] == setting_tuple[1]:
                        unit = value["unit"]
                        break
                    else:
                        unit = "X"
                setting_value = " " + signed_value + " " + unit
                setting_time = " " + str(setting_tuple[3]) + " - " + str(setting_tuple[4]) + " h"
                list_days = list(calendar.day_abbr)
                # Slicing used since first element of list is empty string
                list_months = list(calendar.month_abbr)[1:]
                # Reverse lists since LSB is Monday and January
                list_days.reverse()
                list_months.reverse()
                setting_days = []
                setting_months = []
                bin_days = format(setting_tuple[5], "07b")
                bin_months = format(setting_tuple[6], "012b")
                for day_index in range(len(list_days)):
                    if bin_days[day_index] == "1":
                        setting_days.append(list_days[day_index])
                for month_index in range(len(list_months)):
                    if bin_months[month_index] == "1":
                        # - sign in index accounts for the fact the list is reversed
                        setting_months.append(list_months[month_index])
                # Again reverse list of days and bin_months
                setting_days.reverse()
                setting_months.reverse()
                if len(setting_days) == 7:
                    setting_days_string = " Every day"
                else:
                    setting_days_string = " " + ", ".join(setting_days)
                if len(setting_months) == 12:
                    setting_months_string = " Every month"
                else:
                    setting_months_string = " " + ", ".join(setting_months)
                setting_text = [setting_value, setting_time, setting_days_string, setting_months_string]
                setting_text_object = Text(self.surface, (0, 0), setting_text, self.font_name, 12, variables.black)
                setting_box = Box(self.surface, (0, 0), (int(0.9 * self.width),
                                  setting_text_object.font_h * (len(setting_text_object.text) + 1)),
                                  variables.light_blue3, variables.opposite_te)
                if self.setting_step == 2:
                    setting_box.pos = (int(0.05 * self.width),
                                       (self.height + top_tab.box.size[1] - 2 * setting_box.size[1] - setting_text_object.font_h) / 2 + setting_h * i)
                elif self.setting_step == 1:
                    setting_box.pos = (int(0.05 * self.width), int((next_triangle.pos[1] - prev_triangle.pos[1]) / 2))
                    # print("setting_box.pos, setting_box.size", setting_box.pos, setting_box.size,)
                setting_text_object.pos = (setting_box.pos[0],
                                           setting_box.pos[1] + 0.5 * setting_text_object.font_h)
                delete_box = Box(self.surface, (int(setting_box.pos[0] + setting_box.size[0] * 0.6),
                                 int(setting_box.pos[1] + setting_box.size[1] * 0.1)),
                                 (int(setting_box.size[0] * 0.3), setting_text_object.font_h * 2),
                                 variables.red, variables.red, rounding=1, line_colour=variables.dark_grey)
                delete_text = Text(self.surface, (0, 0), ["DELETE"], self.font_name,
                                   setting_text_object.font_size, variables.white)
                delete_tab = Tab(delete_box, delete_text, functools.partial(self.remove_setting, setting_tuple[0]))
                settings_active_objects.append(delete_tab)
                setting_box.show()
                setting_text_object.show()
                delete_tab.show()
                i += 1
                setting_h = setting_text_object.font_h * (len(setting_text_object.text) + 2)

        add_box = Box(self.surface, (0, 0), top_tab.box.size, variables.light_green,
                      variables.light_green, rounding=1, line_colour=variables.light_grey)
        add_box.pos = (self.width * 0.75, (top_tab.box.size[1] * 4 - add_box.size[1]) / 2)
        add_text = Text(self.surface, (0, 0), ["NEW"], self.font_name, 13, variables.white)
        add_tab = Tab(add_box, add_text, functools.partial(self.go_to, self.new_setting))
        settings_active_objects.append(add_tab)
        add_tab.show()

        if self.coord is not None and self.py_touch:

            for active_object in settings_active_objects:
                active_object.act(self.coord)
            self.start = time.time()
            self.coord = None

        return 2

    def new_setting(self):
        # print("Entered new settings")
        # print(self.new_setting_dict)
        back_box = Box(self.surface, (0, 0), (self.width, self.height), variables.dark_grey, variables.light_grey, 0)
        back_box.show()
        # initialise empty active objects list
        new_setting_active_objects = []

        # Define top left tabs, show them
        font_list = [self.font_name, self.font_name_bold]
        colour_list = [variables.light_blue0, variables.light_blue1]
        tab_texts = ["Home", "Options", "Settings", "Graphs"]
        tab_actions = [functools.partial(self.go_to, a) for a in [self.home, self.options, self.settings, self.graphs]]
        tab_w = int(self.height / 4)  # 60 by default
        for i in range(len(tab_texts)):
            tab_box = Box(self.surface, (tab_w * i, 0), (tab_w, tab_w / 2), colour_list[0], colour_list[1],
                          line_colour=variables.light_blue2)
            tab_text = Text(self.surface, (0, 0), [tab_texts[i]], font_list[0 + (i == 2)], 15, variables.white)
            top_tab = Tab(tab_box, tab_text, tab_actions[i])
            top_tab.show()
            new_setting_active_objects.append(top_tab)

        new_setting_box = Box(self.surface, (0, 0), (int(0.8 * self.width), int(0.8 * self.height)),
                              variables.light_blue3, variables.opposite_te)
        new_setting_box.pos = (int(0.1 * self.width), (top_tab.box.size[1] + self.height - new_setting_box.size[1]) / 2)

        # print(self.new_setting_dict)
        confirm = self.new_setting_dict["confirm"]
        list_parameter = []
        list_property = []
        list_months = []
        list_days = []
        list_start = []
        list_end = []
        list_delta = []

        if (not("property" in self.new_setting_dict) or not(confirm)) and not("months" in self.new_setting_dict):
            # print("property")
            self.make_new_setting_dict("confirm", False)
            # First see what property the setting will affect
            parameter = "property"
            prompt_text = Text(self.surface, (0, 0), ["Choose which", parameter, "the setting", "affects"],
                               self.font_name, 13, variables.black)
            prompt_text.pos = (int(new_setting_box.pos[0] + 0.75 * new_setting_box.size[0] - prompt_text.font_w / 2),
                               int(new_setting_box.pos[1] + 0.2 * new_setting_box.size[1]))
            # We only take into account those sensors that have actuators associated to them
            # i.e. that have a non zero target

            list_sensor_name = [self.list_sensor_names[i]
                                for i in range(len(self.list_sensor_names))
                                if self.list_sensor_dict[self.list_sensor_names[i]]["target"] != 0]
            if parameter in self.new_setting_dict:
                list_bool = [self.new_setting_dict[parameter][i][1]
                             for i in range(len(self.new_setting_dict[parameter]))]
            else:
                list_bool = [False] * len(list_sensor_name)
            list_sensor_values = list(map(lambda x, y: (x, y), list_sensor_name, list_bool))
            list_sensor_text = [Text(self.surface, (0, 0), [name], self.font_name, 13, variables.black)
                                for name in list_sensor_name]
            list_sensor_check = [CheckBox(self.surface, (0, 0), list_sensor_text[i], "action", list_sensor_values[i][1])
                                 for i in range(len(list_sensor_name))]
            if "displaying" in self.new_setting_dict and parameter in self.new_setting_dict:
                displaying = self.new_setting_dict["displaying"]
            else:
                displaying = 0
            sensor_checklist = CheckList(new_setting_box, list_sensor_check, displaying)
            new_setting_active_objects.append(sensor_checklist)
            sensor_checklist.show()
            confirm_box = Box(self.surface, (0, 0), (top_tab.box.size), variables.light_green, variables.light_green)
            confirm_box.pos = (int(new_setting_box.pos[0] + 0.75 * new_setting_box.size[0] - confirm_box.size[0] / 2),
                               int(new_setting_box.pos[1] + 0.75 * new_setting_box.size[1] - confirm_box.size[1] / 2))
            confirm_text = Text(self.surface, (0, 0), ["NEXT >>"], self.font_name,
                                list_sensor_check[0].name.font_size, variables.white)
            confirm_tab = Tab(confirm_box, confirm_text, functools.partial(self.make_new_setting_dict, "confirm", True))
            new_setting_active_objects.append(confirm_tab)
            confirm_tab.show()
            prompt_text.show()

        elif (not("months" in self.new_setting_dict) or not(confirm)) and not("days" in self.new_setting_dict):
            # print("months")
            self.make_new_setting_dict("confirm", False)
            parameter = "months"
            prompt_text = Text(self.surface, (0, 0), ["Choose which", parameter, "the setting", "applies"],
                               self.font_name, 13, variables.black)
            prompt_text.pos = (int(new_setting_box.pos[0] + 0.75 * new_setting_box.size[0] - prompt_text.font_w / 2),
                               int(new_setting_box.pos[1] + 0.2 * new_setting_box.size[1]))
            # Slicing since first element of list is empty string
            list_month_name = calendar.month_abbr[1:]
            if parameter in self.new_setting_dict:
                list_bool = [self.new_setting_dict[parameter][i][1]
                             for i in range(len(self.new_setting_dict[parameter]))]
            else:
                list_bool = [False] * len(list_month_name)
            list_month_value = list(map(lambda x, y: (x, y), list_month_name, list_bool))
            list_month_text = [Text(self.surface, (0, 0), [name], self.font_name, 13, variables.black)
                               for name in list_month_name]
            list_month_check = [CheckBox(self.surface, (0, 0), list_month_text[i], "action", list_month_value[i][1])
                                for i in range(len(list_month_name))]
            if "displaying" in self.new_setting_dict and parameter in self.new_setting_dict:
                displaying = self.new_setting_dict["displaying"]
            else:
                displaying = 0
            # print(displaying)
            month_checklist = CheckList(new_setting_box, list_month_check, displaying)
            new_setting_active_objects.append(month_checklist)
            month_checklist.show()
            confirm_box = Box(self.surface, (0, 0), (top_tab.box.size), variables.light_green, variables.light_green)
            confirm_box.pos = (int(new_setting_box.pos[0] + 0.75 * new_setting_box.size[0] - confirm_box.size[0] / 2),
                               int(new_setting_box.pos[1] + 0.75 * new_setting_box.size[1] - confirm_box.size[1] / 2))
            confirm_text = Text(self.surface, (0, 0), ["NEXT >>"],
                                self.font_name, list_month_check[0].name.font_size, variables.white)
            confirm_tab = Tab(confirm_box, confirm_text, functools.partial(self.make_new_setting_dict, "confirm", True))
            new_setting_active_objects.append(confirm_tab)
            prompt_text.show()
            confirm_tab.show()

        elif (not("days" in self.new_setting_dict) or not(confirm)) and not("start" in self.new_setting_dict):
            # print("days")
            self.make_new_setting_dict("confirm", False)
            parameter = "days"
            prompt_text = Text(self.surface, (0, 0), ["Choose which", parameter, "the setting", "applies"],
                               self.font_name, 13, variables.black)
            prompt_text.pos = (int(new_setting_box.pos[0] + 0.75 * new_setting_box.size[0] - prompt_text.font_w / 2),
                               int(new_setting_box.pos[1] + 0.2 * new_setting_box.size[1]))
            list_day_name = calendar.day_abbr
            if parameter in self.new_setting_dict:
                list_bool = [self.new_setting_dict[parameter][i][1]
                             for i in range(len(self.new_setting_dict[parameter]))]
            else:
                list_bool = [False] * len(list_day_name)
            list_day_value = list(map(lambda x, y: (x, y), list_day_name, list_bool))
            list_day_text = [Text(self.surface, (0, 0), [name], self.font_name, 13, variables.black)
                             for name in list_day_name]
            list_day_check = [CheckBox(self.surface, (0, 0), list_day_text[i], "action", list_day_value[i][1])
                              for i in range(len(list_day_name))]
            if "displaying" in self.new_setting_dict and parameter in self.new_setting_dict:
                displaying = self.new_setting_dict["displaying"]
            else:
                displaying = 0
            # print(displaying)
            day_checklist = CheckList(new_setting_box, list_day_check, displaying)
            new_setting_active_objects.append(day_checklist)
            day_checklist.show()
            confirm_box = Box(self.surface, (0, 0), (top_tab.box.size), variables.light_green, variables.light_green)
            confirm_box.pos = (int(new_setting_box.pos[0] + 0.75 * new_setting_box.size[0] - confirm_box.size[0] / 2),
                               int(new_setting_box.pos[1] + 0.75 * new_setting_box.size[1] - confirm_box.size[1] / 2))
            confirm_text = Text(self.surface, (0, 0), ["NEXT >>"], self.font_name,
                                list_day_check[0].name.font_size, variables.white)
            confirm_tab = Tab(confirm_box, confirm_text, functools.partial(self.make_new_setting_dict, "confirm", True))
            new_setting_active_objects.append(confirm_tab)
            prompt_text.show()
            confirm_tab.show()

        elif (not("start" in self.new_setting_dict) or not(confirm)) and not("end" in self.new_setting_dict):
            # print("start")
            self.make_new_setting_dict("confirm", False)
            parameter = "start"
            prompt_text = Text(self.surface, (0, 0), ["Choose the time at", "which the setting", "starts to apply"],
                               self.font_name, 13, variables.black)
            prompt_text.pos = (int(new_setting_box.pos[0] + 0.75 * new_setting_box.size[0] - prompt_text.font_w / 2),
                               int(new_setting_box.pos[1] + 0.2 * new_setting_box.size[1]))
            if "start" in self.new_setting_dict:
                start_time = self.new_setting_dict["start"][0]
            else:
                start_time = 0

            start_time_text = Text(self.surface, (0, 0), [str(start_time) + ":00"], self.font_name, 20, variables.black)
            start_time_text.pos = (int(new_setting_box.pos[0] + new_setting_box.size[0] * 0.25) - start_time_text.font_w / 2,
                                   int(new_setting_box.pos[1] + new_setting_box.size[1] * 0.5) - start_time_text.font_h / 2)

            more_triangle = Arrow(self.surface, variables.black, "North", (0, 0),
                                  (start_time_text.font_h, ) * 2,
                                  functools.partial(self.make_new_setting_dict, "start", [(start_time + 1) % 25]))
            more_triangle.pos = (start_time_text.pos[0] + (start_time_text.font_w - more_triangle.size[0]) / 2,
                                 start_time_text.pos[1] - more_triangle.size[1] * 1.5)
            new_setting_active_objects.append(more_triangle)
            less_triangle = Arrow(self.surface, variables.white, "South", (0, 0),
                                  (start_time_text.font_h, ) * 2,
                                  functools.partial(self.make_new_setting_dict, "start", [(start_time - 1) % 25]),
                                  line_colour=variables.white)
            less_triangle.pos = (start_time_text.pos[0] + (start_time_text.font_w - more_triangle.size[0]) / 2,
                                 start_time_text.pos[1] + more_triangle.size[1] * 1.5)
            new_setting_active_objects.append(less_triangle)
            confirm_box = Box(self.surface, (0, 0), (top_tab.box.size), variables.light_green, variables.light_green)
            confirm_box.pos = (int(new_setting_box.pos[0] + 0.75 * new_setting_box.size[0] - confirm_box.size[0] / 2),
                               int(new_setting_box.pos[1] + 0.75 * new_setting_box.size[1] - confirm_box.size[1] / 2))
            confirm_text = Text(self.surface, (0, 0), ["NEXT >>"], self.font_name, 13, variables.white)
            confirm_tab = Tab(confirm_box, confirm_text, functools.partial(self.make_new_setting_dict, "confirm", True))
            new_setting_active_objects.append(confirm_tab)

            new_setting_box.show()
            start_time_text.show()
            more_triangle.show()
            less_triangle.show()
            prompt_text.show()
            confirm_tab.show()

        elif (not("end" in self.new_setting_dict) or not(confirm)) and not("delta" in self.new_setting_dict):
            # print("end")
            self.make_new_setting_dict("confirm", False)
            parameter = "end"
            prompt_text = Text(self.surface, (0, 0), ["Choose the time", "up to which", "the setting applies"],
                               self.font_name, 13, variables.black)
            prompt_text.pos = (int(new_setting_box.pos[0] + 0.75 * new_setting_box.size[0] - prompt_text.font_w / 2),
                               int(new_setting_box.pos[1] + 0.2 * new_setting_box.size[1]))
            if "end" in self.new_setting_dict:
                end_time = self.new_setting_dict["end"][0]
            else:
                end_time = 24

            end_time_text = Text(self.surface, (0, 0), [str(end_time) + ":00"], self.font_name, 20, variables.black)
            end_time_text.pos = (int(new_setting_box.pos[0] + new_setting_box.size[0] * 0.25) - end_time_text.font_w / 2,
                                 int(new_setting_box.pos[1] + new_setting_box.size[1] * 0.5) - end_time_text.font_h / 2)

            more_triangle = Arrow(self.surface, variables.black, "North", (0, 0), (end_time_text.font_h, ) * 2,
                                  functools.partial(self.make_new_setting_dict, "end", [(end_time + 1) % 25]))
            more_triangle.pos = (end_time_text.pos[0] + (end_time_text.font_w - more_triangle.size[0]) / 2,
                                 end_time_text.pos[1] - more_triangle.size[1] * 1.5)
            new_setting_active_objects.append(more_triangle)
            less_triangle = Arrow(self.surface, variables.white, "South", (0, 0), (end_time_text.font_h, ) * 2,
                                  functools.partial(self.make_new_setting_dict, "end", [(end_time - 1) % 25]),
                                  line_colour=variables.white)
            less_triangle.pos = (end_time_text.pos[0] + (end_time_text.font_w - more_triangle.size[0]) / 2,
                                 end_time_text.pos[1] + more_triangle.size[1] * 1.5)
            new_setting_active_objects.append(less_triangle)
            confirm_box = Box(self.surface, (0, 0), (top_tab.box.size), variables.light_green, variables.light_green)
            confirm_box.pos = (int(new_setting_box.pos[0] + 0.75 * new_setting_box.size[0] - confirm_box.size[0] / 2),
                               int(new_setting_box.pos[1] + 0.75 * new_setting_box.size[1] - confirm_box.size[1] / 2))
            confirm_text = Text(self.surface, (0, 0), ["NEXT >>"], self.font_name, 13, variables.white)
            confirm_tab = Tab(confirm_box, confirm_text, functools.partial(self.make_new_setting_dict, "confirm", True))
            new_setting_active_objects.append(confirm_tab)

            new_setting_box.show()
            end_time_text.show()
            more_triangle.show()
            less_triangle.show()
            prompt_text.show()
            confirm_tab.show()

        elif (not("delta" in self.new_setting_dict) or not(confirm)):
            # print("delta")
            self.make_new_setting_dict("confirm", False)
            parameter = "delta"
            prompt_text = Text(self.surface, (0, 0), ["Choose the difference", "with the target", "for this setting"],
                               self.font_name, 13, variables.black)
            prompt_text.pos = (int(new_setting_box.pos[0] + 0.75 * new_setting_box.size[0] - prompt_text.font_w / 2),
                               int(new_setting_box.pos[1] + 0.2 * new_setting_box.size[1]))
            if "delta" in self.new_setting_dict:
                delta = self.new_setting_dict["delta"][0]
            else:
                delta = 0.0

            property = [self.new_setting_dict["property"][i][0]
                        for i in range(len(self.new_setting_dict["property"]))
                        if self.new_setting_dict["property"][i][1]]
            delta_text = Text(self.surface, (0, 0),
                              [str(round(self.temp_diff_given_unit(delta) * 2) / 2) + " " + self.list_sensor_dict[property[0]]["unit"]],
                              self.font_name, 20, variables.black)
            delta_text.pos = (int(new_setting_box.pos[0] + new_setting_box.size[0] * 0.25) - delta_text.font_w / 2,
                              int(new_setting_box.pos[1] + new_setting_box.size[1] * 0.5) - delta_text.font_h / 2)
            if property[0] == "Temperature":
                if self.list_sensor_dict[property[0]]["unit"] == "°C":
                    inc = 0.5
                else:
                    inc = 5 / 18
            else:
                inc = self.list_sensor_dict[property[0]]["outer"] / 5
            more_triangle = Arrow(self.surface, variables.black, "North", (0, 0), (delta_text.font_h, ) * 2,
                                  functools.partial(self.make_new_setting_dict, "delta", [(delta + inc)]))
            more_triangle.pos = (delta_text.pos[0] + (delta_text.font_w - more_triangle.size[0]) / 2,
                                 delta_text.pos[1] - more_triangle.size[1] * 1.5)
            new_setting_active_objects.append(more_triangle)
            less_triangle = Arrow(self.surface, variables.white, "South", (0, 0), (delta_text.font_h, ) * 2,
                                  functools.partial(self.make_new_setting_dict, "delta", [(delta - inc)]),
                                  line_colour=variables.white)
            less_triangle.pos = (delta_text.pos[0] + (delta_text.font_w - more_triangle.size[0]) / 2,
                                 delta_text.pos[1] + more_triangle.size[1] * 1.5)
            new_setting_active_objects.append(less_triangle)
            confirm_box = Box(self.surface, (0, 0), (top_tab.box.size), variables.light_green, variables.light_green)
            confirm_box.pos = (int(new_setting_box.pos[0] + 0.75 * new_setting_box.size[0] - confirm_box.size[0] / 2),
                               int(new_setting_box.pos[1] + 0.75 * new_setting_box.size[1] - confirm_box.size[1] / 2))
            confirm_text = Text(self.surface, (0, 0), ["NEXT >>"], self.font_name, 13, variables.white)
            confirm_tab = Tab(confirm_box, confirm_text, functools.partial(self.make_new_setting_dict, "confirm", True))
            new_setting_active_objects.append(confirm_tab)

            new_setting_box.show()
            delta_text.show()
            more_triangle.show()
            less_triangle.show()
            prompt_text.show()
            confirm_tab.show()

        if self.coord is not None and self.py_touch:
            for active_object in new_setting_active_objects:
                active_object.act(self.coord)
            self.start = time.time()
        if parameter == "property":
            list_property = [(check.name.text[0], check.value) for check in list_sensor_check]
            self.make_new_setting_dict("displaying", sensor_checklist.displaying)
        elif parameter == "months":
            list_months = [(check.name.text[0], check.value) for check in list_month_check]
            property_values = [self.new_setting_dict["property"][i][1] for i in range(len(self.new_setting_dict["property"]))]
            if property_values == [False] * len(self.new_setting_dict["property"]):
                list_property = [(self.new_setting_dict["property"][i][0],
                                 self.new_setting_dict["property"][i][1] or (i == 0))
                                 for i in range(len(self.new_setting_dict["property"]))]
                self.make_new_setting_dict("property", list_property)
            self.make_new_setting_dict("displaying", month_checklist.displaying)
        elif parameter == "days":
            list_days = [(check.name.text[0], check.value) for check in list_day_check]
            self.make_new_setting_dict("displaying", day_checklist.displaying)
        elif parameter == "start":
            if "start" in self.new_setting_dict:
                list_start = [self.new_setting_dict["start"][0]]
            else:
                list_start = [start_time]
        elif parameter == "end":
            if "end" in self.new_setting_dict:
                list_end = [self.new_setting_dict["end"][0]]
            else:
                list_end = [end_time]
        elif parameter == "delta":
            if "delta" in self.new_setting_dict:
                list_delta = [self.new_setting_dict["delta"][0]]
            else:
                list_delta = [delta]
        list_parameter = [list_property, list_months, list_days, list_start, list_end, list_delta]
        for _list in list_parameter:
            if len(_list) > 0:
                self.make_new_setting_dict(parameter, _list)
        if "delta" in self.new_setting_dict and self.new_setting_dict["confirm"]:
            id_sensor = [self.list_sensor_dict[property_value[0]]["sensor_id"]
                         for property_value in self.new_setting_dict["property"]
                         if property_value[1]][0]
            delta = float(self.new_setting_dict["delta"][0])
            start = int(self.new_setting_dict["start"][0])
            end = int(self.new_setting_dict["end"][0])
            days = 0
            for i in range(len(self.new_setting_dict["days"])):
                if self.new_setting_dict["days"][i][1]:
                    days += 2**(i)
            months = 0
            for i in range(len(self.new_setting_dict["months"])):
                if self.new_setting_dict["months"][i][1]:
                    months += 2**(i)
            self.db.add_setting(id_sensor, delta, start, end, days, months)
            self.new_setting_dict = {"confirm": False}
            self.go_to(self.settings)
        return 3

    def graphs(self):
        back_box = Box(self.surface, (0, 0), (self.width, self.height), variables.white, variables.white, 0)
        back_box.show()

        # initialise empty active objects list
        graphs_active_objects = []

        # Define top left tabs, show them
        font_list = [self.font_name, self.font_name_bold]
        colour_list = [variables.light_blue0, variables.light_blue1]
        tab_texts = ["Home", "Options", "Settings", "Graphs"]
        tab_actions = [functools.partial(self.go_to, a) for a in [self.home, self.options, self.settings, self.graphs]]
        tab_w = int(self.height / 4)  # 60 by default
        for i in range(len(tab_texts)):
            tab_box = Box(self.surface, (tab_w * i, 0), (tab_w, tab_w / 2), colour_list[0], colour_list[1],
                          line_colour=variables.light_blue2)
            tab_text = Text(self.surface, (0, 0), [tab_texts[i]], font_list[0 + (i == 3)], 15, variables.white)
            top_tab = Tab(tab_box, tab_text, tab_actions[i])
            top_tab.show()
            graphs_active_objects.append(top_tab)
        # Drawing axis lines
        x_axis = Line(self.surface,
                      (self.width * 0.15, int(top_tab.box.size[1] + (self.height - top_tab.box.size[1]) * 0.8)),
                      (self.width * 0.95, int(top_tab.box.size[1] + (self.height - top_tab.box.size[1]) * 0.8)),
                      variables.black)
        y_axis = Line(self.surface,
                      (self.width * 0.15, int(top_tab.box.size[1] + (self.height - top_tab.box.size[1]) * 0.8)),
                      (self.width * 0.15, int(top_tab.box.size[1] + (self.height - top_tab.box.size[1]) * 0.1)),
                      variables.black)
        x_axis_length = x_axis.end_pos[0] - x_axis.start_pos[0]
        y_axis_length = y_axis.start_pos[1] - y_axis.end_pos[1]

        x_title = Text(self.surface, (0, 0), ["Time"], self.font_name_bold, 12, variables.black)
        x_title.pos = (x_axis.start_pos[0] + (x_axis_length - x_title.font_w) / 2, self.height - 1 - x_title.font_h)
        y_title = Text(self.surface, (0, 0), [self.home_property + " (" + self.list_sensor_dict[self.home_property]["unit"] + ")"],
                       self.font_name_bold, 12,
                       variables.black)
        y_title.pos = (max(y_axis.start_pos[0] - y_title.font_w / 2, int(0.05 * self.width)),
                       (top_tab.box.size[1] + y_axis.end_pos[1] - y_title.font_h) / 2)

        font_list = [self.font_name, self.font_name_bold]
        colour_list = [variables.light_green, variables.green]
        tab_texts = ["D", "W", "M"]
        tab_actions = [functools.partial(self.change_attribute, "graph", a) for a in tab_texts]
        # Drawing boxes for "D", "W" and "M"
        for i in range(len(tab_texts)):
            tab_box = Box(self.surface, (self.width / 2 + 40 * i, y_title.pos[1]), (30, y_title.font_h),
                          colour_list[0], colour_list[0], line_colour=variables.light_yellow)
            tab_text = Text(self.surface, (0, 0), [tab_texts[i]], font_list[0 + (self.graph == tab_texts[i])],
                            y_title.font_size, variables.white)
            graph_tab = Tab(tab_box, tab_text, tab_actions[i])
            graph_tab.show()
            graphs_active_objects.append(graph_tab)

        # uploading X and Y 

            X = self.X_database
            Y = self.Y_database
        try:
            min_y = min(Y)
            max_y = max(Y)
            # checking if Y have at least two elements
            at_least_two_values = Y[1]
            if self.list_sensor_dict[self.home_property]["active"] and self.mode == "auto":
                out = self.list_sensor_dict[self.home_property]["out"]
                actual_target = self.list_sensor_dict[self.home_property]["target"] + self.list_sensor_dict[self.home_property]["setting_delta"]
                min_y = min(min_y, actual_target - out)
                max_y = max(max_y, actual_target + out)
            if max_y == min_y:
                max_y += 1  # Avoids division by 0 error further down
            min_x = min(X)
            min_x = min(min_x, time.mktime(self.start_graph))
            max_x = max(X)
            max_x = max(max_x, time.mktime(time.localtime()))
        except:
            graph_error_text = Text(self.surface, (0, 0), ["Not enough data to display this graph"],
                                    self.font_name, 13, variables.black)
            graph_error_text.pos = ((self.width - graph_error_text.font_w) / 2, (self.height + top_tab.box.size[1]) / 2)
            graph_error_text.show()
            if self.coord is not None and self.py_touch:
                for active_object in graphs_active_objects:
                    active_object.act(self.coord)
                self.start = time.time()
            return 4

        # creating outer_box and target_text
        if self.list_sensor_dict[self.home_property]["active"] and self.mode == "auto":
            actual_target = self.list_sensor_dict[self.home_property]["target"] + self.list_sensor_dict[self.home_property]["setting_delta"]
            y_pos = (y_axis_length * 0.9 / (max_y - min_y)) * (actual_target - min_y)
            target_line = Line(self.surface, (x_axis.start_pos[0], int(y_axis.start_pos[1] - 0.05 * y_axis_length - y_pos)),
                               (x_axis.end_pos[0], int(y_axis.start_pos[1] - 0.05 * y_axis_length - y_pos)),
                               variables.te_orange)
            outer = self.list_sensor_dict[self.home_property]["outer"]
            outer_inf_pos = (y_axis_length * 0.9 / (max_y - min_y)) * (actual_target - outer - min_y)
            outer_sup_pos = (y_axis_length * 0.9 / (max_y - min_y)) * (actual_target + outer - min_y)

            outer_box = Box(self.surface, (x_axis.start_pos[0],
                                           int(y_axis.start_pos[1] - 0.05 * y_axis_length - outer_sup_pos)),
                            (x_axis.end_pos[0] - x_axis.start_pos[0], outer_sup_pos - outer_inf_pos), variables.light_grey,
                            variables.light_grey, line_colour=None, )
            outer_box.show()
            # target_line.show()
            if self.home_property == "Temperature":
                val_str = str(round(self.temp_given_unit(actual_target) * 2) / 2)
            else:
                val_str = str(round(actual_target * 2) / 2)
            target_text = Text(self.surface, (0, 0),
                               ["Target : " + val_str + " " + self.list_sensor_dict[self.home_property]["unit"]],
                               self.font_name_bold_italic, 14, variables.te_orange)
            mid_y = (y_axis.start_pos[1] + y_axis.end_pos[1]) / 2
            if target_line.start_pos[1] > mid_y:
                target_text.pos = (int(0.75 * self.width - 0.5 * target_text.font_w),
                                   int(outer_box.pos[1] - target_text.font_h))
            else:
                target_text.pos = (int(0.75 * self.width - 0.5 * target_text.font_w),
                                   int(outer_box.pos[1] + outer_box.size[1]))

        # Convert extrema of y to right unit, create and show y tics
        if self.home_property == "Temperature":
            min_y_axis = self.temp_given_unit(min_y)
            max_y_axis = self.temp_given_unit(max_y)
        else:
            max_y_axis = max_y
            min_y_axis = min_y
        scale = (max_y_axis - min_y_axis) / (y_axis_length * 0.95)  # in unit/ pixel °C/pixel [or °F/pixel]
        y_start_value = min_y_axis - 0.05 * y_axis_length * scale
        if_decimals = False
        grad = []
        if ((max_y_axis - min_y_axis) <= 1):
            grad_interval = 5
            step = 1
            value_grad = int(round(y_start_value,2)*100)
            if_decimals = True
        elif (1 < (max_y_axis - min_y_axis) <= 2):
            grad_interval = 50
            step = 10
            value_grad = round(y_start_value,1)*100
            if_decimals = True
        elif (2 < (max_y_axis - min_y_axis) <= 10):
            grad_interval = 1
            step = 0.5
            value_grad = round(y_start_value,0)
        elif 10 < (max_y_axis - min_y_axis) and (max_y_axis - min_y_axis) <= 20:
            grad_interval = 2
            step = 0.5
            value_grad = round(y_start_value,0)
        elif 20 < (max_y_axis - min_y_axis) <= 50:
            grad_interval = 5
            step = 1
            value_grad = round(y_start_value,0)
        elif 50 <= (max_y_axis - min_y_axis) <= 100:
            grad_interval = 10
            step = 5
            value_grad = round(y_start_value, -1)
        elif 100 < (max_y_axis - min_y_axis) < 500:
            grad_interval = 50
            step = 10
            value_grad = round(y_start_value, -1)
        elif 500 < (max_y_axis - min_y_axis) < 1000:
            grad_interval = 100
            step = 100
            value_grad = round(y_start_value, -2)
        elif 1000 < (max_y_axis - min_y_axis):
            grad_interval = 500
            step = 100
            value_grad = round(y_start_value, -2)
        while value_grad < max_y_axis or (value_grad < max_y_axis*100 and if_decimals == True):
            if value_grad % grad_interval == 0 and if_decimals == False:
                grad.append(round(value_grad, min(0, -1 * (grad_interval > 50))))
            elif (((value_grad) % (grad_interval)) == 0 and if_decimals == True):
                value_grad = round(value_grad,0)
                grad.append(value_grad/100)
            value_grad += step
        tic_size = 3
        for value in grad:
            if(if_decimals):
                tic_text = Text(self.surface, (0, 0), ["{:0.2f}".format(value)], self.font_name, 10, variables.black)
            else:
                tic_text = Text(self.surface, (0, 0), ["{:.0f}".format(value)], self.font_name, 10, variables.black)
            y_pos = y_axis.start_pos[1] - y_axis_length * 0.05 * 0 - (value - y_start_value) / scale
            tic = Line(self.surface, (y_axis.start_pos[0] - tic_size / 2, int(y_pos)),
                       (y_axis.start_pos[0] + tic_size / 2, int(y_pos)), variables.black)
            grid_y = Line(self.surface, (y_axis.start_pos[0] - tic_size / 2, int(y_pos)),
                       (y_axis.start_pos[0]+ x_axis_length , int(y_pos)), variables.grid_colour)
            tic_text.pos = (int(tic.start_pos[0] - 1.1 * tic_text.font_w), tic.start_pos[1] - tic_text.font_h / 2)
            if value >= y_start_value:
                if (variables.grid):
                    grid_y.show()
                tic.show()
                tic_text.show()

        # Now we create and show the x tics
        min_x = time.mktime(self.start_graph)
        max_x = time.mktime(time.localtime())
        scale = (max_x - min_x) / (x_axis_length)  # in unit time/pixel
        grad = []
        delta_hour = 3 * (self.graph == "D")
        delta_day = (self.graph == "W")
        delta_week = (self.graph == "M")
        value_grad = time.mktime(self.start_graph)
        while value_grad <= max_x:
            grad.append(value_grad)
            value_grad += 3600 * (delta_hour + 24 * delta_day + 7 * 24 * delta_week)
        for value_time in grad:
            value = time.localtime(value_time)
            if self.graph == "D":
                tic_text = Text(self.surface, (0, 0), [str(value.tm_hour) + "h"], self.font_name, 10, variables.black)
            else:
                if self.date_format == "%d/%m":
                    tic_text = Text(self.surface, (0, 0), [str(value.tm_mday) + "-" + str(value.tm_mon)],
                                    self.font_name, 10, variables.black)
                elif self.date_format == "%m/%d":
                    tic_text = Text(self.surface, (0, 0), [str(value.tm_mon) + "-" + str(value.tm_mday)],
                                    self.font_name, 10, variables.black)
                else:
                    # dd/mm time format will be used if self.date_fornat is not recognised
                    tic_text = Text(self.surface, (0, 0), [str(value.tm_mday) + "/" + str(value.tm_mon)],
                                    self.font_name, 10, variables.black)
            x_pos = x_axis.start_pos[0] + (value_time - min_x) / scale
            tic = Line(self.surface, (int(x_pos), int(x_axis.start_pos[1] - tic_size / 2)),
                       (int(x_pos), int(x_axis.start_pos[1] + tic_size / 2)), variables.black)
            grid_x = Line(self.surface, (int(x_pos), int(x_axis.start_pos[1] - tic_size / 2)),
                        (int(x_pos) , int(x_axis.start_pos[1] - y_axis_length)), variables.grid_colour)
            tic_text.pos = (int(tic.start_pos[0] - tic_text.font_w / 2), int(tic.start_pos[1] + tic_text.font_h * 0.5))
            if (variables.grid):
                grid_x.show()
            tic.show()
            tic_text.show()

        # showing axis and title
        x_axis.show()
        y_axis.show()
        x_title.show()
        y_title.show()
        # creating curve
        start_x = int(x_axis.start_pos[0] + (x_axis_length / (max_x - min_x)) * (X[0] - min_x))
        start_y = int(y_axis.start_pos[1] - 0.05 * y_axis_length - (y_axis_length * 0.95 / (max_y - min_y)) * (Y[0] - min_y))
        start_pos = (start_x, start_y)
        for i in range(len(X) - 1):
            end_x = (x_axis_length / (max_x - min_x)) * (X[i + 1] - min_x)
            end_y = (y_axis_length * 0.95 / (max_y - min_y)) * (Y[i + 1] - min_y)
            end_pos = (int(x_axis.start_pos[0] + end_x), int(y_axis.start_pos[1] - 0.05 * y_axis_length - end_y))
            if (X[i + 1] - X[i]) < 2 * (self.measure_interval):
                curve_section = Line(self.surface, start_pos, end_pos, variables.black)
                curve_section.show()
            start_pos = end_pos
        # showing target text. We're doing it at the end to have it more visible (not covered by curve)
        try:
            target_text.show()
        except:
            print ("No target to show")
        
      
        if self.coord is not None and self.py_touch:
            for active_object in graphs_active_objects:
                active_object.act(self.coord)
            self.start = time.time()

        return 4

    def increment_target(self):
        if self.home_property == "Temperature":
            if self.list_sensor_dict["Temperature"]["unit"] == "°C":
                inc = 0.5
            else:
                inc = 5 / 18

        else:
            # inc is set as 1/10 of the "outer range"
            inc = self.list_sensor_dict[self.home_property]["outer"] / 5

        new_value = self.list_sensor_dict[self.home_property]["target"] + inc
        self.change_attribute("list_sensor_dict",
                              new_value,
                              self.home_property, "target")
        self.db.set_sensor_target(self.list_sensor_dict[self.home_property]["sensor_id"],
                                  new_value)

        return

    def reduce_target(self):
        if self.home_property == "Temperature":
            if self.list_sensor_dict["Temperature"]["unit"] == "°C":
                dec = 0.5
            else:
                dec = 5 / 18
        else:
            # dec is set as 1/10 of the "outer range"
            dec = variables.sensors[self.home_property][1] / 5
        new_value = self.list_sensor_dict[self.home_property]["target"] - dec
        self.change_attribute("list_sensor_dict",
                              new_value,
                              self.home_property, "target")
        self.db.set_sensor_target(self.list_sensor_dict[self.home_property]["sensor_id"],
                                  new_value)

        return

    def go_to(self, menu):
        self.changed_menu = True
        self.menu = menu
        self.new_setting_dict = {"confirm": False}
        return

    def temp_diff_given_unit(self, diff):
        if self.list_sensor_dict["Temperature"]["unit"] == "°C":
            return diff
        else:
            return diff * (9 / 5)

    def temp_given_unit(self, temp):
        if self.list_sensor_dict["Temperature"]["unit"] == "°C":
            return temp
        else:
            return temp * (9 / 5) + 32

    def get_time_to_target(self, date_change):
        try:
            date_change = time.strptime(date_change, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            return "..."
        date_change = time.mktime(date_change)
        if time.mktime(time.localtime()) - date_change >= 0:
            target_time_str = "..."
        else:
            target_time = date_change - time.time()
            if target_time >= 3600:
                target_time_str = "{:01d}".format(round((target_time % (24 * 3600)) / 3600)) + "h " + "{:02d}".format(round((target_time % 3600) / 60))
            elif target_time >= 60:
                target_time_str = "{:01d}".format(round(target_time / 60)) + " min"
            else:
                target_time_str = "< 1 min"
        return str(target_time_str)

    def change_attribute(self, attribute, new_value, key1=None, key2=None):
        if key1 is None:
            self.values[attribute] = new_value
        elif key2 is None:
            self.values[attribute][key1] = new_value
        else:
            self.values[attribute][key1][key2] = new_value
        # print(attribute, self.values[attribute])
        if attribute == "graph" or attribute == "home_property":
            if self.graph == "D":
                self.start_graph = time.localtime(time.time() - 3600 * 24)
                self.max_measure_group = variables.av_day
                self.measure_interval = variables.av_day* 1/variables.FPS_sensor
            elif self.graph == "W":
                self.start_graph = time.localtime(time.time() - 3600 * 24 * 7)
                self.max_measure_group = variables.av_week
                self.measure_interval = variables.av_week* 1/variables.FPS_sensor
            elif self.graph == "M":
                self.start_graph = time.localtime(time.time() - 3600 * 24 * 30)
                self.max_measure_group = variables.av_month
                self.measure_interval = variables.av_month* 1/variables.FPS_sensor
            
            self.measures = self.db.get_row_from_graphs(self.list_sensor_dict[self.home_property]["sensor_id"],
                                              self.max_measure_group)
            # uploading data to display
            try:
                self.X_database =[float(i) for i in self.measures[0][0].split(',')]
                self.Y_database =[float(i) for i in self.measures[0][1].split(',')]
                update = self.db.set_graph(self.list_sensor_dict[self.home_property]["sensor_id"],
                                                    self.max_measure_group,
                                                    datetime.utcfromtimestamp(int(self.X_database[-1])).strftime('%Y-%m-%d %H:%M:%S')) 
                self.X_database = self.X_database + [time.mktime(time.strptime(element[0], "%Y-%m-%d %H:%M:%S")) for element in update]
                self.Y_database = self.Y_database + [element[1] for element in update]
                min_y = min(self.Y_database)
                while (time.mktime(self.start_graph) >= self.X_database[0]):
                    self.X_database.pop(0)
                    self.Y_database.pop(0)
            except:
                self.measures = self.db.set_graph(self.list_sensor_dict[self.home_property]["sensor_id"],
                                                    self.max_measure_group,
                                                    time.strftime("%Y-%m-%d %H:%M:%S", self.start_graph))
                self.X_database = [time.mktime(time.strptime(element[0], "%Y-%m-%d %H:%M:%S")) for element in self.measures]
                self.Y_database = [element[1] for element in self.measures]
            self.db.insert_graph(str(self.X_database).strip('[]'), str(self.Y_database).strip('[]'),self.list_sensor_dict[self.home_property]["sensor_id"], self.max_measure_group)
        
        elif attribute == "mode":
            if new_value == "auto":
                self.set.set("AUTO", True)
            else:
                self.set.set("AUTO", False)
        elif attribute == "list_sensor_dict" and key1 == "Temperature" and key2 == "unit":
            self.set.set("temp_unit", self.list_sensor_dict["Temperature"]["unit"])
        return

    def convert_bool(self, boolean):
        if boolean:
            return("ON")
        else:
            return("OFF")

    def toggle_actuator(self, name):
        self.db.set_actuator_value(self.actuator_dict[name][0], not(self.actuator_dict[name][1]))
        self.actuator_dict[name] = (self.actuator_dict[name][0], not(self.actuator_dict[name][1]))
        # print("Actuator", id, "is now : ", self.db.get_actuator_state(id))
        return

    def remove_setting(self, id):
        # print("Will remove setting with id", id)
        list_id = [setting_tuple[0][0] for setting_tuple in self.table_setting]
        for i in range(len(list_id)):
            if list_id[i] == id:
                # print("ABOUT TO REMOVE setting", i + 1)
                del self.table_setting[i]
                self.db.remove_setting(id)
        self.setting_page = min(len(self.table_setting) - 1, self.setting_page)
        self.setting_page = max(0, self.setting_page)

    def change_setting_page(self, increment):
        self.setting_page += increment
        self.setting_page %= len(self.table_setting)
        return

    def make_new_setting_dict(self, parameter, value):
        self.new_setting_dict[parameter] = value
        return

    def refresh(self):
        start_refresh = time.time()
        if self.is_raspberry:
            # Open the frame buffer binary file in "write" mode
            f = open("/dev/fb1", "wb")
            # Convert 24 bits depth surface to 16 bits depth
            f.write(self.surface.convert(16).get_buffer())
            # Close frame buffer
            f.close()
        else:
            pygame.display.update()
        refresh_time = time.time() - start_refresh
        if refresh_time < (1 / variables.FPS_gui):
            time.sleep((1 / variables.FPS_gui) - refresh_time)
        # if you feel the program is using your device's capacities too much,
        # comment the 3 lines above and uncomment the following line:
        # time.sleep(1 / variables.FPS_gui)

    def get_coord(self):
        if self.coord is not None:
            self.last_coord = self.coord
        if time.time() - self.start < 0.1:
            # print("Last touch too recent", None)
            return None
        else:
            coord = [-1, -1]
            x_touch = coord[0]
            y_touch = coord[1]

            # Used to map touch evt from the screen hardware to the pygame surface pixels.
            # Values were found empirically for 2.8 inch Waveshare touchscreen, you may need to change them.
            orig = (3735, 130)
            end = (3835, 195)

            if self.is_raspberry:
                # Note: time out time is a tradeoff : too long and program waits for too long
                # too short and iteration passes over touch event
                select.select([self.touch], [], [], 0.1)
                try:
                    for evt in self.touch.read():
                        if evt.type == evdev.ecodes.EV_ABS and evt.code != 24:
                            if evt.code == 1 and x_touch == -1:
                                x_touch = evt.value
                            if evt.code == 0 and y_touch == -1:
                                y_touch = evt.value
                            if x_touch != -1 and y_touch != -1:
                                break
                except BlockingIOError:
                    # print("Block error", None)
                    return None

                # uncomment the following if you think the touch might be off.
                # Modify the values of the variables orig and end accoridngly
                # print(x_touch, y_touch)
                if x_touch != -1 and y_touch != -1:
                    x_touch = (float(x_touch - orig[1]) / (orig[0] - orig[1])) * float(self.width)
                    y_touch = (float(end[0] - y_touch) / (end[0] - end[1])) * float(self.height)

            else:

                x_touch = pygame.mouse.get_pos()[0]
                y_touch = pygame.mouse.get_pos()[1]

            if x_touch != -1 and y_touch != -1:
                coord[0] = int(x_touch)
                coord[1] = int(y_touch)
                # print("[x, y] =", coord)
                if self.is_raspberry:
                    if self.last_coord is None or (abs(coord[0] - self.last_coord[0]) > 0.01 * self.width or abs(coord[1] - self.last_coord[1]) > 0.01 * self.height):
                        return coord
                    else:
                        # This line assumes that one will never click twice in a row at the same place
                        # All instances of this occurence are thus considered as the following:
                        # A user is clicking for too long
                        return None
                else:
                    return coord


class Box:
    def __init__(self, surface, pos, size, colour1, colour2, rounding=0, line_colour=variables.black, line_width=1):
        self.surface = surface
        self.pos = (int(pos[0]), int(pos[1]))
        self.size = (int(size[0]), int(size[1]))
        self.colour1 = colour1
        self.colour2 = colour2
        self.rounding = rounding
        self.line_colour = line_colour
        self.line_width = line_width

    def show(self):
        rect = (self.pos[0], self.pos[1], self.size[0], self.size[1])

        if self.colour1 == self.colour2 and self.rounding == 0:
            pygame.draw.rect(self.surface, self.colour1, rect)

        else:

            if self.rounding != 0:
                min_side = min([side_length for side_length in self.size])
                rad = self.rounding * min_side * (1 / 2)

            x0 = rect[0]
            y0 = rect[1]
            length = rect[2]
            y1 = rect[3]
            r0 = self.colour1[0]
            r1 = self.colour2[0]
            g0 = self.colour1[1]
            g1 = self.colour2[1]
            b0 = self.colour1[2]
            b1 = self.colour2[2]

            for x in range(length):
                r = r0 * (1 - (x / length)) + r1 * (x / length)
                g = g0 * (1 - (x / length)) + g1 * (x / length)
                b = b0 * (1 - (x / length)) + b1 * (x / length)

                colour = (r, g, b)

                if self.rounding == 0:
                    small_rect = (x0 + x, y0, 1, y1)
                else:
                    if x < rad:
                        not_drawn_h = round(rad - (math.sqrt(abs(rad ** 2 - (x - rad) ** 2))))
                    elif x > length - rad:
                        not_drawn_h = round(rad - math.sqrt(abs(rad ** 2 - (x + rad - length) ** 2)))
                    else:
                        not_drawn_h = 0
                    y0 = self.pos[1] + not_drawn_h
                    y1 = self.size[1] - 2 * not_drawn_h
                    small_rect = (x0 + x, y0, 1, y1)

                pygame.draw.rect(self.surface, colour, small_rect)
        if self.line_colour is not None:
            if self.rounding == 0:
                pygame.draw.rect(self.surface, self.line_colour, rect, self.line_width)
            else:
                pygame.draw.line(self.surface, self.line_colour, (self.pos[0] + rad, self.pos[1]),
                                 (self.pos[0] + self.size[0] - rad, self.pos[1]), self.line_width)
                pygame.draw.line(self.surface, self.line_colour, (self.pos[0] + rad, self.pos[1] + self.size[1]),
                                 (self.pos[0] + self.size[0] - rad, self.pos[1] + self.size[1]), self.line_width)
                pygame.draw.line(self.surface, self.line_colour, (self.pos[0], self.pos[1] + rad),
                                 (self.pos[0], self.pos[1] + self.size[1] - rad), self.line_width)
                pygame.draw.line(self.surface, self.line_colour, (self.pos[0] + self.size[0], self.pos[1] + rad),
                                 (self.pos[0] + self.size[0], self.pos[1] + self.size[1] - rad), self.line_width)
                pygame.draw.arc(self.surface, self.line_colour,
                                (self.pos[0] + self.size[0] - 2 * rad, self.pos[1], 2 * rad, 2 * rad), 0,
                                math.pi / 2, self.line_width)
                pygame.draw.arc(self.surface, self.line_colour, (self.pos[0], self.pos[1], 2 * rad, 2 * rad),
                                math.pi / 2, math.pi, self.line_width)
                pygame.draw.arc(self.surface, self.line_colour,
                                (self.pos[0], self.pos[1] + self.size[1] - 2 * rad, 2 * rad, 2 * rad), math.pi,
                                math.pi * (3 / 2), self.line_width)
                box_size = (self.pos[0] + self.size[0], self.pos[1] + self.size[1])
                pygame.draw.arc(self.surface, self.line_colour,
                                (box_size[0] - 2 * rad, box_size[1] - 2 * rad, 2 * rad, 2 * rad),
                                math.pi * (3 / 2), math.pi * 2, self.line_width)


class Text:

    """Display lines of text in the same font. Note: self.text is a list of line strings
    The font may be set to bold or italic or both depending on font name"""
    def __init__(self, surface, pos, list_text, font_name, font_size, font_colour):
        self.surface = surface
        self.pos = (int(pos[0]), int(pos[1]))
        self.text = list_text
        self.font_name = font_name
        self.font_size = font_size
        self.font = pygame.font.Font(self.font_name, font_size)
        self.font_w = max([self.font.size(a)[0] for a in self.text])
        self.font_h = max([self.font.size(a)[1] for a in self.text])
        self.font_colour = font_colour

    def text_surface(self):
        surface_list = []
        rect_list = []
        for i in range(len(self.text)):
            text_surface = self.font.render(self.text[i], True, self.font_colour)
            surface_list.append(text_surface)
            corner = (self.pos[0], self.pos[1] + (i * self.font_h))
            rect_list.append(text_surface.get_rect(topleft=corner))
        return surface_list, rect_list

    def show(self):
        surf, surf_rect = self.text_surface()
        for i in range(len(self.text)):
            self.surface.blit(surf[i], surf_rect[i])


class Tab:
    def __init__(self, box_object, text_object, action, sub_text=None):
        self.box = box_object
        self.text = text_object
        self.action = action
        self.sub_text = sub_text

    def show(self):
        while self.text.font_w > 0.9 * self.box.size[0] or self.text.font_h * len(self.text.text) > self.box.size[1]:
            self.text = Text(self.text.surface, self.text.pos,
                             self.text.text, self.text.font_name, self.text.font_size - 1, self.text.font_colour)
        corner_x = self.box.pos[0] + (self.box.size[0] - self.text.font_w) / 2
        corner_y = self.box.pos[1] + (self.box.size[1] - self.text.font_h * len(self.text.text)) / 2
        corner = (corner_x, corner_y)
        self.text.pos = corner
        self.box.show()
        self.text.show()

        if self.sub_text is not None:
            sub_font = int(self.text.font_size)
            sub_text = Text(self.text.surface, (0, 0), self.sub_text, self.text.font_name,
                            sub_font, self.text.font_colour)
            i = 0
            text_total_h = self.text.font_h * len(self.text.text)
            sub_text_total_h = sub_text.font_h * len(self.sub_text)
            min_margin = 5
            max_h = ((self.box.size[1] - text_total_h) * (1 / 2)) * 0.9
            max_w = self.box.size[0] * 0.9
            while (sub_text_total_h > max_h or sub_text.font_w > max_w) and i < sub_font - 1:
                i += 1
                sub_text = Text(self.text.surface, (0, 0), self.sub_text,
                                self.text.font_name, sub_font - i, self.text.font_colour)
                sub_text_total_h = sub_text.font_h * len(self.sub_text)
            box_based_param_h = self.box.pos[1] + (3 / 4) * self.box.size[1]
            text_h = box_based_param_h + text_total_h * (1 / 4) - sub_text_total_h / 2 - min_margin
            sub_text.pos = (self.box.pos[0] + (self.box.size[0] - sub_text.font_w) / 2, text_h)
            sub_text.show()

    def act(self, coord):
        in_width = 0 < coord[0] - self.box.pos[0] < self.box.size[0]
        in_height = 0 < coord[1] - self.box.pos[1] < self.box.size[1]
        if in_width and in_height and callable(self.action):
            self.action()


class Image:
    def __init__(self, surface, path, pos, size):
        self.surface = surface
        self.path = path
        self.pos = (int(pos[0]), int(pos[1]))
        self.image = pygame.image.load(self.path)
        self.og_size = self.image.get_size()
        self.size = size
        if self.size[0] == "x":
            self.size = (int(self.og_size[0] * (self.size[1] / self.og_size[1])), int(self.size[1]))
        elif self.size[1] == "x":
            self.size = (int(self.size[0]), int(self.og_size[1] * (self.size[0] / self.og_size[0])))

    def show(self):
        image = pygame.transform.scale(self.image, self.size)
        self.surface.blit(image, (self.pos, self.size))


class Line:
    def __init__(self, surface, start_pos, end_pos, colour, line_width=1, dash_size=0, density=2):
        """
        Draw a dashed line by adjusting the parameter size (0 for a solid line)
        It gives the size of each dash.
        The density should be an integer greater than 1; 1 in density dash will
        be drawn to the screen"""
        self.surface = surface
        self.start_pos = (int(start_pos[0]), int(start_pos[1]))
        self.end_pos = (int(end_pos[0]), int(end_pos[1]))
        self.colour = colour
        self.width = line_width
        self.dash_size = dash_size
        self.density = density

    def show(self):
        if self.dash_size == 0:
            pygame.draw.line(self.surface, self.colour, self.start_pos, self.end_pos, self.width)
        else:
            length = math.sqrt((self.start_pos[0] - self.end_pos[0])**2 + (self.start_pos[1] - self.end_pos[1])**2)
            n = int(length) // self.dash_size
            # Generate list of point coordinates
            points = []
            for i in range(int(n)):
                x = self.start_pos[0] + (self.end_pos[0] - self.start_pos[0]) * (i / n)
                y = self.start_pos[1] + (self.end_pos[1] - self.start_pos[1]) * (i / n)
                points.append((x, y))
            points.append(self.end_pos)
            for i in range(len(points) - 1):
                if i % self.density == 0:
                    pygame.draw.line(self.surface, self.colour, points[i], points[i + 1], self.width)


class Clock:
    def __init__(self, surface, time_format, date_format, font_name, font_size):
        self.surface = surface
        self.time_format = time_format
        self.date_format = date_format
        self.time = time.strftime(self.time_format, time.localtime())
        self.date = time.strftime(self.date_format, time.localtime())
        self.font_name = font_name
        self.font_size = font_size
        self.time_text = Text(self.surface, (0, 0), [self.time], self.font_name, self.font_size, variables.black)
        self.date_text = Text(self.surface, (0, 0), [self.date], self.font_name, self.font_size, variables.black)

    def show_time(self, pos, font_colour=variables.black):
        self.time = time.strftime(self.time_format, time.localtime())
        self.time_text = Text(self.surface, (int(pos[0]), int(pos[1])),
                              [self.time], self.font_name, self.font_size, font_colour)
        self.time_text.show()

    def show_date(self, pos, font_colour=variables.black):
        self.date = time.strftime(self.date_format, time.localtime())
        self.date_text = Text(self.surface, pos, [self.date], self.font_name, self.font_size, font_colour)
        self.date_text.show()


class Arrow:
    def __init__(self, surface, colour, direction, pos, size, action, line_colour=variables.black, line_width=1):
        self.surface = surface
        self.colour = colour
        self.direction = direction
        self.pos = (int(pos[0]), int(pos[1]))
        self.size = (int(size[0]), int(size[1]))
        self.action = action
        self.line_colour = line_colour
        self.line_width = line_width

    def show(self):
        if self.direction == 'East':
            point1 = self.pos
            point2 = (self.pos[0] + self.size[0], self.pos[1] + self.size[1] / 2)
            point3 = (self.pos[0], self.pos[1] + self.size[1])
        elif self.direction == 'North':
            point1 = (self.pos[0], self.pos[1] + self.size[1])
            point2 = (self.pos[0] + self.size[0] / 2, self.pos[1])
            point3 = (self.pos[0] + self.size[0], self.pos[1] + self.size[1])
        elif self.direction == 'West':
            point1 = (self.pos[0], self.pos[1] + self.size[1] / 2)
            point2 = (self.pos[0] + self.size[0], self.pos[1])
            point3 = (self.pos[0] + self.size[0], self.pos[1] + self.size[1])
        elif self.direction == 'South':
            point1 = self.pos
            point2 = (self.pos[0] + self.size[0], self.pos[1])
            point3 = (self.pos[0] + self.size[0] / 2, self.pos[1] + self.size[1])
        coordinates = [point1, point2, point3]
        pygame.draw.polygon(self.surface, self.colour, coordinates)
        pygame.draw.polygon(self.surface, self.line_colour, coordinates, self.line_width)

    def act(self, coord):
        in_width = 0 < coord[0] - self.pos[0] < self.size[0]
        in_height = 0 < coord[1] - self.pos[1] < self.size[1]
        if in_width and in_height and callable(self.action):
            self.action()


class Radio:
    def __init__(self, surface, pos, list_names, list_actions, initial_setting):
        self.surface = surface
        self.pos = (int(pos[0]), int(pos[1]))
        self.list_names = list_names
        self.list_actions = list_actions
        self.setting = initial_setting
        self.radius = int(max(text_object.font_h / 2 for text_object in self.list_names))
        self.list_centres = [0] * len(self.list_names)
        for i in range(len(self.list_names)):
            name = self.list_names[i]
            circle_centre = (int(self.pos[0] + self.radius), int(self.pos[1] + self.radius + 4 * self.radius * i))
            self.list_centres[i] = circle_centre
            if self.setting == name.text[0] and callable(self.list_actions[i]):
                list_actions[i]()

    def show(self):
        for i in range(len(self.list_names)):
            circle_centre = self.list_centres[i]
            name = self.list_names[i]
            pygame.draw.circle(self.surface, variables.white, circle_centre, self.radius)
            # Note that self.list_names[i].text is a list of strings
            if self.list_names[i].text[0] == self.setting:
                pygame.draw.circle(self.surface, variables.black, circle_centre, int(self.radius * (1 / 3)))
            name_pos = (circle_centre[0] + 2 * self.radius, circle_centre[1] - self.radius)
            name.pos = name_pos
            name.show()

    def act(self, coord):
        for i in range(len(self.list_centres)):
            circle_centre = self.list_centres[i]
            in_width = abs(circle_centre[0] - coord[0]) < self.radius
            in_height = abs(circle_centre[1] - coord[1]) < self.radius
            if in_width and in_height:
                self.setting = self.list_names[i].text[0]
                if callable(self.list_actions[i]):
                    self.list_actions[i]()


class CheckBox:
    # Note that name should be a Text object
    def __init__(self, surface, pos, name, action, initial_value):
        self.surface = surface
        self.pos = (int(pos[0]), int(pos[1]))
        self.name = name
        self.action = action
        self.value = initial_value
        self.box = Box(self.surface, self.pos, (self.name.font_h, self.name.font_h),
                       variables.white, variables.white, 0, variables.black)

    def show(self):
        self.box = Box(self.surface, self.pos, (self.name.font_h, self.name.font_h),
                       variables.white, variables.white, 0, variables.black)
        self.name.pos = (self.pos[0] + self.box.size[0] * 1.5, self.pos[1])
        self.name.show()
        if self.value:
            self.box.colour1, self.box.colour2 = variables.light_green, variables.light_green
            self.box.line_colour = variables.white
            line1 = Line(self.surface, self.pos, (self.pos[0] + self.box.size[0], self.pos[1] + self.box.size[1]),
                         variables.white)
            line2 = Line(self.surface, (self.pos[0], self.pos[1] + self.box.size[1]),
                         (self.pos[0] + self.box.size[0], self.pos[1]), variables.white)
        self.box.show()
        if self.value:
            line1.show()
            line2.show()

    def act(self, coord):
        in_width = self.box.pos[0] < coord[0] < self.box.pos[0] + self.box.size[0]
        in_height = self.box.pos[1] < coord[1] < self.box.pos[1] + self.box.size[1]
        if in_width and in_height:
            self.value = not(self.value)
            if self.value and callable(self.action):
                self.action()


class CheckList:
    # A class to check that displays a box of checkbox objects
    # If there are too many such objects, it creates as many checklist boxes as necessary
    def __init__(self, box, list_checkbox, start):
        self.box = box
        self.list_checkbox = list_checkbox
        self.check_height = list_checkbox[0].box.size[1]
        self.total_check = len(list_checkbox)
        self.check_per_box = ((self.box.size[1] - 3 * self.check_height) // (self.check_height * 2))
        self.number_of_box = math.ceil(self.total_check / self.check_per_box)
        self.displaying = start
        if self.number_of_box > 1:
            # Create superlist of sublists
            sub_list = []
            self.super_list = []
            for i in range(len(self.list_checkbox)):
                sub_list.append(self.list_checkbox[i])
                if ((i + 1) % self.check_per_box == 0 and i > 0) or i == len(self.list_checkbox) - 1:
                    self.super_list.append(sub_list)
                    sub_list = []
        self.triangle_list = []

    def show(self):
        if self.number_of_box <= 1:
            # print("One box")
            self.box.show()
            for i in range(len(self.list_checkbox)):
                self.list_checkbox[i].pos = (self.box.pos[0] + self.check_height,
                                             self.box.pos[1] + (2 * i + 2) * self.check_height)
                self.list_checkbox[i].show()
            # Text in top right corner
            info_text = Text(self.box.surface, (0, 0),
                             ["Page " + str(self.displaying + 1) + " / " + str(self.number_of_box)],
                             self.list_checkbox[0].name.font_name, self.list_checkbox[0].name.font_size,
                             self.list_checkbox[0].name.font_colour)
            info_text.pos = (self.box.pos[0] + self.box.size[0] - info_text.font_w * 1.2,
                             self.box.pos[1] + int(0.05 * self.box.size[1]))
            info_text.show()
        else:
            # print("many boxes")
            self.box.show()
            current_list = self.super_list[self.displaying]
            for i in range(len(current_list)):
                current_list[i].pos = (self.box.pos[0] + 2 * self.check_height,
                                       self.box.pos[1] + (2 * i + 2) * self.check_height)
                if i == 0:
                    first_pos = current_list[i].pos
                if i == len(current_list) - 1:
                    last_pos = current_list[i].pos
                current_list[i].show()
            # Text in top right corner
            info_text = Text(self.box.surface, (0, 0),
                             ["Page " + str(self.displaying + 1) + " / " + str(self.number_of_box)],
                             self.list_checkbox[0].name.font_name, self.list_checkbox[0].name.font_size,
                             self.list_checkbox[0].name.font_colour)
            info_text.pos = (self.box.pos[0] + self.box.size[0] - info_text.font_w * 1.2,
                             self.box.pos[1] + int(0.05 * self.box.size[1]))
            info_text.show()
            prev_triangle = Arrow(self.box.surface, variables.black, "North", (0, 0),
                                  (self.list_checkbox[0].box.size[0], ) * 2,
                                  functools.partial(self.change_displaying, -1))
            prev_triangle.pos = (first_pos[0] + 0.5 * prev_triangle.size[0],
                                 first_pos[1] - prev_triangle.size[1] * 1.25)
            prev_triangle.show()
            self.triangle_list.append(prev_triangle)
            next_triangle = Arrow(self.box.surface, variables.white, "South", (0, 0),
                                  (self.list_checkbox[0].box.size[0], ) * 2,
                                  functools.partial(self.change_displaying, 1),
                                  line_colour=variables.white)
            next_triangle.pos = (last_pos[0] + 0.5 * next_triangle.size[0], last_pos[1] + next_triangle.size[1] * 1.25)
            next_triangle.show()
            self.triangle_list.append(next_triangle)

    def change_displaying(self, increment):
        self.displaying += increment
        self.displaying %= (self.number_of_box)

    def act(self, coord):
        active_objects = self.list_checkbox + self.triangle_list
        for object in active_objects:
            object.act(coord)
