''' 
   Copyright 2020 TE Connectivity
   Distributed under MIT license.
   See file LICENSE for detail or copy at https://opensource.org/licenses/MIT
'''

def loop():
    import time
    import db
    import pubsub
    import settings
    import variables
    import numpy as np
    import math
    import random
    import scipy.optimize
    name = __name__
    Pub = pubsub.Pubsub()  # Initialize communication object
    Db = db.DB()  # Initialize data storage object
    St = settings.Settings()  # Initialize variable storage object
    Pub.get(name)  # Initialize comminication channel __name__
    time_loop = time.time()  # Start time loop (To regulate max FPS)
    while 1:  # Infinite loop start
        time.sleep(St.get("is_raspberry") * max(0, 1 / float(variables.FPS_analyser) - (time.time() - time_loop)))  # Sleep time to not trigger the script more than the FPS ratio fixed
        time_loop = time.time()  # Start time loop (To regulate max FPS)
        if Pub.get(name) or __name__ == "__main__" or not(St.get("is_raspberry")):  # triggered or test from computer (from convergence.py or from this file)
            sensors = Db.get_sensors_id()  # Get all sensor id
            for i in range(len(sensors)):
                id_sensor = sensors[i]
                actuators_involved = Db.get_actuator__sensors_id(id_sensor)  # Get all actuators that can influence the actual loop sensor
                dates = []  # Initialize list of last action of actuators
                for j in range(len(actuators_involved)):
                    id_actuator = actuators_involved[j]
                    try:
                        dates.append(Db.get_action_date(id_actuator))  # Get the last action of the actuator
                    except IndexError:  # If actuator never triggered
                        continue
                if dates == []:  # If no actuators involved
                    continue
                try:
                    Y = Db.get_measures_date_sensor(id_sensor, max(dates))  # Get all measures of the sensor since a given date
                except IndexError:  # If no measures
                    continue
                if __name__ == "__main__":  # Test block for display datas on computer
                    Y= [25.2, 25.1, 25, 24.8, 24.6]
                # print("raw",Y)
                if len(Y)<=4:  # If not enough datas
                    continue
                while True:  # Pic and constant data filter loop 
                    breaker = True  # breaker for the while loop
                    if len(Y) <= 4:  # If not enough datas
                        break  # Stop while loop
                    sens = 0
                    for j in range(len(Y) - 1):
                        m1 = Y[j]  # measure j (before)
                        m2 = Y[j + 1]  # measure j+1 (after)
                        if m1 > m2:  # if there is a decrease
                            if sens > 0:  # if there were a precedent increase
                                Y = Y[j:]  # Data removing
                                breaker = False  # Restart for loop with new datas
                                break  # Break for loop
                            else:
                                sens = -1  # Set the way (decrease)
                        elif m1 < m2:  # if there is an increase
                            if sens < 0:  # if there were a precedent decrease
                                Y = Y[j:]  # Data removing
                                breaker = False  # Restart for loop with new datas
                                break  # Break for loop
                            else:
                                sens = +1  # Set the way (decrease)
                    if breaker:  # If monotonus datas
                        break  # Stop while loop
                # print("filtered",Y)
                if len(Y) <= 4:  # If monotonus datas
                    continue
                def f(x, a, b, c):  # Function to fit to datas
                    return a + b * x **min(1, c)  # Have a look on the documentation for the justification of this function
                dt = 1 / variables.FPS_sensor  # Step time between two measures
                X = [i * dt for i in range(len(Y))]  # Generate X axis of Y datas
                target = Db.get_sensor_target_delta(id_sensor)  # get current target of the sensor
                outer = abs(Db.get_sensor_outer(id_sensor))  # Get first hysteresis relative-to-target value
                objective = target + outer * sens  # Calcul the objective (value where an actuator will change of state)
                average = sum(Y) / len(Y)  # Calcul average
                # if sens<0 and average<objective:  # Double check for no coherent datas
                    # continue
                # elif sens>0 and average>objective:  # Double check for no coherent datas
                    # continue
                precision = 10 ** -1  # power coeficient initialisation value
                while True:  # Fit function loop
                    try:  # Try to fit F on (X,Y) datas
                        popt, pcov = scipy.optimize.curve_fit(f, X, Y,
                            (
                                Y[-1],  # Initialize a coefficient (offset) with the last measure value
                                1,  # Initialize b with 1
                                precision  # Initialize c (power) with precision value (concave curve to line)
                            )
                        )
                    except Exception as e:  # If no fit found
                        pass
                    breaker = False  # initialisation of breaker for while loop
                    Xf = [1]  # X generate result
                    Yf = []  # Y generate result
                    counter = 1  # counter to avoid infinite loop (if the function tend to a finite value without crossing objective value)
                    while True:  # Extrapolation loop
                        try:
                            Yf.append(f(Xf[-1], *popt))  # Append Y value of the last X value
                        except:
                            break
                        if Xf[-1] > X[-1]:  # If start to predict the futur
                            if (Yf[-1] - objective) * sens > 0:  # if cross the objective line
                                breaker = True  # Stop the prediction
                                break
                            counter *= 1.1  # Increase counter
                        if counter > 10 ** 5:  # if there is no cross for next 24 hours
                            break
                        Xf.append(int(Xf[-1] + counter))  # Increase of one second
                    if breaker:  # If objective has been reached
                        break
                    if precision > 1.1:  # if no Fit curve found
                        break
                    precision *= 2  # Increase of the precision
                if precision > 1.1:  # If no fit found
                    continue
                if __name__ == "__main__":  # Test on computer block
                    import matplotlib.pyplot as plt
                    plt.scatter(X, Y)  # plot measures takes for calculation
                    plt.plot(Xf, Yf)  # plot calculation
                    plt.plot(Xf, [objective for i in Xf])  # plot objective
                    plt.show()  # Display graph
                if counter > 10 ** 5:  # If no cross found
                    continue
                time_to = Xf[-1] - X[-1]  # Time calculation
                date_change=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()+time_to))  # add the time_to value
                Db.set_sensor_date_change(id_sensor, date_change)  # Store the change date in the database
                # print("time_to",time_to)
                # print("date_change",date_change)
                pente = popt[1]
                if sens > 0:
                    Db.set_sensor_derivative_up(id_sensor, pente)  # Store the derivative up
                elif sens < 0:
                    Db.set_sensor_derivative_down(id_sensor, pente)  # Store the derivative down
        if St.get("is_unittest") or __name__ == "__main__":
            break
if __name__ == "__main__":
    import settings
    St = settings.Settings()  # Initialize variable storage object
    St.set("is_unittest", False)
    import convergence
    loop()

