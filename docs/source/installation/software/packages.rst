.. _packages:

Packages
=======================================
First we need to update and upgrade the packages that we already have and finally enable interfaces (SSH,SPI and I2C). Next step is to download
the packages that we need in order to control the TE Smart Thermostat.

**List of needed packages**

   * python3
   * git
   * scipy
   * queuelib
   * numpy
   * libatlas-base-dev
   * sqlite3
   * rpi.gpio
   * pygame
   * smbus
   * evdev
   * Waveshare driver

Last working version
--------------------

.. raw:: html

    <div class= "frame-container">
        <iframe id = "iframe-coverage" src="https://te_connectivity.gitlab.io/smart_thermostat/requirement.html" onload="this.width=0.6*screen.width;"></iframe>
    </div>

Update & Upgrade
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table::
  :align: center

  * - |gif_step_1_pac|


Simply type ``sudo apt-get update`` and after that ``sudo apt-get upgrade -y``.




Interfaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table::
   :align: center

   * - |gif_step_2_pac|

To enable interfaces we need to open Raspberry Pi configuration by command

.. code-block:: bash
   :linenos:

   sudo raspi-config

Next step is to by navigate through arrows on keyboard enable SPI,I2C,Serial,1-Wire and Remote GPIO interfaces.


Python 3
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Command to install Python 3 :

.. code-block:: bash
   :linenos:

   sudo apt-get install python3 -y

Git
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Command to install Git :

.. code-block:: bash
   :linenos:

   sudo apt-get install git -y

After installiation we can clone TE Smart Thermostat project from gitlab by

.. code-block:: bash
   :linenos:

   git clone https://gitlab.com/te_connectivity/smart_thermostat
   cd smart_thermostat
   git submodule init
   git submodule update

Scipy
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Command to install Scipy :

.. code-block:: bash
   :linenos:

   sudo apt-get install python3-scipy -y

Queuelib
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Command to install Queuelib :

.. code-block:: bash
   :linenos:

   sudo apt-get install python3-queuelib -y

Numpy
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Command to install Numpy :

.. code-block:: bash
   :linenos:

   sudo apt-get install python3-numpy -y

Libatlas-base-dev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Command to install Libatlas-base-dev :

.. code-block:: bash
   :linenos:

   sudo apt-get install libatlas-base-dev -y

Sqlite3
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Command to install Sqlite3 :

.. code-block:: bash
   :linenos:

   sudo apt-get install sqlite3 -y

Rpi.gpio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Command to install Rpi.gpio :

.. code-block:: bash
   :linenos:

   sudo apt-get install rpi.gpio -y

Pygame
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Command to install Pygame :

.. code-block:: bash
   :linenos:

   sudo apt-get install python3-pygame -y

Smbus
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Command to install Smbus :

.. code-block:: bash
   :linenos:

   sudo apt-get install python3-smbus -y

Evdev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Command to install Evdev :

.. code-block:: bash
   :linenos:

   sudo apt-get install python3-evdev -y

Waveshare driver
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. note::

  Before this step attach the screen to Raspberry Pi.

First we need to clone git files to **home directory** by:

.. code-block:: bash
   :linenos:

   cd /home/pi
   git clone https://github.com/waveshare/LCD-show.git

After that there are two commands to type

.. warning::
  “LCD_28-show” script will reboot your Rasberry Pi.

.. code-block:: bash
   :linenos:

   cd LCD-show/
   sudo ./LCD28-show lite

This step is not necessary for those who follow our :ref:`sd_setup` - adding text line to file **cmdline.txt**. In order to do it we need to open it using nano

.. code-block:: bash
   :linenos:

   sudo nano /boot/cmdline.txt

At the end of the file add

.. code-block:: bash
   :linenos:

   vt.global_cursor_default = 0

We're doing it so that the flashing cursor is not visible any more.
After this step reboot the Raspberry

.. code-block:: bash
   :linenos:

   sudo reboot

Just few last steps...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here, you will configure the auto-launch of the programm.

.. code-block:: bash
   :linenos:

   sudo nano /etc/rc.local

Add the following lines at the end of the file.

.. warning:: You have to add them **before** ``exit 0``

.. code-block:: bash
   :linenos:

   cd /home/pi/smart_thermostat
   sudo python3 convergence.py &

To close and save it simply use **CTRL+X** and then **y** to confirm changes and **Enter** to close.

.. note:: In order to change the database, you might need to remove the auto-launch.
