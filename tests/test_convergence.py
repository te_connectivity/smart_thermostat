import os
import sys
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))  # Get current dir
parentdir = os.path.dirname(currentdir)  # get parent dir
dir = parentdir + "/"
sys.path.insert(0, dir)  # insert path of parent dir in import settings

from convergence import *

class Test(unittest.TestCase):

    def setUp(self):
        St=settings.Settings()
        St.set("is_unittest",True)
        Db=db.DB()
    def test_sensor(self):
        sensor.loop()
    def test_analyser(self):
        analyser.loop()
    def test_controller(self):
        controller.loop()
    def test_pubsub(self):
        pubsub.Pubsub()
    def test_relay(self):
        relay.loop()
    def test_convergence(self):
        convergence()
    
if __name__ == '__main__':
    unittest.main()