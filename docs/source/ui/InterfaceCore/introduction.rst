The WhatToShow class
==========================

Introduction
-----------------

The class WhatToShow of the **gui.py** file is the backbone of the GUI for TE Smart Thermostat.
It contains attributes that store the values associated with most data in
the database and methods to interact with the **settings.json** and **db.db**
files, both located outside of the *smart_thermostat* folder, as well as to change
what is displayed on the screen (hence the name of the class).

In particular, it contains 6 methods - ``default``, ``home``, ``options``,
``settings``, ``new_setting``, and ``graphs`` - that each correspond to a menu
displayed to the user. We shall henceforth refer to them as *menu methods*.

The class WhatToShow is meant to exist in an infinite loop (obtained with ``while True`` in Python),
such as that which can be found in **refresh.py**, that each iteration, fetches
the coordinates of a potential touch, with the ``get_coord`` method, then calls the
``display`` method and the ``refresh`` method.

We will first look at the ``__init__`` method of WhatToShow.

.. note::

    Throughout the code of this class and of the **refresh.py** file, there often appears an
    if statement that checks whether or not the device the program is ran on is
    a Raspberry Pi or not. In effect, there are slight differences in the handling of a few functions,
    such as fetching coordinates of the touch or refreshing what is shown to the screen, between different devices.

    This is stored in the boolean value associated to the key ``is_raspberry`` of the settings dictionary in **settings.json**.
    The GUI (and the rest of the programs) will generally **not** work if the program considers it is running
    on a Raspberry Pi when running on a laptop and vice versa.

    The value of the boolean ``is_raspberry`` depends on whether the RPi.GPIO module can be imported or not,
    so it's always worth checking that you are able to import it if everything seems to be going wrong.
