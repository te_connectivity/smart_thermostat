Components
=======================================
.. centered::
  |gif_components|

Welcome to the guide to building your TE Smart Thermostat!
Here we present all the components we will need.

|pic_components|

.. list-table:: Table of components
   :align: center
   :widths: 25 25
   :header-rows: 1

   * - **Component**
     - **Quantity**
   * - 1. |Ambimate|
     - .. math:: 1
   * - 2. |Relay|
     - .. math:: 1
   * - 3. |Zero|
     - .. math:: 1
   * - 4. |Waveshare|
     - .. math:: 1
   * - 5. |Camera|
     - .. math:: 1
   * - 6. microSD card (at least 4 GB)
     - .. math:: 1
   * - 7. |Connector|
     - .. math:: 1
   * - 8. |Power|
     - .. math:: 1
   * - 9. Electrical Wires
     - .. math:: 11^{(1)}
   * - 10. :ref:`3D printed thermostat case <printing>`
     - .. math:: 1
   * - 11. Screws M2.5*5
     - .. math:: 6 
   * - 12. |Connector 2|
     - .. math:: 1
   * - 13. Soldering wires
     - .. math:: -
   * - 14. Soldering iron
     - .. math:: 1



.. note::
  :math:`^{(1)}` Wires enough for ONE actuator. For each additional, you need to count +2 wires.
  
.. warning:: Make sure your wires are adapted to the current intensity.

.. centered::
  |pic_components_optional|


.. list-table:: Table of optional components
   :align: center
   :widths: 25 25
   :header-rows: 1

   * - **Component**
     - **Quantity**
   * - 1. :ref:`Half-moon <printing>`
     - .. math:: 1
   * - 2. Lens adapter for Ambimate PIR sensor
     - .. math:: 1