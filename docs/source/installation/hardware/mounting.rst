.. _mounting:

Mounting
=======================================

Soldering wires to the screen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

   <p> <img class="pic_mounting" id=pic_step_0_con.JPG style="display:block;" src="../../_static/1_pic_step_0_con.JPG"> </p>


In the first step we will need soldering machine, tin, right angle connector and **Waveshare screen**.

First, place the connectors in the way shown above and then flip the screen to the other side and start soldering every pin separately.

Soldering wires to the relay
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

     <img class="pic_mounting" id=pic_step_1_con.JPG style="display:block;" src="../../_static/1_pic_step_1_con.JPG" />

There are five wires need soldering here - four that will be connected to Raspberry Pi (orange, yellow, green, blue) and two connected to the screen directly (purple and blue).

.. warning::
    If you are going to use all four actuators, please uncomment (remove '#' sign) part for additional heating in file **variables.py**:
    
    .. code-block:: python

        # "HE+": [6, [["Temperature", True, True]]],

Connecting actuator to relay
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <img class="pic_mounting" id=pic_step_2_con.JPG style="display:block;" src="../../_static/1_pic_step_2_con.JPG" />


Connect wires from your actuator to **Raspberry Pi 4 Channel Relay HAT (RAS-075)**.

.. warning::
  Note that the each switch is normally open and the wires locations depends on it.

Attaching Pi Zero camera
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <img class="pic_mounting" id=pic_step_3_con.JPG style="display:block;" src="../../_static/1_pic_step_3_con.JPG" />
     

Next we will attach Pi Zero camera to the case. In order to do it try firmly push camera into the hole.

Placing lens adapter for Ambimate PIR sensor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. note::
    Steps 1.5 and 1.6 are optional.

.. raw:: html

    <img class="pic_mounting" id=pic_step_4_con.JPG style="display:block;" src="../../_static/1_pic_step_4_con.JPG" />

    
Place the lens adapter for Ambimate PIR sensor and try to rotate it until you find perfect position. Then push it until you hear 'click'.

Placing half-moon
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <img class="pic_mounting" id=pic_step_5_con.JPG style="display:block;" src="../../_static/1_pic_step_5_con.JPG" />
     

Place the half-moon part in the front side of the case. 

Attaching Ambimate module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

   <img class="pic_mounting" id=pic_step_6_con.JPG style="display:block;" src="../../_static/1_pic_step_6_con.JPG" />


Next step is to attach **Ambimate module**.

.. warning::  There is also a 'click'.

Attaching Waveshare screen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <img class="pic_mounting" id=pic_step_7_con.JPG style="display:block;" src="../../_static/1_pic_step_7_con.JPG" />
     


Now we can attach a **Waveshare screen** to the case.

Screwing in the middle part
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <img class="pic_mounting" id=pic_step_8_con.JPG style="display:block;" src="../../_static/1_pic_step_8_con.JPG" />
     

When both camera and Ambimate moduls are attached, we can screw in the middle part of the case. 

Attaching relay
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <img class="pic_mounting" id=pic_step_9_con.JPG style="display:block;" src="../../_static/1_pic_step_9_con.JPG" />
     


Put **Raspberry Pi 4 Channel Relay HAT (RAS-075)** in given place, use the screws to secure it.

Conencting relay to the screen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <img class="pic_mounting" id=pic_step_10_con.JPG style="display:block;" src="../../_static/1_pic_step_10_con.JPG" />
     


The blue wire is connected to the **ground** of the display and the purple one to **5V** power pin.

Connecting Ambimate module and screen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <img class="pic_mounting" id=pic_step_11_con.JPG style="display:block;" src="../../_static/1_pic_step_11_con.JPG" />
     

Connect **Ambimate module** and screen in the way shown above.

Connecting relay to the Raspberry
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <img class="pic_mounting" id=pic_step_12_con.JPG style="display:block;" src="../../_static/1_pic_step_12_con.JPG" />
     

Conenct the wires from relay with **Raspberry Pi** according to the :ref:`connection` page. 

Connecting Raspberry and screen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

    <img class="pic_mounting" id=pic_step_13_con.JPG style="display:block;" src="../../_static/1_pic_step_13_con.JPG" />
     

Connect **Raspberry Pi Zero** to LCD screen.

Connecting Pi Camera with Raspberry Pi
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

    <img class="pic_mounting" id=pic_step_14_con.JPG style="display:block;" src="../../_static/1_pic_step_14_con.JPG" />
     

It is high time to connect Raspberry Pi camera.

.. note::
  Remember that before connecting, unlock the connector - to do so gently drag the parts marked on the picture.
  After attaching the ribbon cable, push them back.

Connecting charger
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <img class="pic_mounting" id=pic_step_15_con.JPG style="display:block;" src="../../_static/1_pic_step_15_con.JPG" />
     


Connect charger cable to **Raspberry Pi Zero**. 

It's almost done now!

Organising wires
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

   <img class="pic_mounting" id=pic_step_16_con.JPG style="display:block;" src="../../_static/1_pic_step_16_con.JPG" />
     

Just before closing the case it's worth to check if the wires are nicely organised.



Finish!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <img class="pic_mounting" id=pic_step_17_con.JPG style="display:block;" src="../../_static/1_pic_step_17_con.JPG" />
     

It's done! Now you can start to programme.
