Common issues
=============

Actuators are inverted
------------------------
There are 4 ways to fix this issue:

* Change the GPIO values in the database using FileZilla and DB-Browser.
* Invert the concerned 5V-wires on the raspberry.
* Invert the concerned 5V-wires on the relay board.
* Invert the concerned power-wires on the relay board.

The tactile function of the screen is not working
--------------------------------------------------
There are 3 possible explanations for this:

* The screen not is correctly stacked on the raspberry.
* The chassis is pressing against the screen.
* The screen has been shortcutted.

The screen is not displaying anything
---------------------------------------
There are 3 possible explanations for this:

* The screen not is correctly stacked on the raspberry.
* The screen driver is not correctly installed.
* The program is not running.

Home property value is unreasonably high
------------------------------------------
The possible reason could be the lack of smbus package. Please try to run "sudo apt-get install python3-smbus" command.
