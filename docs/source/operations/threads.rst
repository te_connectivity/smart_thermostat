Threads
=======================================
How it works
------------
The program has to be able to manage many functionalities independently.
To achieve this, the system uses threads in order to run all loops in parallel.
The code inside each loop is triggered by the Pubsub system.
Every loop has an FPS setting: frames per seconds.
This FPS indicates how many times (at most) the pubsub channel is checked each second.

.. list-table:: Thread system
   :align: center

   * - |scheme_threads_run|


.. note :: 
    This architecture allows to have many applications launching a
    functionality without using the same code several times every second.

Loops files
------------
The following files are all loops that run in the background.

Files:
    - **sensor.py**
    - **controller.py**
    - **relay.py**
    - **analyser.py**
    - **refresh.py**

Loop template
--------------

.. code-block:: python
    :emphasize-lines: 3,5,8,14,15,16,21
    :linenos:

    def loop():
        import MODULES

        name=__name__

        objects=module.class()

        Pub.get(name)
        time_start=time.time()

        while 1:
            time_loop=time.time()
            if Pub.get(name) or __name__=="__main__" or not(St.get("is_raspberry")):

                {CODE LOOP}

            if __name__=="__main__" or not(St.get("is_raspberry")):
                break
            else:
                time.sleep(max(0,1/float(variables.FPS_name)-(time.time()-time_loop)))

    if __name__=="__main__":
        loop()

This template allows to run the code as part of a unit test on a computer, from the convergence directory and from a raspberry pi.

Threads run
-----------
.. literalinclude:: ../../../convergence.py
    :language: python
    :linenos:
