[![pipeline status](https://gitlab.com/te_connectivity/smart_thermostat/badges/master/pipeline.svg)](https://gitlab.com/te_connectivity/smart_thermostat/commits/master)
[![coverage report](https://te_connectivity.gitlab.io/smart_thermostat/coverage.svg)](https://te_connectivity.gitlab.io/smart_thermostat/coverage.html)
[![Documentation](https://img.shields.io/badge/Documentation-Smart_Themostat-blue)](https://te_connectivity.gitlab.io/smart_thermostat) 

# TE Smart Thermostat

Like many engineers, we love finding ways to solve problems, creating next-gen innovations and collaborating with others. We also know that we truly never shut off – we look for DIY projects, tinker at home and engage in maker communities.

A group of us at TE Connectivity took our popular [AmbiMate](https://www.te.com/usa-en/products/sensors/multi-sensor-modules.html?tab=pgp-storyl) sensor module MS4 series and created a smart thermostat using a Raspberry Pi.  AmbiMate provides an application specific set of sensors on a ready to attach PCB assembly.  With the code we released on GitLab, we provide you with easy solutions for light, temperature, humidity, VOC and eCO2. We also created an easy to 3D print enclosure/casing for protection and wall mounting.  

We hope you’ll enjoy this project and will continue to explore more [connectivity and sensor solutions](http://te.com/) for all your technology needs.

## Features
- Simple touchscreen GUI
- Choose unit of temperature (°C or °F)
- Temperature up/down controls
- Display state of heating controls
- Very simple algorithm to control heater/aircon/fan/etc
- Date and time based schedules of operation each with a user set target temperature
- Application configuration via plain text config file.
- History of sensor readings

## To see full documentation follow this link:

https://te_connectivity.gitlab.io/smart_thermostat