The Radio Class
==================

This class is used to display a `radio <https://react.semantic-ui.com/addons/radio/#types-radio-group>`_ on the screen. Radio objects are used in situation when the user chooses
one option among mutually exclusive options. **Note that a radio object uses a list of Text objects with attribute** *self.text* **of length 1 only.**


__init__ method
----------------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Radio.__init__
    :linenos:
    :language: python

List of attributes :
^^^^^^^^^^^^^^^^^^^^^^^^

* *self.surface*: As discussed previously this is generally the screen surface.

* | *self.pos*: A position tuple defining the top left corner of the rectangular zone
  | enclosing the radio.

* | *self.list_names*: A python list of Text objects. Their pos attribute as entered is useless,
  | as it will be recalculated in the show method.

* | *self.list_actions*: A python list of python functions that correspond to the action triggered
  | when the circle corresponding to a text object in *self.list_names* is touched.

* | *self.setting*: Indicates which value of the radio is currently selected. It is important to have an initial value.
  | The value of self.setting is a string that corresponds to the first line of the text associated with the selected option.

* | *self.radius*: The radius of the radio circles in pixel.
  | This is calculated as half of the maximum text height of all text elements in *self.list_names*.

* | *self.list_centres*: A list of tuples corresponding to the coordinates of the centre of
  | each of the radio circles.

.. note::

    * It is important that *self.list_actions* be of the same length as *self.list_names* for correct operation.

    * | *self.list_centres* is calculated with two adjacent centres located a distance of four radii apart.
      | This implies that there is a distance of 1 radio circle's diameter between two adjacent circles.
    * In the ``__init__`` method, the action corresponding to the initial setting is called (if it is callable).

show method
-------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Radio.show
    :linenos:
    :language: python

This method iterates through *self.list_names* with the index ``i`` and shows the associated white circle,
(centred on the point ``self.list_centres[i]``). When the circle corresponding to the user's selection is
met, a smaller black circle (with a radius 3 times smaller than *self.radius*) is also added.
Lastly, the text object associated with the circle has its pos attribute reset (a radius to the right
of the corresponding circle and at the same starting height as the circle). Once pos has
been recalculated, the associated text object is displayed.

act method
---------------

.. literalinclude:: ../../../../gui.py
    :pyobject: Radio.act
    :linenos:
    :language: python

This method takes the coordinate of the last touch as an argument and
iterates through *self.list_centres* with index ``i``. For each iteration,
it checks whether the last touch occurred in the touch zone centred on the centre of the circle.
Note that the shape of the touch zone is the **square** of side length :math:`2 \times self.radius`
and tangent to the corresponding white circle on all sides.
When the last touch occurred in the touch zone, the attribute *self.setting* is
changed accordingly and the associated function in *self.list_actions* is called (if it is callable).

Final example
--------------


.. code-block:: python
    :linenos:

    example_list_names = [Text(self.surface, (0, 0), [text_line], self.font_name, 13, variables.black)
                          for text_line in ["Cheese", "Dessert", "Both cheese and dessert"]]
    example_list_actions = ["choose_cheese", "chosse_dessert", "choose_both"]
    example_radio = Radio(self.surface, (100, 30), example_list_names,
                          example_list_actions, "Dessert")
    example_radio.show()

If you are confused about the syntax employed to define ``example_list_names``, you may
want to read about python `list comprehension <https://www.pythonforbeginners.com/basics/list-comprehensions-in-python>`_.

This code produces:

.. image:: /_static/example_radio.svg
    :align: center
    :alt: alternate

By clicking in each circle a user could choose if he wishes to have cheese, dessert, or both cheese and dessert,
provided the ``example_list_actions`` variable was defined with appropriate functions.
