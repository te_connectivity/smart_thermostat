Regulation loop
===============

Specifications
--------------
The goal of the regulation system is to hold the value read by a sensor constant by controlling an actuator.
The following requirements also apply:

- It is only possible to put an actuator in an ``ON`` or ``OFF`` state.
- Actuators must not act as PWM regulation systems, their state has to be changed as little as possible.
- It is not acceptable that an actuator that reduces the value of a sensor is activated right after or while another actuator
  that increases it is activated.

System behaviour
----------------
To satisfy those requirements, we use a double hysteresis system.

Here is an example of the regulation system on the temperature.

.. list-table:: Regulation behavior
   :align: center

   * - |graph_regulation_HE|
     - |graph_regulation_AC|
   * - 	.. centered:: Double hysteresis graph Heater
     - 	.. centered:: Double hysteresis graph Climatisation


Example explanation
-------------------
Let's take the second example: **Double hysteresis graph Climatisation**

There are 5 thresholds:
    - ``OUT_SUP`` at 17 °C (62.6 °F)
    - ``OUTER_SUP`` at 16 °C (60.8 °F)
    - ``TARGET`` at 15 °C (59 °F)
    - ``OUTER_INF`` at 14 °C (57.2 °F)
    - ``OUT_INF`` at 13 °C (55.4 °F)

Below is a description of the system's behaviour:

At 0 s
    ``TEMPERATURE = 16.6°C (61.9 °F)``

    The system already has the AC actuator activated, and the temperature is higher than ``OUTER_INF``, no changes are done.
At 700 s
    ``TEMPERATURE = 14°C (57.2 °F)``

    The temperature is lower than ``OUTER_INF``, the AC is turned ``OFF``. The thermal mass of the system will make the temperature continue to decrease slightly.
    If the temperature go lower than ``OUT_INF``, the heater will be turned ``ON``.
From 700 s to 1000 s
    The temperature goes up due to thermal losses.
    For the system, this implies that the exterior temperature is higher than the target.
At 1000 s
    ``TEMPERATURE = 16.6°C (61.9 °F)``

    The AC actuator is turned ``ON`` because the temperature is higher than ``OUTER_SUP``.
After 1000 s
    The cycle repeats every 800 s.

Backup actuator
---------------
If an actuator is not powerfull enough to provide the power to reach the wanted value, 
it will be detected by the algorithm and backups actuators will be enable.

If the derivative of the measure concerning the *normal actuator* if opposite to the ``up`` value of the actuator, 
it will activate the backup actuator.

.. code-block:: python
    :linenos:
    :emphasize-lines: 4
    
    derivative=0
    for j in range(len(Y) - 1):  # Y is a list containing the n last values of the sensor
        derivative += Y[j+1] - Y[j]
    derivative /= len(Y) * variables.FPS_sensor #Average derivative of last 10 measures of the sensor
    if derivative * (up * 2 - 1) > 0: # If derivative without backup is fine
        state = False  # Activate the backup actuator
    else: # If derivative without backup not fine
        state = True  # Desactivate the backup actuator

Loop code
---------------------------
.. literalinclude:: ../../../controller.py
    :emphasize-lines: 1
    :language: python
    :linenos:
    :pyobject: loop
