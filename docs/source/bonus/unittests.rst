Unit tests
==========
Coverage result
---------------
|badge_coverage|

.. raw:: html

    <script>
    function refreshFrame(){
        document.getElementById("iframe-coverage").src = 'https://te_connectivity.gitlab.io/smart_thermostat/coverage.html';
    }
    </script>

    <div class= "frame-container">


        <button class = "iframe-button" onClick="refreshFrame();"> ← </button>
        <iframe id = "iframe-coverage" src="https://te_connectivity.gitlab.io/smart_thermostat/coverage.html" onload="this.width=0.6*screen.width;this.height=screen.height*0.7;">

        </iframe>
    </div>

Tests source code
-----------------
Convergence test
~~~~~~~~~~~~~~~~
.. literalinclude:: ../../../tests/test_convergence.py
    :language: python
    :pyobject: Test
    :linenos:

In converance test we're checking if modules as *sensor*, *analyser*, *controller*, *pubsub*, *relay* and *convergance* itself are able to be imported.


Pubsub test
~~~~~~~~~~~
.. literalinclude:: ../../../tests/test_pubsub.py
    :language: python
    :pyobject: Test
    :linenos:

In this module we're testing if sent message ("message_test") in channel *"pub_test"* is adding the message in the pubsub database.

Controller test
~~~~~~~~~~~~~~~
.. literalinclude:: ../../../tests/test_controller.py
    :language: python
    :pyobject: Test
    :linenos:

Setup of controller test requires creating database, *target*(1000), *outer*(100) and *out*(10). In *test_controller* are tested
all the possibilities of the output state of actuator. This output depends on actuator previous state, the relation between *target* value and value
from the sensor and if the actuator is taking value down (ex. cooler for temperature) or up (ex.heater for temperature).
The *test_backup* is made in case of use backup actuator. It also needs clearing database and creating actuator connected to the sensor.
The backup actuator will be turned on in case in which normal actuator is not able to change sensor output.

Relay test
~~~~~~~~~~
.. literalinclude:: ../../../tests/test_relay.py
    :language: python
    :pyobject: Test
    :linenos:

In the setup we activate pubsub and create a new database. Then we remove all existing actuators and create one called *Test_actuator*.
Finally, the new value (1.0) is assigned to *Test_actuator* and in relay we check if actuator has the right value.

Analyser test
~~~~~~~~~~~~~
.. literalinclude:: ../../../tests/test_analyser.py
    :language: python
    :pyobject: Test
    :linenos:

In the setup we: create and clear database, create a test sensor which will have the setting *target* (1000), *outer* (100) and *out* (100),
create test actuator and connect it to the sensor.
Then in *test_analyse* we're creating sensor history and we're asking analyser for prediction to check it create a prediction.
