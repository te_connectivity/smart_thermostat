Smart Thermostat
================

**Release and Indemnification**


You hereby release, waive,  discharge, and promise not to sue TE Connectivity Corporation (TE), and its parent company, subsidiaries and affiliates and their directors, officers,  employees, and agents (“The Releasees”) from liability for any and all claims including the Negligence, Strict Liability, Breach of Contract, or any other claim for damages against The Releasees, resulting in personal injury (including death), accidents or illnesses, and   destruction or loss of property arising from, the assembly, use and operation of the Product.  You also agree to indemnify and hold The Releasees harmless from any and all claims, actions, suits, procedures, costs, expenses, damages and liabilities, including attorney’s fees arising out of your assembly, use and operation of the Product and You agree to reimburse the Releasees from any such expenses incurred.  You further agree that this Waiver of Liability and Indemnity Agreement is intended to be as broad and inclusive as is permitted by law, and that if any portion thereof is held invalid its remaining portions will continue to have full legal force and effect.


.. image:: /_static/header.svg
    :width: 100%

.. toctree::
   :caption: Introduction:

   Presentation <introduction/presentation>
   Components <introduction/components>

.. image:: /_static/header.svg
    :width: 100%

.. toctree::
   :caption: Setup:

   Raspberry setup <installation/raspberry/index>
   Hardware setup <installation/hardware/index>
   Software setup <installation/software/index>
   Final setup <installation/final/index>

.. image:: /_static/header.svg
    :width: 100%

.. toctree::
   :caption: Operations:

   Database <operations/database>
   Threads <operations/threads>
   Pubsub <operations/pubsub>
   Settings <operations/settings>
   Sensor <operations/sensor>
   Regulation <operations/regulation>
   Relay <operations/relay>
   Analyser <operations/analyser>

.. image:: /_static/header.svg
    :width: 100%

.. toctree::
    :caption: User interface:

    Basic classes <ui/BasicClasses/index>
    Interface core <ui/InterfaceCore/index>

.. image:: /_static/header.svg
    :width: 100%

.. toctree::
   :caption: Bonus:

   Ambimate module <ambimate_python_driver/docs/source/index>
   Informations <bonus/informations>
   Common issues <bonus/issues>
   Disclaimer <bonus/disclaimer>
   Unit tests <bonus/unittests>
