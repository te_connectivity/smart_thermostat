Change database
===============

Transfert database
--------------------------

Connect your computer to your raspberry pi with **FileZilla**.

.. list-table:: FileZilla > Site Manager > (Protocol / Host / User / Password)
  :align: center

  * - |screen_fileZilla_connection|


Then transfert the file ``/home/pi/db.db`` in your computer.

.. list-table:: Path: ``/home/pi/db.db`` > Right click > **Add files to queue**
  :align: center

  * - |screen_fileZilla_transfert|

Open database
-----------------

Run ``db-browser`` tool in order to open the database transfered.

Then open with this software ``db.db``.

Change datas
------------

Go to *Browse Data > Table: sensor* to add a actuator.
Here you can change GPIO and actuator name.

Go to *Browse Data > Table: actuator_sensor* to add a link between actuator and sensors.
Here you can set an actuator ID with a sensor ID. 
Then you define id the actuator increase (up=1) or decrease (up=0) then sensor value.
Finally, you define if the actuator is a backup (backup=1) or a normal actuator (backup=0)

Go to *Browse Data > Table: sensor* to add a foreign sensor of the ambimate module.
Then go to ``sensor.py`` and add the code to get the value of your sensor in order to use the function ``set_sensor_value(self,id_sensor,value)``

Send new database to your raspberry
-----------------------------------

Connect your computer to your raspberry pi with **FileZilla**.

Then drag and drop you computer version of the database to your raspberry pi.

.. warning:: You must close every instance of the database from both side to avoid any fail transfert.