import inspect
import os
import sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))  # Get current dir
parentdir = os.path.dirname(currentdir)  # get parent dir
dir = parentdir + "/"
sys.path.insert(0, dir)  # insert path of parent dir in import settings

from convergence import *

class Test(unittest.TestCase):

    def setUp(self):
        self.St = settings.Settings()
        self.St.set("is_unittest",True)
        self.Db = db.DB()
        self.target = 10**3
        self.outer = 10**2
        self.out = 10**1


    def test_controller(self):
        for up in [0,1]:
            for state in [0,1]:
                self.Db.clear_bdd()
                self.Db.insert_sensor("test_sensor", self.target, self.outer, self.out)
                self.Db.insert_actuator("test_actuator")
                self.id_sensor = self.Db.get_sensor_id("test_sensor")
                self.id_actuator = self.Db.get_actuator_id("test_actuator")
                self.Db.insert_actuator__sensor(self.id_sensor, self.id_actuator ,up, 0)
                for m in [
                    self.target + self.outer + self.out + 1,
                    self.target + self.outer + 1,
                    self.target + 1,
                    self.target - 1,
                    self.target - self.outer - 1,
                    self.target - self.outer - self.out - 1
                    ]:

                    is_out_inf = int(m < self.target - self.outer - self.out)
                    is_outer_inf = int(m >= self.target - self.outer - self.out and m < self.target - self.outer)
                    is_in_inf = int(m >= self.target - self.outer and m < self.target)
                    is_in_sup = int(m >= self.target and m <= self.target + self.outer)
                    is_outer_sup = int(m > self.target + self.outer and m <= self.target + self.outer + self.out)
                    is_out_sup = int(m > self.target + self.outer + self.out)

                    self.Db.set_actuator_value(self.id_actuator, state)
                    self.Db.set_sensor_value(self.id_sensor, m)
                    controller.loop()

                    A,B,C,D,E,F,G,H=up,state,is_out_sup,is_outer_sup,is_in_sup,is_in_inf,is_outer_inf,is_out_inf
                    nA,nB,nC,nD,nE,nF,nG,nH=1-A,1-B,1-C,1-D,1-E,1-F,1-G,1-H
                    O=self.Db.get_actuator_state(self.id_actuator)

                    self.assertEqual(O,
                    min(1,
                        (nA*nC*D*nE*nF*nG*nH)+
                        (nA*C*nD*nE*nF*nG*nH)+
                        (nC*nD*nE*nF*nG*nH)+
                        (A*nC*nD*nE*nF*nG*H)+
                        (A*nC*nD*nE*nF*G*nH)+
                        (B*nC*nD*nE*F*nG*nH)+
                        (B*nC*nD*E*nF*nG*nH)+
                        (C*nD*nE*nF*nG*H)+
                        (C*nD*nE*nF*G*nH)+
                        (C*nD*nE*F*nG*nH)+
                        (C*nD*E*nF*nG*nH)+
                        (C*D*nE*nF*nG*nH)+
                        (D*nE*nF*nG*H)+
                        (D*nE*nF*G*nH)+
                        (D*nE*F*nG*nH)+
                        (D*E*nF*nG*nH)+
                        (E*nF*nG*H)+
                        (E*nF*G*nH)+
                        (E*F*nG*nH)+
                        (F*nG*H)+
                        (F*G*nH)+
                        (G*H)
                    ))


    def test_backup(self):
        for up in [0,1]:
            self.Db.clear_bdd()
            self.Db.insert_sensor("test_sensor", self.target, self.outer, self.out)
            self.Db.insert_actuator("test_actuator")
            self.id_sensor = self.Db.get_sensor_id("test_sensor")
            self.id_actuator = self.Db.get_actuator_id("test_actuator")
            self.Db.insert_actuator__sensor(self.id_sensor, self.id_actuator, up, 1)

            self.Db.set_actuator_value(self.id_actuator, 0)
            self.Db.set_sensor_value(self.id_sensor, self.target - (self.outer + self.out + 1) * (up * 2 - 1))
            controller.loop()
            self.assertEqual(self.Db.get_actuator_state(self.id_actuator), 0)

            for i in range(variables.backup_steps):
                self.Db.set_sensor_value(self.id_sensor, self.target - (self.outer + self.out + 1) * (up * 2 - 1))
            controller.loop()
            self.assertEqual(self.Db.get_actuator_state(self.id_actuator), 1)


if __name__ == '__main__':
    unittest.main()
