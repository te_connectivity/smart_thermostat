Basic classes
=============

.. image:: /_static/header.svg
    :width: 100%

.. toctree::
    :caption: Basic Classes for the UI:

    Classes <classes>
    Box <box>
    Text <text>
    Tab <tab>
    Image <image>
    Line <line>
    Clock <clock>
    Arrow <arrow>
    Radio <radio>
    CheckBox <checkbox>
    CheckList <checklist>
