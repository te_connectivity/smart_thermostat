The change_attribute method
===================================

This method allows us to change attribute in an easy and compact manner.
Combined with the functools.partial function, this gives an extremelly powerful tool to modify attributes of the WhatToShow class at will.

Before talking about it further we must first digress and explore the subject of python decorators.

Decorators
---------------

In essence, a decorator, is a callable object that may be applied to a python function and "decorate" it
by extending its functionality but without changing its definition.

It is quite a confusing topic, especially for python beginners, and you may want to `read about it more <https://realpython.com/primer-on-python-decorators/>`_.
Now, we will be using a particular kind of built in decorators:
the @property and @values.setter.
You are encouraged to read about an `example of their use <https://realpython.com/primer-on-python-decorators/>`_ to fully understand what is going on.

Now notice the two methods right after the ``__init__`` method: they are both called ``values`` but take different arguments
and are decorated differently.

.. code-block:: python
    :linenos:

    @property
    def values(self):
        return self._values

    @values.setter
    def values(self, new_values):
        self._values = new_values

What does this do?

Recall that the attribute *self.values* is a dictionary that has the name of all the attributes of WhatToShow
and values their corresponding value.

For example, after initialsation, ``print(self.values)`` could output the
following to the console:

.. code-block:: console
    :linenos:

    {'set': <settings.Settings object at 0x0000014C76D32BE0>, 'is_raspberry': False, 'font_name': 'C:\\Users\\TE392816\\Documents\\smart_thermostat\\files\\fonts\\Open_Sans\\OpenSans-Regular.ttf', 'font_name_bold': 'C:\\Users\\TE392816\\Documents\\smart_thermostat\\files\\fonts\\Open_Sans\\OpenSans-Bold.ttf', 'font_name_italic': 'C:\\Users\\TE392816\\Documents\\smart_thermostat\\files\\fonts\\Open_Sans\\OpenSans-RegularItalic.ttf', 'font_name_bold_italic': 'C:\\Users\\TE392816\\Documents\\smart_thermostat\\files\\fonts\\Open_Sans\\OpenSans-BoldItalic.ttf', 'surface': <Surface(320x240x32 SW)>, 'width': 320, 'height': 240, 'time_format': '%H:%M', 'date_format': '%d/%m', 'db': <db.DB object at 0x0000014C76D3BA58>, 'pub': <pubsub.Pubsub object at 0x0000014C784DC6D8>, 'changed_menu': True, 'menu': <bound method WhatToShow.default of <gui.WhatToShow object at 0x0000014C76D32BA8>>, 'start': 1568905247.7704978, 'last_coord': None, 'coord': None, 'mode': 'auto', 'py_touch': False, 'actuator_dict': {'AC': (1, 1), 'FAN': (3, 0), 'HE': (2, 0)}, 'list_sensor_names': ['Temperature', 'Humidity'], 'list_sensor_displayed': ['Temperature', 'Humidity'], 'list_sensor_id': [1, 2], 'list_sensor_id_displayed': [1, 2], 'list_sensor_dict': {'Temperature': {'name': 'Temperature', 'sensor_id': 1, 'unit': '�C', 'current': 24.3, 'target': 18.5, 'setting_delta': 0, 'target_time': '...', 'derivative_up': 0.0528928431266015, 'derivative_down': -0.017543709565332046, 'outer': 0.6, 'out': 1.0, 'active': True}, 'Humidity': {'name': 'Humidity', 'sensor_id': 2, 'unit': '%', 'current': 33.2, 'target': 30.0, 'setting_delta': 0, 'target_time': '...', 'derivative_up': 0.0, 'derivative_down': 0.0, 'outer': 5.0, 'out': 10.0, 'active': True}}, 'list_sensor_dict_displayed': {'Temperature': {'name': 'Temperature', 'sensor_id': 1, 'unit': '�C', 'current': 24.3, 'target': 18.5, 'setting_delta': 0, 'target_time': '...', 'derivative_up': 0.0528928431266015, 'derivative_down': -0.017543709565332046, 'outer': 0.6, 'out': 1.0, 'active': True}, 'Humidity': {'name': 'Humidity', 'sensor_id': 2, 'unit': '%', 'current': 33.2, 'target': 30.0, 'setting_delta': 0, 'target_time': '...', 'derivative_up': 0.0, 'derivative_down': 0.0, 'outer': 5.0, 'out': 10.0, 'active': True}}, 'last_measure': 1568905247.7794733, 'default_property': 'Temperature', 'home_property': 'Temperature', 'options_screen': 0, 'options_screen1_displaying': 0, 'setting_step': 1, 'table_setting': [], 'setting_page': 0, 'new_setting_dict': {'confirm': False}, 'graph': 'D', 'start_graph': time.struct_time(tm_year=2019, tm_mon=9, tm_mday=18, tm_hour=16, tm_min=0, tm_sec=47, tm_wday=2, tm_yday=261, tm_isdst=1), 'max_number_points': 100, 'measures': [('09/18/2019, 17:14:07', 28.8), ('09/18/2019, 17:26:28', 29.8), ('09/18/2019, 17:38:48', 29.6), ('09/18/2019, 17:51:09', 29.4), ('09/18/2019, 18:03:29', 29.5), ('09/18/2019, 18:15:50', 29.6), ('09/18/2019, 18:28:11', 29.6), ('09/18/2019, 18:40:31', 29.6), ('09/18/2019, 18:52:52', 29.6), ('09/18/2019, 19:05:12', 29.6), ('09/18/2019, 19:17:33', 29.7), ('09/18/2019, 19:29:53', 29.6), ('09/18/2019, 19:42:14', 29.6), ('09/18/2019, 19:54:35', 29.6), ('09/18/2019, 20:06:55', 29.6), ('09/18/2019, 20:19:16', 29.5), ('09/18/2019, 20:31:36', 29.5), ('09/18/2019, 20:43:57', 29.5), ('09/18/2019, 20:56:17', 29.4), ('09/18/2019, 21:08:38', 29.3), ('09/18/2019, 21:20:59', 29.6), ('09/18/2019, 21:33:19', 31.1), ('09/18/2019, 21:45:40', 30.1), ('09/18/2019, 21:58:00', 29.6), ('09/18/2019, 22:10:21', 29.3), ('09/18/2019, 22:22:42', 31.2), ('09/18/2019, 22:35:02', 30.2), ('09/18/2019, 22:47:23', 29.6), ('09/18/2019, 22:59:43', 29.2), ('09/18/2019, 23:12:04', 31), ('09/18/2019, 23:24:24', 30), ('09/18/2019, 23:36:45', 29.5), ('09/18/2019, 23:49:06', 30.5), ('09/19/2019, 00:01:26', 30.4), ('09/19/2019, 00:13:47', 29.7), ('09/19/2019, 00:26:07', 29.6), ('09/19/2019, 00:38:28', 30.7), ('09/19/2019, 00:50:48', 29.8), ('09/19/2019, 01:03:09', 29.3), ('09/19/2019, 01:15:30', 31.2), ('09/19/2019, 01:27:50', 30), ('09/19/2019, 01:40:11', 29.3), ('09/19/2019, 01:52:31', 31.1), ('09/19/2019, 02:04:52', 30.1), ('09/19/2019, 02:17:13', 29.4), ('09/19/2019, 02:29:33', 30.7), ('09/19/2019, 02:41:54', 30.1), ('09/19/2019, 02:54:14', 29.4), ('09/19/2019, 03:06:35', 31.2), ('09/19/2019, 03:18:55', 29.9), ('09/19/2019, 03:31:16', 29.3), ('09/19/2019, 03:43:37', 30.8), ('09/19/2019, 03:55:57', 29.7), ('09/19/2019, 04:08:18', 29.8), ('09/19/2019, 04:20:38', 30.2), ('09/19/2019, 04:32:59', 29.4), ('09/19/2019, 04:45:20', 31.2), ('09/19/2019, 04:57:40', 29.8), ('09/19/2019, 05:10:01', 29.8), ('09/19/2019, 05:22:21', 30.3), ('09/19/2019, 05:34:42', 29.4), ('09/19/2019, 05:47:03', 31), ('09/19/2019, 05:59:23', 29.6), ('09/19/2019, 06:11:44', 30.5), ('09/19/2019, 06:24:04', 30.1), ('09/19/2019, 06:36:25', 29.4), ('09/19/2019, 06:48:45', 30.5), ('09/19/2019, 07:01:06', 29.4), ('09/19/2019, 07:13:27', 31.1), ('09/19/2019, 07:25:47', 29.7), ('09/19/2019, 07:38:08', 30.8), ('09/19/2019, 07:50:29', 29.9), ('09/19/2019, 08:02:49', 29.9), ('09/19/2019, 08:15:10', 30.4), ('09/19/2019, 08:27:30', 29.4), ('09/19/2019, 08:39:51', 31.1), ('09/19/2019, 08:52:11', 30), ('09/19/2019, 09:04:32', 29.4), ('09/19/2019, 09:16:53', 31.2), ('09/19/2019, 09:29:13', 30.1), ('09/19/2019, 09:41:34', 29.5), ('09/19/2019, 10:01:52', 28.8), ('09/19/2019, 10:14:13', 30.3), ('09/19/2019, 10:26:34', 30.4), ('09/19/2019, 10:38:54', 29.7), ('09/19/2019, 10:51:15', 29.4), ('09/19/2019, 11:03:36', 30.4), ('09/19/2019, 11:15:56', 30.6), ('09/19/2019, 11:28:17', 29.9), ('09/19/2019, 11:40:37', 29.6), ('09/19/2019, 11:52:58', 29.3), ('09/19/2019, 12:05:19', 30.3), ('09/19/2019, 12:17:39', 30.6), ('09/19/2019, 12:30:00', 30.6), ('09/19/2019, 12:42:20', 31), ('09/19/2019, 12:54:31', 30.8), ('09/19/2019, 13:07:59', 31.2), ('09/19/2019, 13:20:20', 31.3), ('09/19/2019, 13:32:41', 31.4), ('09/19/2019, 13:45:01', 31.5)], '_values': {...}}

Now, this is great, we can get the value of a given attribute (for example *self.is_raspberry*), as ``self.values["is_raspberry"]``.
However this is not particularly useful as we could get it anyways as ``self.is_raspberry``.
But what if we could fo the reverse operation, that is, set the value of *self.is_raspberry* from *self.values* and have it be at that value.

Well, this is exactly what our decorated methods ``values`` do!
In effect, we are able to write ``self.values["is_raspberry"] = True``,
and the value of *self.is_raspberry* will  be ``True`` after the line is executed.

Throughout the UI code, we will need to modify some attributes, such as *self.graph*, say, that needs to be changed
when the user chooses to see graphs for the last day, week, or month.
Rather than defining a method for each attribute to be updated, we instead create a general method:
``change attribute`` than uses the above mechanism.

Code flow of change_attribute
----------------------------------


.. literalinclude:: ../../../../gui.py
    :pyobject: WhatToShow.change_attribute
    :linenos:
    :language: python

This method takes 4 arguments:

* | ``attribute``: a string of text corresponding to the name of the attribute to be changed.
  | For example, to change *self.graph*, the attribute argument should be ``"graph"``.

* | ``new_value``: the new value that the attribute should take

* | ``key1``: An optional argument, set to ``None`` when omitted.
  | This is useful if the attribute we wish to modify is a dictionary, but we simply wish to modify one of its values.
  | key1 is then the key of this dictionary associated to the value to be modified,
  | i.e. the value to be modified is ``self.values[attribute][key1]``.

* | ``key2``: An optional argument, set to ``None`` when omitted.
  | This is useful when the attribute we wish to modify is a dictionary that has dictionaries as its values.
  | key2 is then the key of this sub-dicitonary associated to the value to be modified.
  | In effect, the value to be modified is ``self.values[attribute][key1][key2]``.

This method then checks whether ``key1`` is ``None``.
If it is, then it assigns ``new_value`` to the right attribute with ``self.values[attribute] = new_value``.
If ``key1`` is not ``None``, the method then checks whether ``key2`` is ``None``.
If it is, then it assigns ``new_value`` to the right key of the attribute with ``self.values[attribute][key1] = new_value``.
Lastly if ``key2`` is not ``None``, the method will execute ``self.values[attribute][key1][key2] = new_value``.

For example, if we wish to modify the current value of the property of a sensor with name "Humidity", to 38.
We would execute the following in python:

.. code-block:: python
    :linenos:

    self.change_attribute("list_sensor_dict", 38, "Humidity", "current")

You may need to refer to the documentation of *self.list_sensor_dict*.

The method then checks what attribute was changed and takes appropriate action.

When the attribute changed is *self.graph* or *self.home_property*,
then *self.start_graph* needs to be recalculated. For example if the user was displaying the graph for the last day,
and then clicks the M tab, *self.start_graph* must become the Unix time of a month ago, rather than a day ago.
Then, *self.measures* is recalculated for the right period and for the right property.

When the attribute change is *self.mode*,
the method will store the appropriate value in **settings.json**, with the key "AUTO". This file is located outside of the *smart_thermostat* directory.
This ensures that upon reboot, the UI reverts to the mode it was in before rebooting.

Lastly, if the temperature unit is modified,
the method will store the appropriate unit in **settings.json**, with the key "temp_unit".
As for *self.mode*, this ensures that this setting is not lost upon reboot.
