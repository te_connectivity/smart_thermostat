Presentation
=======================================
|badge_pipeline| |badge_coverage|

TE Smart Thermostat project
---------------------------

.. list-table:: Final product
   :align: center

   * - .. image:: /_static/1_pic_step_17_con.JPG
        :width: 80%
        :align: center

This documentation provide all steps to build your own Smart Thermostat.

Features
    - Simple touchscreen GUI
    - Choose unit of temperature (°C or °F)
    - Temperature up/down controls
    - Display state of heating controls
    - Very simple algorithm to control heater/aircon/fan/etc
    - Date and time based schedules of operation each with a user set target temperature
    - Application configuration via plain text config file.
    - History of sensor readings

Available sensors
------------------

Core sensors:
  - **Light** 
    
    Sensor values are within a range of 0 to 1024 Lux which is enough for any interior lighting. Connecting Smart Thermostat to dimmer can give us the flexibility
    in matching light intensity with our daily routine - the only thing we need to do is to set a schedule in Settings. 

  - **Temperature** 
    
    The temperature measurement can be from 5˚C up to +50˚C. That's probably the most widely used feature of Smart Thermostat - we can set the target value, 
    display current and historical values (in Graph section).   
  
  - **Humidity** 
    
    We can measure relative humidity value from 5% to 95% RH. Works with our device in the same way as temperature. We can set target value and display 
    current and historical values. To control this property we can use the ventilation system. The proper humidity level in your home and workplace is important to keep a comfortable,
    healthy space.

Optional sensors:
  - **VOC and eCO2** 
    
    Those two sensors are responsible for air quality detection. This part will measure eCO2 (equivalent calculated carbon-dioxide measurement based on total VOC 
    concentrations) concentration within a range of 400 to 8192 parts per million (ppm), and TVOC (Total Volatile Organic Compound) concentration within a range of 
    0 to 1187 parts per billion (ppb). It's good to keep it relatively low, too high concentration of these can be even dangerous. 
    They are maintained by the ventilation system. 

All the sensors mentioned above can be controlled by Smart Thermostat. In each of them, the target setting process is more or less similar. In order to set the target 
current value, we simply need to use arrows on the Home screen next to the property that we want to set. Another way is to schedule reaching exact value for a specific 
time which is possible via the Settings menu.   

User interface
--------------

.. list-table:: Default menu
   :align: center

   * - |scheme_welcome_menu|
     - **Touch the sleeping screen to acces menus.**

       |gif_waking_up|

.. list-table:: Main menu
   :align: center

   * - |scheme_main_menu| 
     - **Change the target of sensors.**
       
       |gif_changing_target| 

       **Click on the right and left arrows to change the sensor menu.**
       
       |gif_switching_temperatureORhumidityORetc|

.. list-table:: Option menu 
   :align: center

   * - |scheme_option_menu_1|
     - **Change degree unit.**

       |gif_changing_to_fahrenheid|
       
       **Manual mode**

       |gif_changing_to_manual_mode|
   * - |scheme_option_menu_2|
     - **Hide a sensor.**

       |gif_display_setings|

.. list-table:: Settings
   :align: center

   * - |scheme_settings_menu|
     - **Add a setting.**

       |gif_settings|

.. list-table:: Graphs
   :align: center

   * - |scheme_graphs_menu|
     - **Graph display.**

       |gif_graph|


About TE Connectivity
---------------------

.. list-table:: 
  :align: center

  * - |pic_te|
  * - If you would like to know more about us please visit `TE website  <https://www.te.com>`_.

Solutions that power electric vehicles, aircraft, digital factories, and smart homes.
Innovation that enables life-saving medical care, sustainable communities,
efficient utility networks, and the global communications infrastructure.
For more than 75 years, we have partnered with customers to produce highly engineered connectivity and sensing products that make a connected world possible.
Our focus on reliability and durability, our commitment to progress,
and the unmatched range of our product portfolio enables companies large and small to turn ideas into technology that can transform how the world works and lives tomorrow.


At **TE**, we invest in significant research, development, and engineering to meet the rigorous demands of our customers’ increasingly complex technology.
Our expertise in materials science research, advanced design methods, and global manufacturing capability allow us to create the rugged and reliable solutions
engineers and entrepreneurs need.


At **TE**, we operate with the highest standards of ethics and sustainability. We embrace our responsibility to the customers and communities we serve as well as to the future of our planet.
