Interface core
==============

.. image:: /_static/header.svg
    :width: 100%

.. toctree::
    :caption: Basic Classes for the UI:

    Introduction <introduction>
    Initialisation <init>
    Loop methods <loop_methods>
    change_attribute method <change_attribute>
    Other non-menu methods <other_non_menu_methods>
    Default menu <default_menu>
    Home menu <home_menu>
    Graphs <graphs>
