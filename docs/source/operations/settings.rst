Settings
========

Introduction
------------
In order to store and modify permanently some variables that are uniques, you can use the file ``settings.py``.
It is based on the ``.json`` storage and working with the ``pubsub``.

How it's work
-------------
There is only two usable methods for this class and three at all:

Methods:
    Refresh method. It permit to open the ``.json`` file and 
    update the content of the dictionnary attribute ``self.data``.
    
    .. literalinclude:: ../../../settings.py
        :emphasize-lines: 1
        :language: python
        :linenos:
        :pyobject: Settings.refresh
        
    The get method return the variable value. 
    If there were a change on this variable according to the pubsub, it launch the refresh method.
    
    .. literalinclude:: ../../../settings.py
        :emphasize-lines: 1
        :language: python
        :linenos:
        :pyobject: Settings.get
        
    This method permit to modify the content of ``.json`` file. 
    It also trigger the pubsub on the channel of the variable name.
    
    .. literalinclude:: ../../../settings.py
        :emphasize-lines: 1
        :language: python
        :linenos:
        :pyobject: Settings.set


Source code
-----------
.. literalinclude:: ../../../settings.py
    :emphasize-lines: 1
    :language: python
    :linenos:
    :pyobject: Settings