import inspect
import os
import sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))  # Get current dir
parentdir = os.path.dirname(currentdir)  # get parent dir
dir = parentdir + "/"
sys.path.insert(0, dir)  # insert path of parent dir in import settings

from convergence import *

class Test(unittest.TestCase):

    def setUp(self):
        St=settings.Settings()
        St.set("is_unittest",True)
        self.Pub = pubsub.Pubsub()
        self.Db = db.DB()
        actuators = self.Db.reset_actuators_state()
    def test_1_relay(self):
        self.Db.remove_actuators()
        self.Db.insert_actuator("Test_actuator",1)
        id = self.Db.get_actuator_id("Test_actuator")
        self.Db.set_actuator_value(id,1.0)
        time.sleep(1)
        self.Pub.get("relay_test1_pass")
        relay.loop()
        self.assertEqual(self.Pub.get("relay_test1_pass"), 1)

if __name__ == '__main__':
    unittest.main()
