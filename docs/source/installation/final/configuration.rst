Configuration
==============
Sensors
-------

.. list-table:: Sensors default configuration
  :align: center

  * - **Variable**
    - **Temperature**
    - **Humidity**
    - **CO2**
    - **VOC**
  * - ``Default``
    - 30
    - 50
    - 750
    - 1000
  * - ``outer``
    - 0.4
    - 5
    - 100
    - 200
  * - ``out``
    - 0.7
    - 10
    - 250
    - 300
  * - ``unit``
    - °C
    - %
    - ppm
    - ppb

Actuators
---------

.. list-table:: Actuators default configuration
  :align: center

  * - **Variable**
    - **AC**
    - **HE**
    - **FAN**
    - **HE+**
  * - ``gpio``
    - 13
    - 6
    - 5
    - 26

Actuators sensors
-----------------

.. list-table:: Actuators sensors default configuration
  :align: center
  
  * - **Variable**
    - **Temperature**
    - **Humidity**
    - **CO2**
    - **VOC**
  * - **AC**
    - ``up=0`` ``backup=0``
    -
    -
    -
  * - **HE**
    - ``up=1`` ``backup=0``
    -
    -
    -
  * - **FAN**
    -
    - ``up=0`` ``backup=0``
    - ``up=0`` ``backup=0``
    - ``up=0`` ``backup=0``
  * - **HE+**
    - ``up=1`` ``backup=1``
    -
    -
    -

Files
-----

.. list-table:: There is 3 different files that host datas in this project:
  :align: center

  * - Name
    - Extension
    - Path
  * - db 
    - ``.db``
    - */../db.db*
  * - settings 
    - ``.json``
    - */../settings.json*
  * - variables 
    - ``.py``
    - */variables.py*

Database
~~~~~~~~

The database store huge amont of datas and permit data processing in order to trace graphs.
The file ``db.db`` is created automatically when you run the project for the first time. 
To reset changes, you just need to remove this file, it will be regenerate automatically.

.. note:: It is here that you can add / remove sensors, actuators and links between actuators and sensors.

Settings
~~~~~~~~

The setting storage permit to store uniques variables that the user can change through the user interface.
The file ``settings.json`` is created automatically when you run the project for the first time. 
To reset changes, you just need to remove this file, it will be regenerate automatically.

.. note:: You can edit this file with any text editor software.

Variables
~~~~~~~~~

The file ``variables.py`` store every fixed variables in the project and default configuration of actuators, sensors and links between actuators and sensors.